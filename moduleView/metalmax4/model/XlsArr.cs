﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsArr
    {
        private int seqNo;
        private long offset ;
        private int blockLen;
        private String name;
        private List<byte> data = new List<byte>();
        

        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }
        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }

        public int BlockLen
        {
            get { return blockLen; }
            set { blockLen = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }


    }
}
