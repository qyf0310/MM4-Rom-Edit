﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsBtlActdataArrDecodse
    {
        public static List<XlsBattleActData> loadData(byte[] datas, string key)
        {
            List<XlsNam> itemNames = DataBaseService.getData("btl_act_msg.nam") as List<XlsNam>;
            List<XlsBattleActData> result = new List<XlsBattleActData>();

            int index = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + 9 >= datas.Length)
                {
                    break;
                }
                XlsBattleActData o = new XlsBattleActData();
                result.Add(o);
                o.SeqNo = index;
                o.Offset = i;
                o.NameOffAddr = datas[i + 1] * 256 + datas[i];
                o.ActionCode= datas[i + 3] * 256 + datas[i+2];
                o.Unknown1= datas[i + 5] * 256 + datas[i + 4];
                o.AnimeId = datas[i + 7] * 256 + datas[i + 6];
                o.WordsOffAdr = datas[i + 9] * 256 + datas[i + 8];

                var temp = itemNames.FirstOrDefault(x => x.Offset == o.NameOffAddr);
                o.Name = temp == null ? "unknown" : temp.Text;

                temp = itemNames.FirstOrDefault(x => x.Offset == o.WordsOffAdr);
                o.Words = temp == null ? "unknown" : temp.Text;

                for (int j = 0; j < 10; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }


                index++;


                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o,key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null,key,true);
            DataBaseService.addData(key, result);
            return result;
        }
    }
}
