﻿using MM4RomEdit.Controls;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    partial class ItemView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbType = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbbStar = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rbRandomNo = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.rbRandomYes = new System.Windows.Forms.RadioButton();
            this.tbRemark = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.chebIsDLC = new System.Windows.Forms.CheckBox();
            this.tbRomAddr = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbRomData = new System.Windows.Forms.TextBox();
            this.btnSaveToRom = new System.Windows.Forms.Button();
            this.searchTree1 = new SearchTree();
            this.ncMoney = new NumericControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCarStar7 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncMoney)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(168, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "价格：";
            // 
            // tbType
            // 
            this.tbType.Location = new System.Drawing.Point(368, 32);
            this.tbType.Name = "tbType";
            this.tbType.ReadOnly = true;
            this.tbType.Size = new System.Drawing.Size(100, 21);
            this.tbType.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(321, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "类别：";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(215, 32);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(100, 21);
            this.tbName.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(168, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "名称：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbbStar);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.rbRandomNo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.rbRandomYes);
            this.groupBox1.Location = new System.Drawing.Point(170, 190);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(298, 87);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "星数";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(159, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "（非随机时为入手星数）";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(159, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(125, 12);
            this.label6.TabIndex = 10;
            this.label6.Text = "（变动范围：0~预置）";
            // 
            // cbbStar
            // 
            this.cbbStar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbStar.FormattingEnabled = true;
            this.cbbStar.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.cbbStar.Location = new System.Drawing.Point(77, 24);
            this.cbbStar.Name = "cbbStar";
            this.cbbStar.Size = new System.Drawing.Size(76, 20);
            this.cbbStar.TabIndex = 10;
            this.cbbStar.SelectedIndexChanged += new System.EventHandler(this.cbbStar_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "预置星数：";
            // 
            // rbRandomNo
            // 
            this.rbRandomNo.AutoSize = true;
            this.rbRandomNo.Location = new System.Drawing.Point(118, 55);
            this.rbRandomNo.Name = "rbRandomNo";
            this.rbRandomNo.Size = new System.Drawing.Size(35, 16);
            this.rbRandomNo.TabIndex = 7;
            this.rbRandomNo.TabStop = true;
            this.rbRandomNo.Text = "否";
            this.rbRandomNo.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "入手随机：";
            // 
            // rbRandomYes
            // 
            this.rbRandomYes.AutoSize = true;
            this.rbRandomYes.Location = new System.Drawing.Point(77, 55);
            this.rbRandomYes.Name = "rbRandomYes";
            this.rbRandomYes.Size = new System.Drawing.Size(35, 16);
            this.rbRandomYes.TabIndex = 0;
            this.rbRandomYes.TabStop = true;
            this.rbRandomYes.Text = "是";
            this.rbRandomYes.UseVisualStyleBackColor = true;
            this.rbRandomYes.CheckedChanged += new System.EventHandler(this.rbRandomYes_CheckedChanged);
            // 
            // tbRemark
            // 
            this.tbRemark.Location = new System.Drawing.Point(215, 138);
            this.tbRemark.Name = "tbRemark";
            this.tbRemark.ReadOnly = true;
            this.tbRemark.Size = new System.Drawing.Size(251, 21);
            this.tbRemark.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(168, 141);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 9;
            this.label8.Text = "备注：";
            // 
            // chebIsDLC
            // 
            this.chebIsDLC.AutoSize = true;
            this.chebIsDLC.Enabled = false;
            this.chebIsDLC.Location = new System.Drawing.Point(368, 90);
            this.chebIsDLC.Name = "chebIsDLC";
            this.chebIsDLC.Size = new System.Drawing.Size(66, 16);
            this.chebIsDLC.TabIndex = 11;
            this.chebIsDLC.Text = "DLC物品";
            this.chebIsDLC.UseVisualStyleBackColor = true;
            // 
            // tbRomAddr
            // 
            this.tbRomAddr.Location = new System.Drawing.Point(233, 301);
            this.tbRomAddr.Name = "tbRomAddr";
            this.tbRomAddr.ReadOnly = true;
            this.tbRomAddr.Size = new System.Drawing.Size(138, 21);
            this.tbRomAddr.TabIndex = 13;
            this.tbRomAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(168, 304);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "ROM地址：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(168, 350);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 12);
            this.label10.TabIndex = 14;
            this.label10.Text = "ROM数据：";
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(424, 301);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(44, 21);
            this.tbLength.TabIndex = 16;
            this.tbLength.Text = "12";
            this.tbLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(377, 304);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 15;
            this.label11.Text = "长度：";
            // 
            // tbRomData
            // 
            this.tbRomData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRomData.Location = new System.Drawing.Point(233, 347);
            this.tbRomData.Multiline = true;
            this.tbRomData.Name = "tbRomData";
            this.tbRomData.ReadOnly = true;
            this.tbRomData.Size = new System.Drawing.Size(617, 114);
            this.tbRomData.TabIndex = 17;
            // 
            // btnSaveToRom
            // 
            this.btnSaveToRom.Location = new System.Drawing.Point(474, 301);
            this.btnSaveToRom.Name = "btnSaveToRom";
            this.btnSaveToRom.Size = new System.Drawing.Size(75, 23);
            this.btnSaveToRom.TabIndex = 18;
            this.btnSaveToRom.Text = "写入ROM";
            this.btnSaveToRom.UseVisualStyleBackColor = true;
            this.btnSaveToRom.Click += new System.EventHandler(this.btnSaveToRom_Click);
            // 
            // searchTree1
            // 
            this.searchTree1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTree1.Location = new System.Drawing.Point(1, 1);
            this.searchTree1.Name = "searchTree1";
            this.searchTree1.Size = new System.Drawing.Size(161, 471);
            this.searchTree1.TabIndex = 0;
            this.searchTree1.DoubleClick += new System.EventHandler(this.searchTree1_DoubleClick);
            // 
            // ncMoney
            // 
            this.ncMoney.Location = new System.Drawing.Point(215, 85);
            this.ncMoney.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncMoney.MaxNumberControl = null;
            this.ncMoney.MinNumberControl = null;
            this.ncMoney.Name = "ncMoney";
            this.ncMoney.Size = new System.Drawing.Size(100, 21);
            this.ncMoney.TabIndex = 43;
            this.ncMoney.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCarStar7);
            this.groupBox2.Location = new System.Drawing.Point(555, 224);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 100);
            this.groupBox2.TabIndex = 44;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "快速修改";
            // 
            // btnCarStar7
            // 
            this.btnCarStar7.Location = new System.Drawing.Point(6, 20);
            this.btnCarStar7.Name = "btnCarStar7";
            this.btnCarStar7.Size = new System.Drawing.Size(188, 23);
            this.btnCarStar7.TabIndex = 45;
            this.btnCarStar7.Text = "战车引擎、武器固定7星";
            this.btnCarStar7.UseVisualStyleBackColor = true;
            this.btnCarStar7.Click += new System.EventHandler(this.btnCarStar7_Click);
            // 
            // ItemView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 473);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.ncMoney);
            this.Controls.Add(this.btnSaveToRom);
            this.Controls.Add(this.tbRomData);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbRomAddr);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.chebIsDLC);
            this.Controls.Add(this.tbRemark);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchTree1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ItemView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "ItemView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncMoney)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.SearchTree searchTree1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbRandomNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbRandomYes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbbStar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chebIsDLC;
        private System.Windows.Forms.TextBox tbRomAddr;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbRomData;
        private System.Windows.Forms.Button btnSaveToRom;
        private Controls.NumericControl ncMoney;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCarStar7;
    }
}