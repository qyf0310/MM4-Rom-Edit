﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsNam
    {
        private int maxLength;
        private int currentLength;
        private string text;
        private List<byte> data=new List<byte>();
        private long offset = 0;

        public int MaxLength
        {
            get { return maxLength; }
            set { maxLength = value; }
        }

        public int CurrentLength
        {
            get { return currentLength; }
            set { currentLength = value; }
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
