﻿using MM4RomEdit.Controls;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    partial class EngineView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbbUpdate = new System.Windows.Forms.ComboBox();
            this.cbbAbility = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSaveToRom = new System.Windows.Forms.Button();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbRomAddr = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbRomData = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tbUpdateDesc = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbGeneration = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ncLoadPower = new NumericControl();
            this.ncDefMax = new NumericControl();
            this.ncDEFInit = new NumericControl();
            this.ncCost = new NumericControl();
            this.ncWeight = new NumericControl();
            this.searchTree1 = new SearchTree();
            ((System.ComponentModel.ISupportInitialize)(this.ncLoadPower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDefMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEFInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncWeight)).BeginInit();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(228, 6);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(241, 21);
            this.tbName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "名称    ：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(157, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "重量    ：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(310, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 42;
            this.label2.Text = "改造费  ：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(157, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 44;
            this.label4.Text = "初始防御：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(310, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 46;
            this.label5.Text = "最大防御：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(157, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 52;
            this.label9.Text = "载重：";
            // 
            // cbbUpdate
            // 
            this.cbbUpdate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbUpdate.FormattingEnabled = true;
            this.cbbUpdate.Items.AddRange(new object[] {
            "不可升级",
            "无限制",
            "星数限制"});
            this.cbbUpdate.Location = new System.Drawing.Point(228, 187);
            this.cbbUpdate.Name = "cbbUpdate";
            this.cbbUpdate.Size = new System.Drawing.Size(76, 20);
            this.cbbUpdate.TabIndex = 64;
            this.cbbUpdate.SelectedIndexChanged += new System.EventHandler(this.cbbUpdate_SelectedIndexChanged);
            // 
            // cbbAbility
            // 
            this.cbbAbility.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAbility.FormattingEnabled = true;
            this.cbbAbility.Location = new System.Drawing.Point(381, 143);
            this.cbbAbility.Name = "cbbAbility";
            this.cbbAbility.Size = new System.Drawing.Size(88, 20);
            this.cbbAbility.TabIndex = 63;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(157, 190);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 61;
            this.label13.Text = "升级条件：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(310, 147);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 12);
            this.label12.TabIndex = 60;
            this.label12.Text = "特性   ：";
            // 
            // btnSaveToRom
            // 
            this.btnSaveToRom.Location = new System.Drawing.Point(480, 269);
            this.btnSaveToRom.Name = "btnSaveToRom";
            this.btnSaveToRom.Size = new System.Drawing.Size(86, 23);
            this.btnSaveToRom.TabIndex = 63;
            this.btnSaveToRom.Text = "写入ROM";
            this.btnSaveToRom.UseVisualStyleBackColor = true;
            this.btnSaveToRom.Click += new System.EventHandler(this.btnSaveToRom_Click);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(425, 271);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(44, 21);
            this.tbLength.TabIndex = 62;
            this.tbLength.Text = "20";
            this.tbLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(378, 274);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 61;
            this.label38.Text = "长度：";
            // 
            // tbRomAddr
            // 
            this.tbRomAddr.Location = new System.Drawing.Point(228, 271);
            this.tbRomAddr.Name = "tbRomAddr";
            this.tbRomAddr.ReadOnly = true;
            this.tbRomAddr.Size = new System.Drawing.Size(136, 21);
            this.tbRomAddr.TabIndex = 60;
            this.tbRomAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(157, 274);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 12);
            this.label37.TabIndex = 59;
            this.label37.Text = "ROM地址：";
            // 
            // tbRomData
            // 
            this.tbRomData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRomData.Location = new System.Drawing.Point(159, 324);
            this.tbRomData.Multiline = true;
            this.tbRomData.Name = "tbRomData";
            this.tbRomData.ReadOnly = true;
            this.tbRomData.Size = new System.Drawing.Size(570, 96);
            this.tbRomData.TabIndex = 65;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(157, 309);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 12);
            this.label39.TabIndex = 64;
            this.label39.Text = "ROM数据：";
            // 
            // tbUpdateDesc
            // 
            this.tbUpdateDesc.Location = new System.Drawing.Point(312, 223);
            this.tbUpdateDesc.Name = "tbUpdateDesc";
            this.tbUpdateDesc.ReadOnly = true;
            this.tbUpdateDesc.Size = new System.Drawing.Size(157, 21);
            this.tbUpdateDesc.TabIndex = 67;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(157, 226);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 12);
            this.label6.TabIndex = 68;
            this.label6.Text = "星数限制时的条件：";
            // 
            // tbGeneration
            // 
            this.tbGeneration.Location = new System.Drawing.Point(380, 187);
            this.tbGeneration.Name = "tbGeneration";
            this.tbGeneration.ReadOnly = true;
            this.tbGeneration.Size = new System.Drawing.Size(89, 21);
            this.tbGeneration.TabIndex = 69;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(310, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 70;
            this.label7.Text = "引擎世代：";
            // 
            // ncLoadPower
            // 
            this.ncLoadPower.DecimalPlaces = 2;
            this.ncLoadPower.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ncLoadPower.Location = new System.Drawing.Point(228, 142);
            this.ncLoadPower.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            131072});
            this.ncLoadPower.MaxNumberControl = null;
            this.ncLoadPower.MinNumberControl = null;
            this.ncLoadPower.Name = "ncLoadPower";
            this.ncLoadPower.Size = new System.Drawing.Size(76, 21);
            this.ncLoadPower.TabIndex = 66;
            this.ncLoadPower.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncDefMax
            // 
            this.ncDefMax.Location = new System.Drawing.Point(381, 94);
            this.ncDefMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncDefMax.MaxNumberControl = null;
            this.ncDefMax.MinNumberControl = null;
            this.ncDefMax.Name = "ncDefMax";
            this.ncDefMax.Size = new System.Drawing.Size(88, 21);
            this.ncDefMax.TabIndex = 47;
            this.ncDefMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncDEFInit
            // 
            this.ncDEFInit.Location = new System.Drawing.Point(228, 94);
            this.ncDEFInit.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncDEFInit.MaxNumberControl = null;
            this.ncDEFInit.MinNumberControl = null;
            this.ncDEFInit.Name = "ncDEFInit";
            this.ncDEFInit.Size = new System.Drawing.Size(76, 21);
            this.ncDEFInit.TabIndex = 45;
            this.ncDEFInit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncCost
            // 
            this.ncCost.Location = new System.Drawing.Point(381, 42);
            this.ncCost.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncCost.MaxNumberControl = null;
            this.ncCost.MinNumberControl = null;
            this.ncCost.Name = "ncCost";
            this.ncCost.Size = new System.Drawing.Size(88, 21);
            this.ncCost.TabIndex = 43;
            this.ncCost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncWeight
            // 
            this.ncWeight.DecimalPlaces = 2;
            this.ncWeight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ncWeight.Location = new System.Drawing.Point(228, 42);
            this.ncWeight.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            131072});
            this.ncWeight.MaxNumberControl = null;
            this.ncWeight.MinNumberControl = null;
            this.ncWeight.Name = "ncWeight";
            this.ncWeight.Size = new System.Drawing.Size(76, 21);
            this.ncWeight.TabIndex = 41;
            this.ncWeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // searchTree1
            // 
            this.searchTree1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTree1.Location = new System.Drawing.Point(2, 2);
            this.searchTree1.Name = "searchTree1";
            this.searchTree1.Size = new System.Drawing.Size(149, 428);
            this.searchTree1.TabIndex = 0;
            this.searchTree1.DoubleClick += new System.EventHandler(this.searchTree1_DoubleClick);
            // 
            // EngineView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 432);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbGeneration);
            this.Controls.Add(this.cbbAbility);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.tbUpdateDesc);
            this.Controls.Add(this.cbbUpdate);
            this.Controls.Add(this.ncLoadPower);
            this.Controls.Add(this.tbRomData);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnSaveToRom);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.tbRomAddr);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ncDefMax);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ncDEFInit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ncCost);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ncWeight);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.searchTree1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EngineView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "CarView";
            ((System.ComponentModel.ISupportInitialize)(this.ncLoadPower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDefMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEFInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncWeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.SearchTree searchTree1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Controls.NumericControl ncWeight;
        private Controls.NumericControl ncCost;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private Controls.NumericControl ncDEFInit;
        private Controls.NumericControl ncDefMax;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbbUpdate;
        private System.Windows.Forms.ComboBox cbbAbility;
        private System.Windows.Forms.Button btnSaveToRom;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbRomAddr;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbRomData;
        private System.Windows.Forms.Label label39;
        private Controls.NumericControl ncLoadPower;
        private System.Windows.Forms.TextBox tbUpdateDesc;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbGeneration;
        private System.Windows.Forms.Label label7;
    }
}