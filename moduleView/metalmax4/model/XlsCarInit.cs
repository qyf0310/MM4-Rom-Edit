﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsCarInit : ModelBase
    {

        private int seqNo;
        private long offset = 0;

        private String name;
        private int nameOffAddr;
        private List<byte> data=new List<byte>();
        
        private int chassisIdx;
        private int cunitIdx;
        private int enginIdx;
        private int hole1Idx;
        private int hole2Idx;
        private int hole3Idx;
        private int hole4Idx;
        private int hole5Idx;
        private int hole6Idx;
        private int item1Idx;
        private int item2Idx;
        private int item3Idx;
        private int item4Idx;
        private int item5Idx;
        private int sp;
        private string unknown;

        public override string getText()
        {
            return name;
        }

        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int NameOffAddr
        {
            get { return nameOffAddr; }
            set { nameOffAddr = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }

        public int ChassisIdx
        {
            get { return chassisIdx; }
            set { chassisIdx = value; }
        }

        public int CunitIdx
        {
            get { return cunitIdx; }
            set { cunitIdx = value; }
        }

        public int EnginIdx
        {
            get { return enginIdx; }
            set { enginIdx = value; }
        }

        public int Hole1Idx
        {
            get { return hole1Idx; }
            set { hole1Idx = value; }
        }

        public int Hole2Idx
        {
            get { return hole2Idx; }
            set { hole2Idx = value; }
        }

        public int Hole3Idx
        {
            get { return hole3Idx; }
            set { hole3Idx = value; }
        }

        public int Hole4Idx
        {
            get { return hole4Idx; }
            set { hole4Idx = value; }
        }

        public int Hole5Idx
        {
            get { return hole5Idx; }
            set { hole5Idx = value; }
        }

        public int Hole6Idx
        {
            get { return hole6Idx; }
            set { hole6Idx = value; }
        }

        public int Item1Idx
        {
            get { return item1Idx; }
            set { item1Idx = value; }
        }

        public int Item2Idx
        {
            get { return item2Idx; }
            set { item2Idx = value; }
        }

        public int Item3Idx
        {
            get { return item3Idx; }
            set { item3Idx = value; }
        }

        public int Item4Idx
        {
            get { return item4Idx; }
            set { item4Idx = value; }
        }

        public int Item5Idx
        {
            get { return item5Idx; }
            set { item5Idx = value; }
        }

        public int Sp
        {
            get { return sp; }
            set { sp = value; }
        }

        public string Unknown
        {
            get { return unknown; }
            set { unknown = value; }
        }
    }
}
