﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class FileOperateHelper
    {
        public static byte[] readAllBytes(String filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new Exception("file not exist:"+filePath);
            }
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                byte[] dataBytes = new byte[(int)fs.Length];
                fs.Read(dataBytes, 0, dataBytes.Length);
                return dataBytes;
            }
        }

        public static byte[] modifyBin(String filePath,int offset,byte[] data)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite))
            {
                fs.Seek(offset, SeekOrigin.Begin);
                fs.Write(data,0,data.Length);
                fs.Flush();
                byte[] result=new byte[data.Length];
                fs.Seek(offset, SeekOrigin.Begin);
                for (int i = 0; i < data.Length; i++)
                {
                    result[i] = (byte)fs.ReadByte();
                }
                return result;
            }
        }
        public static void outputTextFile(String filePath, String content)
        {
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                FileInfo fi = new FileInfo(filePath);
                var dir = fi.Directory;
                if (!dir.Exists)
                {
                    dir.Create();
                }
                fs = new FileStream(filePath, FileMode.Create);
                sw = new StreamWriter(fs);

                sw.WriteLine(content);
                sw.Flush();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }
    }
}
