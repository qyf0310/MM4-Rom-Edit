﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.model;
using SharpConfig;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class HumanWeponView : Form
    {
        private XlsNingenBuki currentItem = null;
        private Section animeSection = null;
        public HumanWeponView()
        {
            InitializeComponent();
            init();
            
        }
        public void init()
        {
            List<XlsNingenBuki> datas = DataBaseService.getData(ConstanDef.ArrFileKey_Human_Wepon) as List<XlsNingenBuki>;
            searchTree1.loadList(datas);


            for (int i = 0; i < 42; i++)
            {
                String name = DataBaseService.getWeponAttkRange(i);
                cbbRange.Items.Add(name);
            }           
            List<AnimeNam> animeNams=DataBaseService.getAnimeList().Where(x=>x.Type==2).ToList();
            cbbAnime.ValueMember = "Code";
            cbbAnime.DisplayMember = "Text";
            cbbAnime.DataSource = animeNams;

            List<XlsBattleActData> batActions = DataBaseService.getData(ConstanDef.ArrFileKey_btl_actdata) as List<XlsBattleActData>;
            List<XlsBattleActData> temp=new List<XlsBattleActData>(batActions);
            cbbAttribute.ValueMember = "ActionCode";
            cbbAttribute.DisplayMember = "Name";
            cbbAttribute.DataSource = temp;

        }
        private void searchTree1_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree)sender;
            if (control.TreeView.SelectedNode != null)
            {
                loadItem(Convert.ToInt32(control.TreeView.SelectedNode.Tag));
            }

        }
        private void loadItem(int index)
        {
            List<XlsNingenBuki> datas = DataBaseService.getData(ConstanDef.ArrFileKey_Human_Wepon) as List<XlsNingenBuki>;
            XlsNingenBuki o = datas[index];
            currentItem = o;
            tbName.Text = o.Name;
            tbRomAddr.Text = String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_Human_Wepon)["data_offset"].StringValue, 16) + o.Offset);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in o.Data)
            {
                sb.Append(String.Format("{0:X2} ", b));
            }
            tbRomData.Text = sb.ToString();
            ncATK.Value = o.Attack;
            tbUnknown1.Text = o.Unknown.ToString();
            cbbAnime.SelectedValue = o.AnimeId;
            //labelAnime.Text = "";
            //if (cbbAnime.SelectedValue == null)
            //{
            //    labelAnime.Text = String.Format("动画未知,code:{0},0x{0:X4}", o.Attribute);
            //}
            cbbRange.SelectedIndex = o.Range;
            cbbAttribute.SelectedValue = o.Attribute;
            tbAttackResult.Text = "";
            if (cbbAttribute.SelectedValue == null)
            {
                tbAttackResult.Text = String.Format("效果未知,code:{0},0x{0:X4}", o.Attribute);
            }
            else
            {
                List<XlsBattleActData> temp = DataBaseService.getData(ConstanDef.ArrFileKey_btl_actdata) as List<XlsBattleActData>;
                var wordsTemp = temp.FirstOrDefault(x => x.ActionCode == o.Attribute);
                tbAttackResult.Text = wordsTemp == null ? "" : wordsTemp.Words;
            }

            setMaleEquiSel((byte)o.Male);
            setFeMaleEquiSel((byte)o.Female);
            setAnimalEquiSel((byte)o.Animal);
        }

        private void btnSaveToRom_Click(object sender, EventArgs e)
        {
            byte[] bytes = currentItem.Data.ToArray();

            bytes[0] = getMaleEquip();
            bytes[1] = getFeMaleEquip();
            bytes[2] = getAnimalEquip();

            bytes[4] = (byte)(ncATK.Value % 256);
            bytes[5] = (byte)(ncATK.Value / 256);


            if (cbbAttribute.SelectedValue!=null)
            {
                int selValue = Convert.ToInt32(cbbAttribute.SelectedValue);
                bytes[6] = (byte)(selValue % 256);
                bytes[7] = (byte)(selValue / 256);
            }

            if (cbbAnime.SelectedValue!=null)
            {
                int selValue = Convert.ToInt32(cbbAnime.SelectedValue);
                bytes[8] = (byte)(selValue % 256);
                bytes[9] = (byte)(selValue / 256);
            }
            bytes[10] = (byte)cbbRange.SelectedIndex;



            try
            {
                byte[] newBytes = FileOperateHelper.modifyBin(DataBaseService.getRomPath(), Convert.ToInt32(tbRomAddr.Text, 16), bytes);
                XlsNingenBuki newO = XlsNingenBukiArrDecode.loadData(newBytes, "edit")[0];
                
                
                List<XlsNingenBuki> datas = DataBaseService.getData(ConstanDef.ArrFileKey_Human_Wepon) as List<XlsNingenBuki>;
                int index = Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag);
                XlsNingenBuki oldO = datas[index];
                newO.Name = oldO.Name;
                newO.SeqNo = oldO.SeqNo;
                newO.Offset = oldO.Offset;
                datas[index] = newO;
                loadItem(index);
                MessageBox.Show("写入完毕");
            }
            catch (Exception exception)
            {
                MessageBox.Show("写入失败," + exception.Message);
            }
        }

        private void cbbAttribute_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<XlsBattleActData> temp = DataBaseService.getData(ConstanDef.ArrFileKey_btl_actdata) as List<XlsBattleActData>;
            var wordsTemp = temp.FirstOrDefault(x => x.ActionCode == Convert.ToInt32(cbbAttribute.SelectedValue));
            tbAttackResult.Text = wordsTemp == null ? "" : wordsTemp.Words;
        }

        private void chebEquiAnime_CheckedChanged(object sender, EventArgs e)
        {
            chebEquiDog.Checked = chebEquiAnime.Checked;
            chebEquiBear.Checked = chebEquiAnime.Checked;
        }

        private void chebEquiFM_CheckedChanged(object sender, EventArgs e)
        {
            chebEquiFL.Checked = chebEquiFM.Checked;
            chebEquiFX.Checked = chebEquiFM.Checked;
            chebEquiFB.Checked = chebEquiFM.Checked;
            chebEquiFH.Checked = chebEquiFM.Checked;
            chebEquiFS.Checked = chebEquiFM.Checked;
            chebEquiFY.Checked = chebEquiFM.Checked;
            chebEquiFW.Checked = chebEquiFM.Checked;
            chebEquiFQ.Checked = chebEquiFM.Checked;
        }

        private void chebEquibM_CheckedChanged(object sender, EventArgs e)
        {
            chebEquiML.Checked = chebEquibM.Checked;
            chebEquiMX.Checked = chebEquibM.Checked;
            chebEquiMB.Checked = chebEquibM.Checked;
            chebEquiMH.Checked = chebEquibM.Checked;
            chebEquiMS.Checked = chebEquibM.Checked;
            chebEquiMY.Checked = chebEquibM.Checked;
            chebEquiMW.Checked = chebEquibM.Checked;
            chebEquiMQ.Checked = chebEquibM.Checked;
        }

        private void chebEquiAll_CheckedChanged(object sender, EventArgs e)
        {
            chebEquibM.Checked = chebEquiAll.Checked;
            chebEquiFM.Checked= chebEquiAll.Checked;
            chebEquiAnime.Checked= chebEquiAll.Checked;
        }

        private void setMaleEquiSel(byte data)
        {
            chebEquiML.Checked = (data & 0x01)>0;
            chebEquiMX.Checked = (data & 0x02) > 0;
            chebEquiMB.Checked = (data & 0x04) > 0;
            chebEquiMH.Checked = (data & 0x08) > 0;
            chebEquiMS.Checked = (data & 0x10) > 0;
            chebEquiMY.Checked = (data & 0x20) > 0;
            chebEquiMW.Checked = (data & 0x80) > 0;
            chebEquiMQ.Checked = (data & 0x40) > 0;
        }
        private void setFeMaleEquiSel(byte data)
        {
            chebEquiFL.Checked = (data & 0x01) > 0;
            chebEquiFX.Checked = (data & 0x02) > 0;
            chebEquiFB.Checked = (data & 0x04) > 0;
            chebEquiFH.Checked = (data & 0x08) > 0;
            chebEquiFS.Checked = (data & 0x10) > 0;
            chebEquiFY.Checked = (data & 0x20) > 0;
            chebEquiFW.Checked = (data & 0x80) > 0;
            chebEquiFQ.Checked = (data & 0x40) > 0;
        }
        private void setAnimalEquiSel(byte data)
        {
            chebEquiDog.Checked = (data & 0x01) > 0;
            chebEquiBear.Checked = (data & 0x02) > 0;
        }
        private byte getMaleEquip()
        {
            byte result = 0;
            if (chebEquiML.Checked)
            {
                result |= 0x01;
            }
            if (chebEquiMX.Checked)
            {
                result |= 0x02;
            }
            if (chebEquiMB.Checked)
            {
                result |= 0x04;
            }
            if (chebEquiMH.Checked)
            {
                result |= 0x08;
            }
            if (chebEquiMS.Checked)
            {
                result |= 0x10;
            }
            if (chebEquiMY.Checked)
            {
                result |= 0x20;
            }
            if (chebEquiMW.Checked)
            {
                result |= 0x80;
            }
            if (chebEquiMQ.Checked)
            {
                result |= 0x40;
            }
            return result;
        }
        private byte getFeMaleEquip()
        {
            byte result = 0;
            if (chebEquiFL.Checked)
            {
                result |= 0x01;
            }
            if (chebEquiFX.Checked)
            {
                result |= 0x02;
            }
            if (chebEquiFB.Checked)
            {
                result |= 0x04;
            }
            if (chebEquiFH.Checked)
            {
                result |= 0x08;
            }
            if (chebEquiFS.Checked)
            {
                result |= 0x10;
            }
            if (chebEquiFY.Checked)
            {
                result |= 0x20;
            }
            if (chebEquiFW.Checked)
            {
                result |= 0x80;
            }
            if (chebEquiFQ.Checked)
            {
                result |= 0x40;
            }
            return result;
        }
        private byte getAnimalEquip()
        {
            byte result = 0;
            if (chebEquiDog.Checked)
            {
                result |= 0x01;
            }
            if (chebEquiBear.Checked)
            {
                result |= 0x02;
            }
            return result;
        }
    }
}
