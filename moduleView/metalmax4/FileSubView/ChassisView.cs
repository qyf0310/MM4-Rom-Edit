﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class ChassisView : Form
    {
        private XlsChassis currentItem = null;
        public ChassisView()
        {
            InitializeComponent();
            init();
        }
        public void init()
        {
            List<XlsChassis> datas = DataBaseService.getData(ConstanDef.ArrFileKey_chassis) as List<XlsChassis>;
            searchTree1.loadList(datas);
            for (int i = 0; i < 50; i++)
            {
                String name = DataBaseService.getTankAbility(i);
                cbbAbility.Items.Add(name);

            }
            for (int i = 0; i < 99; i++)
            {
                String name = DataBaseService.getChassisSeriesName(i);
                cbbType.Items.Add(name);
            }
            for (int i = 0; i < 103; i++)
            {
                String name = DataBaseService.getChassisRouteList(i);
                cbbUpgradeRoute.Items.Add(name);
            }
            for (int i = 0; i < 703; i++)
            {
                String name = "";
                if (i == 0)
                {
                    name = "未指定";
                }
                else if (i<=79 || i==148)
                {
                    name=String.Format("自由主炮-{0:X4}",i);
                }
                else if(i<=111 || i==146 || i==147)
                {
                    name = String.Format("自由副炮-{0:X4}", i);
                }
                else if (i <= 145)
                {
                    name = String.Format("自由SE-{0:X4}", i);
                }
                else
                {
                    name = String.Format("{0}-{1:X4}",DataBaseService.getChassisFixedWeapone(i), i);
                }
                cbbHole1.Items.Add(name);
                cbbHole2.Items.Add(name);
                cbbHole3.Items.Add(name);
                cbbHole4.Items.Add(name);
                cbbHole5.Items.Add(name);
                cbbHole6.Items.Add(name);
            }
            cbbHole6.Enabled = false;
        }
        private void searchTree1_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree)sender;
            if (control.TreeView.SelectedNode != null)
            {
                loadItem(Convert.ToInt32(control.TreeView.SelectedNode.Tag));
            }
        }
        private void loadItem(int index)
        {
            try
            {
                List<XlsChassis> datas = DataBaseService.getData(ConstanDef.ArrFileKey_chassis) as List<XlsChassis>;
                XlsChassis o = datas[index];
                currentItem = o;
                tbName.Text = o.Name;
                tbRomAddr.Text = String.Format("{0:X8}",
                    Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_chassis)["data_offset"].StringValue, 16) + o.Offset);
                StringBuilder sb = new StringBuilder();
                foreach (byte b in o.Data)
                {
                    sb.Append(String.Format("{0:X2} ", b));
                }
                tbRomData.Text = sb.ToString();


                cbbUpgradeRoute.SelectedIndex = o.Remodel;
                ncLev.Value = o.Level;
                cbbType.SelectedIndex = o.Type;
                chebDC.Checked = o.DoubleC;
                chebDE.Checked = o.DoubleE;
                ncStorage.Value = Math.Min(o.Storage, 18);
                cbbAbility.SelectedIndex = o.Ability;

                tbCode.Text = String.Format("{0:D2}->{1:D2}", o.Type,o.Remodel);

                cbbHole1.SelectedIndex = o.Hole1;
                cbbHole2.SelectedIndex = o.Hole2;
                cbbHole3.SelectedIndex = o.Hole3;
                cbbHole4.SelectedIndex = o.Hole4;
                cbbHole5.SelectedIndex = o.Hole5;
                cbbHole6.SelectedIndex = o.Hole6;
                refreshUpgradeList(o.Remodel);
            }
            catch(Exception e)
            {
                MessageBox.Show(String.Format("加载失败," + e.Message));
            }

        }

        private void btnSaveToRom_Click(object sender, EventArgs e)
        {




            byte[] bytes = currentItem.Data.ToArray();

            int hole = cbbHole1.SelectedIndex;
            bytes[0] = (byte)(hole % 256);
            bytes[1] = (byte)(hole / 256);

            hole = cbbHole2.SelectedIndex;
            bytes[2] = (byte)(hole  % 256);
            bytes[3] = (byte)(hole  / 256);

            hole = cbbHole3.SelectedIndex;
            bytes[4] = (byte)(hole % 256);
            bytes[5] = (byte)(hole / 256);

            hole = cbbHole4.SelectedIndex;
            bytes[6] = (byte)(hole % 256);
            bytes[7] = (byte)(hole / 256);

            hole = cbbHole5.SelectedIndex;
            bytes[8] = (byte)(hole % 256);
            bytes[9] = (byte)(hole / 256);

            hole = cbbHole6.SelectedIndex;
            bytes[10] = (byte)(hole % 256);
            bytes[11] = (byte)(hole / 256);
            bytes[12] = (byte)cbbUpgradeRoute.SelectedIndex;
            bytes[13] = (byte)cbbType.SelectedIndex;
            bytes[14] = (byte)ncLev.Value;
            bytes[15] = (byte)ncStorage.Value;
            bytes[16] = (byte) (chebDE.Checked ? 1 : 0);
            bytes[17] = (byte)(chebDC.Checked ? 1 : 0);
            bytes[18] = (byte)cbbAbility.SelectedIndex;

            try
            {
                byte[] newBytes = FileOperateHelper.modifyBin(DataBaseService.getRomPath(), Convert.ToInt32(tbRomAddr.Text, 16), bytes);
                XlsChassis newO = XlsChassisArrDecode.loadData(newBytes, "edit")[0];
                
                
                List<XlsChassis> datas = DataBaseService.getData(ConstanDef.ArrFileKey_chassis) as List<XlsChassis>;
                int index = Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag);
                XlsChassis oldO = datas[index];
                newO.Name = oldO.Name;
                newO.SeqNo = oldO.SeqNo;
                newO.Offset = oldO.Offset;
                datas[index] = newO;
                loadItem(index);
                MessageBox.Show("写入完毕");
            }
            catch (Exception exception)
            {
                MessageBox.Show("写入失败," + exception.Message);
            }
        }

        private void cbbUpgrade_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshUpgradeList(Convert.ToInt32(cbbUpgradeRoute.SelectedIndex));
        }
        private void refreshUpgradeList(int code)
        {
            lvData_upgrade.Items.Clear();
            if(code == 0)
            {
                return;
            }
            List<XlsChassis> datas = DataBaseService.getData(ConstanDef.ArrFileKey_chassis) as List<XlsChassis>;
            List<XlsChassis> similars = datas.FindAll(x => x.Type == code).ToList();
            foreach (XlsChassis w in similars)
            {
                //ListViewItem item = new ListViewItem(w.Name);
                //lvData_upgrade.Items.Add(w.Type.ToString());
                //lvData_upgrade.Items.Add(w.Remodel.ToString());
                //XlsChassis next = datas.FirstOrDefault(x => x.Type == w.Remodel);
                //lvData_upgrade.Items.Add(next == null ? "" : next.Name);

            }
        }
    }
}
