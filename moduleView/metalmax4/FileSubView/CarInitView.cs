﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class CarInitView : Form
    {
        private XlsCarInit currentItem = null;
        public CarInitView()
        {
            InitializeComponent();
            init();
        }
        public void init()
        {
            List<XlsCarInit> datas = DataBaseService.getData(ConstanDef.ArrFileKey_carinit) as List<XlsCarInit>;
            searchTree1.loadList(datas);

            List<XlsItemList> allItems = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            List<XlsItemList> chassisList = allItems.Where(x=>x.TypeId==11).ToList();
            List<XlsItemList> cunitList = allItems.Where(x => x.TypeId == 13).ToList();
            List<XlsItemList> enginList = allItems.Where(x => x.TypeId == 12).ToList();
            List<XlsItemList> hole1List = allItems.Where(x => x.TypeId >= 14 && x.TypeId <= 19).ToList();
            List<XlsItemList> hole2List = allItems.Where(x => x.TypeId >= 14 && x.TypeId <= 19).ToList();
            List<XlsItemList> hole3List = allItems.Where(x => x.TypeId >= 14 && x.TypeId <= 19).ToList();
            List<XlsItemList> hole4List = allItems.Where(x => x.TypeId >= 14 && x.TypeId <= 19).ToList();
            List<XlsItemList> hole5List = allItems.Where(x => x.TypeId >= 14 && x.TypeId <= 19).ToList();
            List<XlsItemList> hole6List = allItems.Where(x => x.TypeId >= 12 && x.TypeId <= 19).ToList();

            List<XlsItemList> item1List = allItems.Where(x => x.TypeId >= 20 && x.TypeId <= 26).ToList();
            List<XlsItemList> item2List = allItems.Where(x => x.TypeId >= 20 && x.TypeId <= 26).ToList();
            List<XlsItemList> item3List = allItems.Where(x => x.TypeId >= 20 && x.TypeId <= 26).ToList();
            List<XlsItemList> item4List = allItems.Where(x => x.TypeId >= 20 && x.TypeId <= 26).ToList();
            List<XlsItemList> item5List = allItems.Where(x => x.TypeId >= 20 && x.TypeId <= 26).ToList();
            cbbChassis.DataSource = chassisList;
            cbbChassis.DisplayMember = "Name";
            cbbChassis.ValueMember = "SeqNo";

            cbbCunit.DataSource = cunitList;
            cbbCunit.DisplayMember = "Name";
            cbbCunit.ValueMember = "SeqNo";

            cbbEngin.DataSource = enginList;
            cbbEngin.DisplayMember = "Name";
            cbbEngin.ValueMember = "SeqNo";

            cbbHole1.DataSource = hole1List;cbbHole1.DisplayMember = "Name";cbbHole1.ValueMember = "SeqNo";
            cbbHole2.DataSource = hole2List; cbbHole2.DisplayMember = "Name"; cbbHole2.ValueMember = "SeqNo";
            cbbHole3.DataSource = hole3List; cbbHole3.DisplayMember = "Name"; cbbHole3.ValueMember = "SeqNo";
            cbbHole4.DataSource = hole4List; cbbHole4.DisplayMember = "Name"; cbbHole4.ValueMember = "SeqNo";
            cbbHole5.DataSource = hole5List; cbbHole5.DisplayMember = "Name"; cbbHole5.ValueMember = "SeqNo";
            cbbHole6.DataSource = hole6List; cbbHole6.DisplayMember = "Name"; cbbHole6.ValueMember = "SeqNo";

            cbbItem1.DataSource = item1List; cbbItem1.DisplayMember = "Name"; cbbItem1.ValueMember = "SeqNo";
            cbbItem2.DataSource = item2List; cbbItem2.DisplayMember = "Name"; cbbItem2.ValueMember = "SeqNo";
            cbbItem3.DataSource = item3List; cbbItem3.DisplayMember = "Name"; cbbItem3.ValueMember = "SeqNo";
            cbbItem4.DataSource = item4List; cbbItem4.DisplayMember = "Name"; cbbItem4.ValueMember = "SeqNo";
            cbbItem5.DataSource = item5List; cbbItem5.DisplayMember = "Name"; cbbItem5.ValueMember = "SeqNo";
        }

        private void searchTree1_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree)sender;
            if (control.TreeView.SelectedNode != null)
            {
                loadItem(Convert.ToInt32(control.TreeView.SelectedNode.Tag));
            }
        }
        private void loadItem(int index)
        {
            List<XlsCarInit> datas = DataBaseService.getData(ConstanDef.ArrFileKey_carinit) as List<XlsCarInit>;
            XlsCarInit o = datas[index];
            currentItem = o;
            tbName.Text = o.Name;
            tbRomAddr.Text = String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_carinit)["data_offset"].StringValue, 16) + o.Offset);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in o.Data)
            {
                sb.Append(String.Format("{0:X2} ", b));
            }
            tbRomData.Text = sb.ToString();

            tbNameAddr.Text= String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_carinit)["name_offset"].StringValue, 16) + o.NameOffAddr);
            tbUnknown.Text = o.Unknown;

            
            ncSP.Value = o.Sp;
            cbbChassis.SelectedValue = o.ChassisIdx - 1;
            cbbCunit.SelectedValue = o.CunitIdx - 1;
            cbbEngin.SelectedValue = o.EnginIdx - 1;



            cbbHole1.SelectedValue = o.Hole1Idx - 1;
            cbbHole2.SelectedValue = o.Hole2Idx - 1;
            cbbHole3.SelectedValue = o.Hole3Idx - 1;
            cbbHole4.SelectedValue = o.Hole4Idx - 1;
            cbbHole5.SelectedValue = o.Hole5Idx - 1;
            cbbHole6.SelectedValue = o.Hole6Idx - 1;

            cbbItem1.SelectedValue = o.Item1Idx - 1;
            cbbItem2.SelectedValue = o.Item2Idx - 1;
            cbbItem3.SelectedValue = o.Item3Idx - 1;
            cbbItem4.SelectedValue = o.Item4Idx - 1;
            cbbItem5.SelectedValue = o.Item5Idx - 1;
        }

        private void btnSaveToRom_Click(object sender, EventArgs e)
        {
            byte[] bytes = currentItem.Data.ToArray();
            int selValue = cbbChassis.SelectedValue == null ? -1 : (int) cbbChassis.SelectedValue;
            bytes[2] = (byte)((selValue+1) % 256);
            bytes[3] = (byte)((selValue + 1) / 256);

            selValue = cbbCunit.SelectedValue==null? -1 : (int)cbbCunit.SelectedValue;
            bytes[4] =(byte)((selValue + 1) % 256);
            bytes[5] = (byte)((selValue + 1) / 256);

            selValue = cbbEngin.SelectedValue == null ? -1 : (int)cbbEngin.SelectedValue;
            bytes[6] = (byte)((selValue + 1) % 256);
            bytes[7] = (byte)((selValue + 1) / 256);

            selValue = cbbHole1.SelectedValue == null ? -1 : (int)cbbHole1.SelectedValue;
            bytes[8] = (byte)((selValue + 1) % 256);
            bytes[9] = (byte)((selValue + 1) / 256);

            selValue = cbbHole2.SelectedValue == null ? -1 : (int)cbbHole2.SelectedValue;
            bytes[10] = (byte)((selValue + 1) % 256);
            bytes[11] = (byte)((selValue + 1) / 256);

            selValue = cbbHole3.SelectedValue == null ? -1 : (int)cbbHole3.SelectedValue;
            bytes[12] = (byte)((selValue + 1) % 256);
            bytes[13] = (byte)((selValue + 1) / 256);

            selValue = cbbHole4.SelectedValue == null ? -1 : (int)cbbHole4.SelectedValue;
            bytes[14] = (byte)((selValue + 1) % 256);
            bytes[15] = (byte)((selValue + 1) / 256);

            selValue = cbbHole5.SelectedValue == null ? -1 : (int)cbbHole5.SelectedValue;
            bytes[16] = (byte)((selValue + 1) % 256);
            bytes[17] = (byte)((selValue + 1) / 256);

            selValue = cbbHole6.SelectedValue == null ? -1 : (int)cbbHole6.SelectedValue;
            bytes[18] = (byte)((selValue + 1) % 256);
            bytes[19] = (byte)((selValue + 1) / 256);

            selValue = cbbItem1.SelectedValue == null ? -1 : (int)cbbItem1.SelectedValue;
            bytes[20] = (byte)((selValue + 1) % 256);
            bytes[21] = (byte)((selValue + 1) / 256);

            selValue = cbbItem2.SelectedValue == null ? -1 : (int)cbbItem2.SelectedValue;
            bytes[22] = (byte)((selValue + 1) % 256);
            bytes[23] = (byte)((selValue + 1) / 256);

            selValue = cbbItem3.SelectedValue == null ? -1 : (int)cbbItem3.SelectedValue;
            bytes[24] = (byte)((selValue + 1) % 256);
            bytes[25] = (byte)((selValue + 1) / 256);

            selValue = cbbItem4.SelectedValue == null ? -1 : (int)cbbItem4.SelectedValue;
            bytes[26] = (byte)((selValue + 1) % 256);
            bytes[27] = (byte)((selValue + 1) / 256);

            selValue = cbbItem5.SelectedValue == null ? -1 : (int)cbbItem5.SelectedValue;
            bytes[28] = (byte)((selValue + 1) % 256);
            bytes[29] = (byte)((selValue + 1) / 256);

            bytes[38] = (byte)((ncSP.Value) % 256);
            bytes[39] = (byte)((ncSP.Value) / 256);

            try
            {
               byte[] newBytes= FileOperateHelper.modifyBin(DataBaseService.getRomPath(), Convert.ToInt32(tbRomAddr.Text, 16), bytes);
               XlsCarInit newO= XlsCarInitArrDecode.loadData(newBytes, "edit")[0];
                List<XlsCarInit> datas = DataBaseService.getData(ConstanDef.ArrFileKey_carinit) as List<XlsCarInit>;
                int index = Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag);
                XlsCarInit oldO = datas[index];
                newO.Offset = oldO.Offset;
                newO.SeqNo = oldO.SeqNo;
                datas[index] = newO;
                loadItem(index);
                MessageBox.Show("写入完毕" );
            }
            catch (Exception exception)
            {
                MessageBox.Show("写入失败," + exception.Message);
            }
        }
    }
}
