﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsNingenBougu : ModelBase
    {
        private int seqNo;
        private String name;

        private int male;
        private int female;
        private int animal;
        private int unknown1;
        private int attack;
        private int defence;
        private int speed;
        private int man;
        private int unknown2;
        private int fireR;
        private int iceR;
        private int electriR;
        private int gasR;
        private int laserR;
        private int soundR;
        private int unknown3;
        private String typeName;

        private List<byte> data = new List<byte>();
        private long offset = 0;

        public override string getText()
        {
            return name;
        }

        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Male
        {
            get { return male; }
            set { male = value; }
        }

        public int Female
        {
            get { return female; }
            set { female = value; }
        }

        public int Animal
        {
            get { return animal; }
            set { animal = value; }
        }

        public int Unknown1
        {
            get { return unknown1; }
            set { unknown1 = value; }
        }

        public int Attack
        {
            get { return attack; }
            set { attack = value; }
        }

        public int Defence
        {
            get { return defence; }
            set { defence = value; }
        }

        public int Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public int Man
        {
            get { return man; }
            set { man = value; }
        }

        public int Unknown2
        {
            get { return unknown2; }
            set { unknown2 = value; }
        }

        public int FireR
        {
            get { return fireR; }
            set { fireR = value; }
        }

        public int IceR
        {
            get { return iceR; }
            set { iceR = value; }
        }

        public int ElectriR
        {
            get { return electriR; }
            set { electriR = value; }
        }

        public int GasR
        {
            get { return gasR; }
            set { gasR = value; }
        }

        public int LaserR
        {
            get { return laserR; }
            set { laserR = value; }
        }

        public int SoundR
        {
            get { return soundR; }
            set { soundR = value; }
        }

        public int Unknown3
        {
            get { return unknown3; }
            set { unknown3 = value; }
        }

        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
