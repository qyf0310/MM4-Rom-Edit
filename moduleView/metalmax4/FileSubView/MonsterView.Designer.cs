﻿using MM4RomEdit.Controls;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    partial class MonsterView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbNameAddr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbAppearance = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ncPAR = new MM4RomEdit.Controls.NumericControl();
            this.ncHIT = new MM4RomEdit.Controls.NumericControl();
            this.ncSPD = new MM4RomEdit.Controls.NumericControl();
            this.ncDEF = new MM4RomEdit.Controls.NumericControl();
            this.ncATK = new MM4RomEdit.Controls.NumericControl();
            this.ncHP = new MM4RomEdit.Controls.NumericControl();
            this.ncMoney = new MM4RomEdit.Controls.NumericControl();
            this.ncExp = new MM4RomEdit.Controls.NumericControl();
            this.ncLev = new MM4RomEdit.Controls.NumericControl();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ncUnknowP2 = new MM4RomEdit.Controls.NumericControl();
            this.ncLaser = new MM4RomEdit.Controls.NumericControl();
            this.ncGas = new MM4RomEdit.Controls.NumericControl();
            this.ncSound = new MM4RomEdit.Controls.NumericControl();
            this.ncElectri = new MM4RomEdit.Controls.NumericControl();
            this.ncIce = new MM4RomEdit.Controls.NumericControl();
            this.ncFire = new MM4RomEdit.Controls.NumericControl();
            this.ncNormal = new MM4RomEdit.Controls.NumericControl();
            this.ncUnknowP1 = new MM4RomEdit.Controls.NumericControl();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tbUKAct7 = new System.Windows.Forms.TextBox();
            this.tbUKAct6 = new System.Windows.Forms.TextBox();
            this.tbUKAct5 = new System.Windows.Forms.TextBox();
            this.tbUKAct4 = new System.Windows.Forms.TextBox();
            this.tbUKAct3 = new System.Windows.Forms.TextBox();
            this.tbUKAct2 = new System.Windows.Forms.TextBox();
            this.tbUKAct1 = new System.Windows.Forms.TextBox();
            this.ncAct7 = new MM4RomEdit.Controls.NumericControl();
            this.ncAct6 = new MM4RomEdit.Controls.NumericControl();
            this.ncAct5 = new MM4RomEdit.Controls.NumericControl();
            this.ncAct4 = new MM4RomEdit.Controls.NumericControl();
            this.ncAct3 = new MM4RomEdit.Controls.NumericControl();
            this.ncAct2 = new MM4RomEdit.Controls.NumericControl();
            this.ncAct1 = new MM4RomEdit.Controls.NumericControl();
            this.label42 = new System.Windows.Forms.Label();
            this.cbbAct7 = new System.Windows.Forms.ComboBox();
            this.cbbAct6 = new System.Windows.Forms.ComboBox();
            this.cbbAct5 = new System.Windows.Forms.ComboBox();
            this.cbbAct4 = new System.Windows.Forms.ComboBox();
            this.cbbAct3 = new System.Windows.Forms.ComboBox();
            this.cbbAct2 = new System.Windows.Forms.ComboBox();
            this.cbbAct1 = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.ncDrop4 = new MM4RomEdit.Controls.NumericControl();
            this.cbbDrop4 = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.ncDrop3 = new MM4RomEdit.Controls.NumericControl();
            this.cbbDrop3 = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.ncDrop2 = new MM4RomEdit.Controls.NumericControl();
            this.cbbDrop2 = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.ncDrop1 = new MM4RomEdit.Controls.NumericControl();
            this.cbbDrop1 = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tbRomAddr = new System.Windows.Forms.TextBox();
            this.btnSaveToRom = new System.Windows.Forms.Button();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.tbRomData = new System.Windows.Forms.TextBox();
            this.ncActionTimes = new MM4RomEdit.Controls.NumericControl();
            this.searchTree1 = new MM4RomEdit.Controls.SearchTree();
            this.label44 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncPAR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncHIT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncSPD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncATK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncHP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncExp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncLev)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncUnknowP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncLaser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncGas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncSound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncElectri)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncIce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncFire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncNormal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncUnknowP1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct1)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncDrop4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDrop3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDrop2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDrop1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncActionTimes)).BeginInit();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(211, 6);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(100, 21);
            this.tbName.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(164, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "名称：";
            // 
            // tbNameAddr
            // 
            this.tbNameAddr.Location = new System.Drawing.Point(388, 6);
            this.tbNameAddr.Name = "tbNameAddr";
            this.tbNameAddr.ReadOnly = true;
            this.tbNameAddr.Size = new System.Drawing.Size(100, 21);
            this.tbNameAddr.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(317, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "名称地址：";
            // 
            // tbAppearance
            // 
            this.tbAppearance.Location = new System.Drawing.Point(565, 6);
            this.tbAppearance.Name = "tbAppearance";
            this.tbAppearance.ReadOnly = true;
            this.tbAppearance.Size = new System.Drawing.Size(100, 21);
            this.tbAppearance.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(494, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 11;
            this.label2.Text = "贴图编号：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ncPAR);
            this.groupBox1.Controls.Add(this.ncHIT);
            this.groupBox1.Controls.Add(this.ncSPD);
            this.groupBox1.Controls.Add(this.ncDEF);
            this.groupBox1.Controls.Add(this.ncATK);
            this.groupBox1.Controls.Add(this.ncHP);
            this.groupBox1.Controls.Add(this.ncMoney);
            this.groupBox1.Controls.Add(this.ncExp);
            this.groupBox1.Controls.Add(this.ncLev);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(166, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(145, 261);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "属性";
            // 
            // ncPAR
            // 
            this.ncPAR.Location = new System.Drawing.Point(53, 229);
            this.ncPAR.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncPAR.MaxNumberControl = null;
            this.ncPAR.MinNumberControl = null;
            this.ncPAR.Name = "ncPAR";
            this.ncPAR.Size = new System.Drawing.Size(86, 21);
            this.ncPAR.TabIndex = 48;
            this.ncPAR.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncHIT
            // 
            this.ncHIT.Location = new System.Drawing.Point(53, 202);
            this.ncHIT.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncHIT.MaxNumberControl = null;
            this.ncHIT.MinNumberControl = null;
            this.ncHIT.Name = "ncHIT";
            this.ncHIT.Size = new System.Drawing.Size(86, 21);
            this.ncHIT.TabIndex = 47;
            this.ncHIT.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncSPD
            // 
            this.ncSPD.Location = new System.Drawing.Point(53, 175);
            this.ncSPD.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncSPD.MaxNumberControl = null;
            this.ncSPD.MinNumberControl = null;
            this.ncSPD.Name = "ncSPD";
            this.ncSPD.Size = new System.Drawing.Size(86, 21);
            this.ncSPD.TabIndex = 46;
            this.ncSPD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncDEF
            // 
            this.ncDEF.Location = new System.Drawing.Point(53, 148);
            this.ncDEF.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncDEF.MaxNumberControl = null;
            this.ncDEF.MinNumberControl = null;
            this.ncDEF.Name = "ncDEF";
            this.ncDEF.Size = new System.Drawing.Size(86, 21);
            this.ncDEF.TabIndex = 45;
            this.ncDEF.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncATK
            // 
            this.ncATK.Location = new System.Drawing.Point(53, 121);
            this.ncATK.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncATK.MaxNumberControl = null;
            this.ncATK.MinNumberControl = null;
            this.ncATK.Name = "ncATK";
            this.ncATK.Size = new System.Drawing.Size(86, 21);
            this.ncATK.TabIndex = 44;
            this.ncATK.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncHP
            // 
            this.ncHP.Location = new System.Drawing.Point(53, 95);
            this.ncHP.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncHP.MaxNumberControl = null;
            this.ncHP.MinNumberControl = null;
            this.ncHP.Name = "ncHP";
            this.ncHP.Size = new System.Drawing.Size(86, 21);
            this.ncHP.TabIndex = 43;
            this.ncHP.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncMoney
            // 
            this.ncMoney.Location = new System.Drawing.Point(53, 68);
            this.ncMoney.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncMoney.MaxNumberControl = null;
            this.ncMoney.MinNumberControl = null;
            this.ncMoney.Name = "ncMoney";
            this.ncMoney.Size = new System.Drawing.Size(86, 21);
            this.ncMoney.TabIndex = 42;
            this.ncMoney.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncExp
            // 
            this.ncExp.Location = new System.Drawing.Point(53, 41);
            this.ncExp.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncExp.MaxNumberControl = null;
            this.ncExp.MinNumberControl = null;
            this.ncExp.Name = "ncExp";
            this.ncExp.Size = new System.Drawing.Size(86, 21);
            this.ncExp.TabIndex = 41;
            this.ncExp.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncLev
            // 
            this.ncLev.Location = new System.Drawing.Point(53, 14);
            this.ncLev.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncLev.MaxNumberControl = null;
            this.ncLev.MinNumberControl = null;
            this.ncLev.Name = "ncLev";
            this.ncLev.Size = new System.Drawing.Size(86, 21);
            this.ncLev.TabIndex = 40;
            this.ncLev.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 233);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 30;
            this.label12.Text = "回避：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 206);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 28;
            this.label11.Text = "命中：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 179);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 26;
            this.label10.Text = "速度：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 24;
            this.label9.Text = "防御：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 125);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 22;
            this.label8.Text = "攻击：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 20;
            this.label7.Text = "HP：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 71);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 18;
            this.label6.Text = "金钱：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "经验：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 14;
            this.label4.Text = "等级：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(671, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 32;
            this.label13.Text = "行动次数：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ncUnknowP2);
            this.groupBox2.Controls.Add(this.ncLaser);
            this.groupBox2.Controls.Add(this.ncGas);
            this.groupBox2.Controls.Add(this.ncSound);
            this.groupBox2.Controls.Add(this.ncElectri);
            this.groupBox2.Controls.Add(this.ncIce);
            this.groupBox2.Controls.Add(this.ncFire);
            this.groupBox2.Controls.Add(this.ncNormal);
            this.groupBox2.Controls.Add(this.ncUnknowP1);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Location = new System.Drawing.Point(319, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(145, 261);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "抗性";
            // 
            // ncUnknowP2
            // 
            this.ncUnknowP2.Location = new System.Drawing.Point(53, 229);
            this.ncUnknowP2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncUnknowP2.MaxNumberControl = null;
            this.ncUnknowP2.MinNumberControl = null;
            this.ncUnknowP2.Name = "ncUnknowP2";
            this.ncUnknowP2.Size = new System.Drawing.Size(86, 21);
            this.ncUnknowP2.TabIndex = 57;
            this.ncUnknowP2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncLaser
            // 
            this.ncLaser.Location = new System.Drawing.Point(53, 202);
            this.ncLaser.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncLaser.MaxNumberControl = null;
            this.ncLaser.MinNumberControl = null;
            this.ncLaser.Name = "ncLaser";
            this.ncLaser.Size = new System.Drawing.Size(86, 21);
            this.ncLaser.TabIndex = 56;
            this.ncLaser.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncGas
            // 
            this.ncGas.Location = new System.Drawing.Point(53, 175);
            this.ncGas.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncGas.MaxNumberControl = null;
            this.ncGas.MinNumberControl = null;
            this.ncGas.Name = "ncGas";
            this.ncGas.Size = new System.Drawing.Size(86, 21);
            this.ncGas.TabIndex = 55;
            this.ncGas.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncSound
            // 
            this.ncSound.Location = new System.Drawing.Point(53, 148);
            this.ncSound.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncSound.MaxNumberControl = null;
            this.ncSound.MinNumberControl = null;
            this.ncSound.Name = "ncSound";
            this.ncSound.Size = new System.Drawing.Size(86, 21);
            this.ncSound.TabIndex = 54;
            this.ncSound.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncElectri
            // 
            this.ncElectri.Location = new System.Drawing.Point(53, 121);
            this.ncElectri.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncElectri.MaxNumberControl = null;
            this.ncElectri.MinNumberControl = null;
            this.ncElectri.Name = "ncElectri";
            this.ncElectri.Size = new System.Drawing.Size(86, 21);
            this.ncElectri.TabIndex = 53;
            this.ncElectri.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncIce
            // 
            this.ncIce.Location = new System.Drawing.Point(53, 95);
            this.ncIce.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncIce.MaxNumberControl = null;
            this.ncIce.MinNumberControl = null;
            this.ncIce.Name = "ncIce";
            this.ncIce.Size = new System.Drawing.Size(86, 21);
            this.ncIce.TabIndex = 52;
            this.ncIce.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncFire
            // 
            this.ncFire.Location = new System.Drawing.Point(53, 69);
            this.ncFire.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncFire.MaxNumberControl = null;
            this.ncFire.MinNumberControl = null;
            this.ncFire.Name = "ncFire";
            this.ncFire.Size = new System.Drawing.Size(86, 21);
            this.ncFire.TabIndex = 51;
            this.ncFire.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncNormal
            // 
            this.ncNormal.Location = new System.Drawing.Point(53, 42);
            this.ncNormal.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncNormal.MaxNumberControl = null;
            this.ncNormal.MinNumberControl = null;
            this.ncNormal.Name = "ncNormal";
            this.ncNormal.Size = new System.Drawing.Size(86, 21);
            this.ncNormal.TabIndex = 50;
            this.ncNormal.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncUnknowP1
            // 
            this.ncUnknowP1.Location = new System.Drawing.Point(53, 14);
            this.ncUnknowP1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncUnknowP1.MaxNumberControl = null;
            this.ncUnknowP1.MinNumberControl = null;
            this.ncUnknowP1.Name = "ncUnknowP1";
            this.ncUnknowP1.Size = new System.Drawing.Size(86, 21);
            this.ncUnknowP1.TabIndex = 49;
            this.ncUnknowP1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 233);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 30;
            this.label15.Text = "酸  ：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 206);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 12);
            this.label16.TabIndex = 28;
            this.label16.Text = "激光：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 179);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 12);
            this.label17.TabIndex = 26;
            this.label17.Text = "瓦斯：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 152);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 24;
            this.label18.Text = "音波：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 125);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 12);
            this.label19.TabIndex = 22;
            this.label19.Text = "电  ：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 98);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 20;
            this.label20.Text = "冰  ：";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 71);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 12);
            this.label21.TabIndex = 18;
            this.label21.Text = "火  ：";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 44);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 16;
            this.label22.Text = "普通：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 17);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 14;
            this.label23.Text = "未知：";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.tbUKAct7);
            this.groupBox3.Controls.Add(this.tbUKAct6);
            this.groupBox3.Controls.Add(this.tbUKAct5);
            this.groupBox3.Controls.Add(this.tbUKAct4);
            this.groupBox3.Controls.Add(this.tbUKAct3);
            this.groupBox3.Controls.Add(this.tbUKAct2);
            this.groupBox3.Controls.Add(this.tbUKAct1);
            this.groupBox3.Controls.Add(this.ncAct7);
            this.groupBox3.Controls.Add(this.ncAct6);
            this.groupBox3.Controls.Add(this.ncAct5);
            this.groupBox3.Controls.Add(this.ncAct4);
            this.groupBox3.Controls.Add(this.ncAct3);
            this.groupBox3.Controls.Add(this.ncAct2);
            this.groupBox3.Controls.Add(this.ncAct1);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.cbbAct7);
            this.groupBox3.Controls.Add(this.cbbAct6);
            this.groupBox3.Controls.Add(this.cbbAct5);
            this.groupBox3.Controls.Add(this.cbbAct4);
            this.groupBox3.Controls.Add(this.cbbAct3);
            this.groupBox3.Controls.Add(this.cbbAct2);
            this.groupBox3.Controls.Add(this.cbbAct1);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Location = new System.Drawing.Point(470, 33);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(384, 218);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "攻击方式";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(63, 14);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(155, 12);
            this.label43.TabIndex = 77;
            this.label43.Text = "行为替换可能会引发战斗bug";
            // 
            // tbUKAct7
            // 
            this.tbUKAct7.Location = new System.Drawing.Point(298, 187);
            this.tbUKAct7.Name = "tbUKAct7";
            this.tbUKAct7.ReadOnly = true;
            this.tbUKAct7.Size = new System.Drawing.Size(80, 21);
            this.tbUKAct7.TabIndex = 76;
            this.tbUKAct7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbUKAct6
            // 
            this.tbUKAct6.Location = new System.Drawing.Point(298, 163);
            this.tbUKAct6.Name = "tbUKAct6";
            this.tbUKAct6.ReadOnly = true;
            this.tbUKAct6.Size = new System.Drawing.Size(80, 21);
            this.tbUKAct6.TabIndex = 75;
            this.tbUKAct6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbUKAct5
            // 
            this.tbUKAct5.Location = new System.Drawing.Point(298, 136);
            this.tbUKAct5.Name = "tbUKAct5";
            this.tbUKAct5.ReadOnly = true;
            this.tbUKAct5.Size = new System.Drawing.Size(80, 21);
            this.tbUKAct5.TabIndex = 74;
            this.tbUKAct5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbUKAct4
            // 
            this.tbUKAct4.Location = new System.Drawing.Point(298, 111);
            this.tbUKAct4.Name = "tbUKAct4";
            this.tbUKAct4.ReadOnly = true;
            this.tbUKAct4.Size = new System.Drawing.Size(80, 21);
            this.tbUKAct4.TabIndex = 73;
            this.tbUKAct4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbUKAct3
            // 
            this.tbUKAct3.Location = new System.Drawing.Point(298, 83);
            this.tbUKAct3.Name = "tbUKAct3";
            this.tbUKAct3.ReadOnly = true;
            this.tbUKAct3.Size = new System.Drawing.Size(80, 21);
            this.tbUKAct3.TabIndex = 72;
            this.tbUKAct3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbUKAct2
            // 
            this.tbUKAct2.Location = new System.Drawing.Point(298, 59);
            this.tbUKAct2.Name = "tbUKAct2";
            this.tbUKAct2.ReadOnly = true;
            this.tbUKAct2.Size = new System.Drawing.Size(80, 21);
            this.tbUKAct2.TabIndex = 71;
            this.tbUKAct2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbUKAct1
            // 
            this.tbUKAct1.Location = new System.Drawing.Point(298, 32);
            this.tbUKAct1.Name = "tbUKAct1";
            this.tbUKAct1.ReadOnly = true;
            this.tbUKAct1.Size = new System.Drawing.Size(80, 21);
            this.tbUKAct1.TabIndex = 57;
            this.tbUKAct1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ncAct7
            // 
            this.ncAct7.Location = new System.Drawing.Point(248, 187);
            this.ncAct7.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncAct7.MaxNumberControl = null;
            this.ncAct7.MinNumberControl = null;
            this.ncAct7.Name = "ncAct7";
            this.ncAct7.Size = new System.Drawing.Size(43, 21);
            this.ncAct7.TabIndex = 70;
            this.ncAct7.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncAct6
            // 
            this.ncAct6.Location = new System.Drawing.Point(248, 162);
            this.ncAct6.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncAct6.MaxNumberControl = null;
            this.ncAct6.MinNumberControl = null;
            this.ncAct6.Name = "ncAct6";
            this.ncAct6.Size = new System.Drawing.Size(43, 21);
            this.ncAct6.TabIndex = 69;
            this.ncAct6.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncAct5
            // 
            this.ncAct5.Location = new System.Drawing.Point(248, 135);
            this.ncAct5.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncAct5.MaxNumberControl = null;
            this.ncAct5.MinNumberControl = null;
            this.ncAct5.Name = "ncAct5";
            this.ncAct5.Size = new System.Drawing.Size(43, 21);
            this.ncAct5.TabIndex = 68;
            this.ncAct5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncAct4
            // 
            this.ncAct4.Location = new System.Drawing.Point(248, 111);
            this.ncAct4.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncAct4.MaxNumberControl = null;
            this.ncAct4.MinNumberControl = null;
            this.ncAct4.Name = "ncAct4";
            this.ncAct4.Size = new System.Drawing.Size(43, 21);
            this.ncAct4.TabIndex = 67;
            this.ncAct4.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncAct3
            // 
            this.ncAct3.Location = new System.Drawing.Point(248, 84);
            this.ncAct3.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncAct3.MaxNumberControl = null;
            this.ncAct3.MinNumberControl = null;
            this.ncAct3.Name = "ncAct3";
            this.ncAct3.Size = new System.Drawing.Size(43, 21);
            this.ncAct3.TabIndex = 66;
            this.ncAct3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncAct2
            // 
            this.ncAct2.Location = new System.Drawing.Point(248, 59);
            this.ncAct2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncAct2.MaxNumberControl = null;
            this.ncAct2.MinNumberControl = null;
            this.ncAct2.Name = "ncAct2";
            this.ncAct2.Size = new System.Drawing.Size(43, 21);
            this.ncAct2.TabIndex = 65;
            this.ncAct2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncAct1
            // 
            this.ncAct1.Location = new System.Drawing.Point(248, 32);
            this.ncAct1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncAct1.MaxNumberControl = null;
            this.ncAct1.MinNumberControl = null;
            this.ncAct1.Name = "ncAct1";
            this.ncAct1.Size = new System.Drawing.Size(43, 21);
            this.ncAct1.TabIndex = 57;
            this.ncAct1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(305, 14);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(53, 12);
            this.label42.TabIndex = 64;
            this.label42.Text = "其它参数";
            // 
            // cbbAct7
            // 
            this.cbbAct7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAct7.FormattingEnabled = true;
            this.cbbAct7.Location = new System.Drawing.Point(48, 188);
            this.cbbAct7.Name = "cbbAct7";
            this.cbbAct7.Size = new System.Drawing.Size(180, 20);
            this.cbbAct7.TabIndex = 63;
            // 
            // cbbAct6
            // 
            this.cbbAct6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAct6.FormattingEnabled = true;
            this.cbbAct6.Location = new System.Drawing.Point(48, 163);
            this.cbbAct6.Name = "cbbAct6";
            this.cbbAct6.Size = new System.Drawing.Size(180, 20);
            this.cbbAct6.TabIndex = 62;
            // 
            // cbbAct5
            // 
            this.cbbAct5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAct5.FormattingEnabled = true;
            this.cbbAct5.Location = new System.Drawing.Point(48, 136);
            this.cbbAct5.Name = "cbbAct5";
            this.cbbAct5.Size = new System.Drawing.Size(180, 20);
            this.cbbAct5.TabIndex = 61;
            // 
            // cbbAct4
            // 
            this.cbbAct4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAct4.FormattingEnabled = true;
            this.cbbAct4.Location = new System.Drawing.Point(48, 110);
            this.cbbAct4.Name = "cbbAct4";
            this.cbbAct4.Size = new System.Drawing.Size(180, 20);
            this.cbbAct4.TabIndex = 60;
            // 
            // cbbAct3
            // 
            this.cbbAct3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAct3.FormattingEnabled = true;
            this.cbbAct3.Location = new System.Drawing.Point(48, 83);
            this.cbbAct3.Name = "cbbAct3";
            this.cbbAct3.Size = new System.Drawing.Size(180, 20);
            this.cbbAct3.TabIndex = 59;
            // 
            // cbbAct2
            // 
            this.cbbAct2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAct2.FormattingEnabled = true;
            this.cbbAct2.Location = new System.Drawing.Point(48, 58);
            this.cbbAct2.Name = "cbbAct2";
            this.cbbAct2.Size = new System.Drawing.Size(180, 20);
            this.cbbAct2.TabIndex = 58;
            // 
            // cbbAct1
            // 
            this.cbbAct1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAct1.FormattingEnabled = true;
            this.cbbAct1.Location = new System.Drawing.Point(48, 32);
            this.cbbAct1.Name = "cbbAct1";
            this.cbbAct1.Size = new System.Drawing.Size(180, 20);
            this.cbbAct1.TabIndex = 52;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(234, 14);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(65, 12);
            this.label41.TabIndex = 57;
            this.label41.Text = "次数?0不限";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 191);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(47, 12);
            this.label40.TabIndex = 26;
            this.label40.Text = "行为7：";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 166);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 12);
            this.label27.TabIndex = 25;
            this.label27.Text = "行为6：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 139);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(47, 12);
            this.label26.TabIndex = 22;
            this.label26.Text = "行为5：";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 113);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(47, 12);
            this.label25.TabIndex = 20;
            this.label25.Text = "行为4：";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 86);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 12);
            this.label24.TabIndex = 18;
            this.label24.Text = "行为3：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 61);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 12);
            this.label14.TabIndex = 16;
            this.label14.Text = "行为2：";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 35);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(47, 12);
            this.label31.TabIndex = 14;
            this.label31.Text = "行为1：";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label35);
            this.groupBox4.Controls.Add(this.ncDrop4);
            this.groupBox4.Controls.Add(this.cbbDrop4);
            this.groupBox4.Controls.Add(this.label36);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.ncDrop3);
            this.groupBox4.Controls.Add(this.cbbDrop3);
            this.groupBox4.Controls.Add(this.label34);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.ncDrop2);
            this.groupBox4.Controls.Add(this.cbbDrop2);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.ncDrop1);
            this.groupBox4.Controls.Add(this.cbbDrop1);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Location = new System.Drawing.Point(166, 331);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(298, 123);
            this.groupBox4.TabIndex = 38;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "掉落";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(273, 98);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(11, 12);
            this.label35.TabIndex = 51;
            this.label35.Text = "%";
            // 
            // ncDrop4
            // 
            this.ncDrop4.Location = new System.Drawing.Point(222, 94);
            this.ncDrop4.MaxNumberControl = null;
            this.ncDrop4.MinNumberControl = null;
            this.ncDrop4.Name = "ncDrop4";
            this.ncDrop4.Size = new System.Drawing.Size(45, 21);
            this.ncDrop4.TabIndex = 50;
            this.ncDrop4.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbbDrop4
            // 
            this.cbbDrop4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbDrop4.FormattingEnabled = true;
            this.cbbDrop4.Location = new System.Drawing.Point(53, 95);
            this.cbbDrop4.Name = "cbbDrop4";
            this.cbbDrop4.Size = new System.Drawing.Size(163, 20);
            this.cbbDrop4.TabIndex = 49;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 98);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(29, 12);
            this.label36.TabIndex = 48;
            this.label36.Text = "本周";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(273, 71);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(11, 12);
            this.label33.TabIndex = 47;
            this.label33.Text = "%";
            // 
            // ncDrop3
            // 
            this.ncDrop3.Location = new System.Drawing.Point(222, 67);
            this.ncDrop3.MaxNumberControl = null;
            this.ncDrop3.MinNumberControl = null;
            this.ncDrop3.Name = "ncDrop3";
            this.ncDrop3.Size = new System.Drawing.Size(45, 21);
            this.ncDrop3.TabIndex = 46;
            this.ncDrop3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbbDrop3
            // 
            this.cbbDrop3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbDrop3.FormattingEnabled = true;
            this.cbbDrop3.Location = new System.Drawing.Point(53, 68);
            this.cbbDrop3.Name = "cbbDrop3";
            this.cbbDrop3.Size = new System.Drawing.Size(163, 20);
            this.cbbDrop3.TabIndex = 45;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 71);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(11, 12);
            this.label34.TabIndex = 44;
            this.label34.Text = "3";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(273, 45);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(11, 12);
            this.label30.TabIndex = 43;
            this.label30.Text = "%";
            // 
            // ncDrop2
            // 
            this.ncDrop2.Location = new System.Drawing.Point(222, 41);
            this.ncDrop2.MaxNumberControl = null;
            this.ncDrop2.MinNumberControl = null;
            this.ncDrop2.Name = "ncDrop2";
            this.ncDrop2.Size = new System.Drawing.Size(45, 21);
            this.ncDrop2.TabIndex = 42;
            this.ncDrop2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbbDrop2
            // 
            this.cbbDrop2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbDrop2.FormattingEnabled = true;
            this.cbbDrop2.Location = new System.Drawing.Point(53, 42);
            this.cbbDrop2.Name = "cbbDrop2";
            this.cbbDrop2.Size = new System.Drawing.Size(163, 20);
            this.cbbDrop2.TabIndex = 41;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 45);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(11, 12);
            this.label32.TabIndex = 40;
            this.label32.Text = "2";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(273, 19);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(11, 12);
            this.label29.TabIndex = 39;
            this.label29.Text = "%";
            // 
            // ncDrop1
            // 
            this.ncDrop1.Location = new System.Drawing.Point(222, 15);
            this.ncDrop1.MaxNumberControl = null;
            this.ncDrop1.MinNumberControl = null;
            this.ncDrop1.Name = "ncDrop1";
            this.ncDrop1.Size = new System.Drawing.Size(45, 21);
            this.ncDrop1.TabIndex = 17;
            this.ncDrop1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbbDrop1
            // 
            this.cbbDrop1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbDrop1.FormattingEnabled = true;
            this.cbbDrop1.Location = new System.Drawing.Point(53, 16);
            this.cbbDrop1.Name = "cbbDrop1";
            this.cbbDrop1.Size = new System.Drawing.Size(163, 20);
            this.cbbDrop1.TabIndex = 16;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 19);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(11, 12);
            this.label28.TabIndex = 15;
            this.label28.Text = "1";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(470, 266);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 12);
            this.label37.TabIndex = 50;
            this.label37.Text = "ROM地址：";
            // 
            // tbRomAddr
            // 
            this.tbRomAddr.Location = new System.Drawing.Point(535, 263);
            this.tbRomAddr.Name = "tbRomAddr";
            this.tbRomAddr.ReadOnly = true;
            this.tbRomAddr.Size = new System.Drawing.Size(124, 21);
            this.tbRomAddr.TabIndex = 51;
            this.tbRomAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSaveToRom
            // 
            this.btnSaveToRom.Location = new System.Drawing.Point(768, 263);
            this.btnSaveToRom.Name = "btnSaveToRom";
            this.btnSaveToRom.Size = new System.Drawing.Size(86, 23);
            this.btnSaveToRom.TabIndex = 54;
            this.btnSaveToRom.Text = "写入ROM";
            this.btnSaveToRom.UseVisualStyleBackColor = true;
            this.btnSaveToRom.Click += new System.EventHandler(this.btnSaveToRom_Click);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(718, 263);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(44, 21);
            this.tbLength.TabIndex = 53;
            this.tbLength.Text = "118";
            this.tbLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(671, 266);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 52;
            this.label38.Text = "长度：";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(470, 298);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 12);
            this.label39.TabIndex = 55;
            this.label39.Text = "ROM数据：";
            // 
            // tbRomData
            // 
            this.tbRomData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRomData.Location = new System.Drawing.Point(470, 313);
            this.tbRomData.Multiline = true;
            this.tbRomData.Name = "tbRomData";
            this.tbRomData.ReadOnly = true;
            this.tbRomData.Size = new System.Drawing.Size(384, 141);
            this.tbRomData.TabIndex = 56;
            // 
            // ncActionTimes
            // 
            this.ncActionTimes.Location = new System.Drawing.Point(742, 6);
            this.ncActionTimes.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncActionTimes.MaxNumberControl = null;
            this.ncActionTimes.MinNumberControl = null;
            this.ncActionTimes.Name = "ncActionTimes";
            this.ncActionTimes.Size = new System.Drawing.Size(86, 21);
            this.ncActionTimes.TabIndex = 49;
            this.ncActionTimes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // searchTree1
            // 
            this.searchTree1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTree1.Location = new System.Drawing.Point(2, 2);
            this.searchTree1.Name = "searchTree1";
            this.searchTree1.Size = new System.Drawing.Size(156, 452);
            this.searchTree1.TabIndex = 0;
            this.searchTree1.DoubleClick += new System.EventHandler(this.searchTree1_DoubleClick);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(171, 303);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(173, 12);
            this.label44.TabIndex = 57;
            this.label44.Text = "赏金首血量 = 标准血量 *10 ？";
            // 
            // MonsterView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 455);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.tbRomData);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.btnSaveToRom);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.tbRomAddr);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.ncActionTimes);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbAppearance);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbNameAddr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.searchTree1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MonsterView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "MonsterView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncPAR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncHIT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncSPD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncATK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncHP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncExp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncLev)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncUnknowP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncLaser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncGas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncSound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncElectri)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncIce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncFire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncNormal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncUnknowP1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAct1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncDrop4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDrop3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDrop2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDrop1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncActionTimes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.SearchTree searchTree1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbNameAddr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbAppearance;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cbbDrop1;
        private System.Windows.Forms.Label label28;
        private Controls.NumericControl ncDrop1;
        private System.Windows.Forms.Label label29;
        private Controls.NumericControl ncLev;
        private Controls.NumericControl ncPAR;
        private Controls.NumericControl ncHIT;
        private Controls.NumericControl ncSPD;
        private Controls.NumericControl ncDEF;
        private Controls.NumericControl ncATK;
        private Controls.NumericControl ncHP;
        private Controls.NumericControl ncMoney;
        private Controls.NumericControl ncExp;
        private Controls.NumericControl ncActionTimes;
        private Controls.NumericControl ncUnknowP2;
        private Controls.NumericControl ncLaser;
        private Controls.NumericControl ncGas;
        private Controls.NumericControl ncSound;
        private Controls.NumericControl ncElectri;
        private Controls.NumericControl ncIce;
        private Controls.NumericControl ncFire;
        private Controls.NumericControl ncNormal;
        private Controls.NumericControl ncUnknowP1;
        private System.Windows.Forms.Label label35;
        private Controls.NumericControl ncDrop4;
        private System.Windows.Forms.ComboBox cbbDrop4;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label33;
        private Controls.NumericControl ncDrop3;
        private System.Windows.Forms.ComboBox cbbDrop3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label30;
        private Controls.NumericControl ncDrop2;
        private System.Windows.Forms.ComboBox cbbDrop2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbRomAddr;
        private System.Windows.Forms.Button btnSaveToRom;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbRomData;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox cbbAct7;
        private System.Windows.Forms.ComboBox cbbAct6;
        private System.Windows.Forms.ComboBox cbbAct5;
        private System.Windows.Forms.ComboBox cbbAct4;
        private System.Windows.Forms.ComboBox cbbAct3;
        private System.Windows.Forms.ComboBox cbbAct2;
        private System.Windows.Forms.ComboBox cbbAct1;
        private System.Windows.Forms.Label label42;
        private Controls.NumericControl ncAct1;
        private Controls.NumericControl ncAct7;
        private Controls.NumericControl ncAct6;
        private Controls.NumericControl ncAct5;
        private Controls.NumericControl ncAct4;
        private Controls.NumericControl ncAct3;
        private Controls.NumericControl ncAct2;
        private System.Windows.Forms.TextBox tbUKAct7;
        private System.Windows.Forms.TextBox tbUKAct6;
        private System.Windows.Forms.TextBox tbUKAct5;
        private System.Windows.Forms.TextBox tbUKAct4;
        private System.Windows.Forms.TextBox tbUKAct3;
        private System.Windows.Forms.TextBox tbUKAct2;
        private System.Windows.Forms.TextBox tbUKAct1;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
    }
}