﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsNingenBouguArrDecode
    {
        public static List<XlsNingenBougu> loadData(byte[] datas, string key)
        {
            List<XlsItemList> itemNames = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            List<XlsNingenBougu> result = new List<XlsNingenBougu>();

            int index = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + 15 > datas.Length)
                {
                    break;
                }
                XlsNingenBougu o = new XlsNingenBougu();
                result.Add(o);
                o.SeqNo = index;
                o.Offset = i;
                o.Male = datas[i];
                o.Female = datas[i + 1];
                o.Animal = datas[i + 2];
                o.Unknown1 = datas[i + 3];

                o.Attack = datas[i + 4];
                o.Defence= datas[i + 5];
                o.Speed = datas[i + 6] ;
                o.Man = datas[i + 7];
                o.Unknown2 = datas[i + 8];
                o.FireR= datas[i+9];
                o.LaserR = datas[i + 10];
                o.ElectriR = datas[i + 11];
                o.SoundR = datas[i + 12];
                o.GasR = datas[i + 13];
                o.IceR = datas[i + 14];
                o.Unknown3 = datas[i + 15];

                o.Name = "";
                if (itemNames != null)
                {
                    List<XlsItemList> temp = itemNames.Where(x => x.TypeId >=2 && x.TypeId<=6).OrderBy(x => x.Offset).ToList();
                    if (temp.Any())
                    {
                        o.Name = temp[index].Name;
                        switch (temp[index].TypeId)
                        {
                            case 2: o.TypeName = "头";break;
                            case 3: o.TypeName = "衣"; break;
                            case 4: o.TypeName = "脚"; break;
                            case 5: o.TypeName = "手"; break;
                            case 6: o.TypeName = "饰"; break;
                            default: o.TypeName = "unknown";break;
                        }
                    }
                }

                for (int j = 0; j < 16; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }


                index++;


                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o, key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null, key, true);
            DataBaseService.addData(key, result);
            return result;
        }
    }
}
