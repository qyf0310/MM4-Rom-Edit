﻿using MM4RomEdit.Controls;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    partial class CUnitView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchTree1 = new SearchTree();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ncWeight = new NumericControl();
            this.ncCost = new NumericControl();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ncDEFInit = new NumericControl();
            this.ncDefMax = new NumericControl();
            this.label5 = new System.Windows.Forms.Label();
            this.ncHitMax = new NumericControl();
            this.label6 = new System.Windows.Forms.Label();
            this.ncHitInit = new NumericControl();
            this.label7 = new System.Windows.Forms.Label();
            this.ncParMax = new NumericControl();
            this.label8 = new System.Windows.Forms.Label();
            this.ncParInit = new NumericControl();
            this.label9 = new System.Windows.Forms.Label();
            this.ncInitAbilityNum = new NumericControl();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbbAbility3 = new System.Windows.Forms.ComboBox();
            this.cbbAbility2 = new System.Windows.Forms.ComboBox();
            this.cbbAbility1 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSaveToRom = new System.Windows.Forms.Button();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbRomAddr = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbRomData = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ncWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEFInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDefMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncHitMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncHitInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncParMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncParInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncInitAbilityNum)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // searchTree1
            // 
            this.searchTree1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTree1.Location = new System.Drawing.Point(2, 2);
            this.searchTree1.Name = "searchTree1";
            this.searchTree1.Size = new System.Drawing.Size(149, 428);
            this.searchTree1.TabIndex = 0;
            this.searchTree1.DoubleClick += new System.EventHandler(this.searchTree1_DoubleClick);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(228, 6);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(241, 21);
            this.tbName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "名称    ：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(157, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "重量    ：";
            // 
            // ncWeight
            // 
            this.ncWeight.DecimalPlaces = 2;
            this.ncWeight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ncWeight.Location = new System.Drawing.Point(228, 42);
            this.ncWeight.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            131072});
            this.ncWeight.MaxNumberControl = null;
            this.ncWeight.MinNumberControl = null;
            this.ncWeight.Name = "ncWeight";
            this.ncWeight.Size = new System.Drawing.Size(76, 21);
            this.ncWeight.TabIndex = 41;
            this.ncWeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncCost
            // 
            this.ncCost.Location = new System.Drawing.Point(381, 42);
            this.ncCost.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncCost.MaxNumberControl = null;
            this.ncCost.MinNumberControl = null;
            this.ncCost.Name = "ncCost";
            this.ncCost.Size = new System.Drawing.Size(88, 21);
            this.ncCost.TabIndex = 43;
            this.ncCost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(310, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 42;
            this.label2.Text = "改造费  ：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(157, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 44;
            this.label4.Text = "初始防御：";
            // 
            // ncDEFInit
            // 
            this.ncDEFInit.Location = new System.Drawing.Point(228, 94);
            this.ncDEFInit.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncDEFInit.MaxNumberControl = null;
            this.ncDEFInit.MinNumberControl = null;
            this.ncDEFInit.Name = "ncDEFInit";
            this.ncDEFInit.Size = new System.Drawing.Size(76, 21);
            this.ncDEFInit.TabIndex = 45;
            this.ncDEFInit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncDefMax
            // 
            this.ncDefMax.Location = new System.Drawing.Point(381, 94);
            this.ncDefMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncDefMax.MaxNumberControl = null;
            this.ncDefMax.MinNumberControl = null;
            this.ncDefMax.Name = "ncDefMax";
            this.ncDefMax.Size = new System.Drawing.Size(88, 21);
            this.ncDefMax.TabIndex = 47;
            this.ncDefMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(310, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 46;
            this.label5.Text = "最大防御：";
            // 
            // ncHitMax
            // 
            this.ncHitMax.Location = new System.Drawing.Point(381, 145);
            this.ncHitMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncHitMax.MaxNumberControl = null;
            this.ncHitMax.MinNumberControl = null;
            this.ncHitMax.Name = "ncHitMax";
            this.ncHitMax.Size = new System.Drawing.Size(88, 21);
            this.ncHitMax.TabIndex = 51;
            this.ncHitMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(310, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 50;
            this.label6.Text = "最大命中：";
            // 
            // ncHitInit
            // 
            this.ncHitInit.Location = new System.Drawing.Point(228, 145);
            this.ncHitInit.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncHitInit.MaxNumberControl = null;
            this.ncHitInit.MinNumberControl = null;
            this.ncHitInit.Name = "ncHitInit";
            this.ncHitInit.Size = new System.Drawing.Size(76, 21);
            this.ncHitInit.TabIndex = 49;
            this.ncHitInit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(157, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 48;
            this.label7.Text = "初始命中：";
            // 
            // ncParMax
            // 
            this.ncParMax.Location = new System.Drawing.Point(381, 198);
            this.ncParMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncParMax.MaxNumberControl = null;
            this.ncParMax.MinNumberControl = null;
            this.ncParMax.Name = "ncParMax";
            this.ncParMax.Size = new System.Drawing.Size(88, 21);
            this.ncParMax.TabIndex = 55;
            this.ncParMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(310, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 54;
            this.label8.Text = "最大回避：";
            // 
            // ncParInit
            // 
            this.ncParInit.Location = new System.Drawing.Point(228, 198);
            this.ncParInit.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncParInit.MaxNumberControl = null;
            this.ncParInit.MinNumberControl = null;
            this.ncParInit.Name = "ncParInit";
            this.ncParInit.Size = new System.Drawing.Size(76, 21);
            this.ncParInit.TabIndex = 53;
            this.ncParInit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(157, 200);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 52;
            this.label9.Text = "初始回避：";
            // 
            // ncInitAbilityNum
            // 
            this.ncInitAbilityNum.Location = new System.Drawing.Point(77, 30);
            this.ncInitAbilityNum.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.ncInitAbilityNum.MaxNumberControl = null;
            this.ncInitAbilityNum.MinNumberControl = null;
            this.ncInitAbilityNum.Name = "ncInitAbilityNum";
            this.ncInitAbilityNum.Size = new System.Drawing.Size(52, 21);
            this.ncInitAbilityNum.TabIndex = 57;
            this.ncInitAbilityNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 56;
            this.label10.Text = "初始数量：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbbAbility3);
            this.groupBox1.Controls.Add(this.cbbAbility2);
            this.groupBox1.Controls.Add(this.cbbAbility1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.ncInitAbilityNum);
            this.groupBox1.Location = new System.Drawing.Point(480, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 207);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "特性";
            // 
            // cbbAbility3
            // 
            this.cbbAbility3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAbility3.FormattingEnabled = true;
            this.cbbAbility3.Location = new System.Drawing.Point(77, 180);
            this.cbbAbility3.Name = "cbbAbility3";
            this.cbbAbility3.Size = new System.Drawing.Size(166, 20);
            this.cbbAbility3.TabIndex = 64;
            // 
            // cbbAbility2
            // 
            this.cbbAbility2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAbility2.FormattingEnabled = true;
            this.cbbAbility2.Location = new System.Drawing.Point(77, 132);
            this.cbbAbility2.Name = "cbbAbility2";
            this.cbbAbility2.Size = new System.Drawing.Size(166, 20);
            this.cbbAbility2.TabIndex = 63;
            // 
            // cbbAbility1
            // 
            this.cbbAbility1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAbility1.FormattingEnabled = true;
            this.cbbAbility1.Location = new System.Drawing.Point(77, 81);
            this.cbbAbility1.Name = "cbbAbility1";
            this.cbbAbility1.Size = new System.Drawing.Size(166, 20);
            this.cbbAbility1.TabIndex = 62;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 61;
            this.label13.Text = "特性3   ：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 135);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 60;
            this.label12.Text = "特性2   ：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 84);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 59;
            this.label11.Text = "特性1   ：";
            // 
            // btnSaveToRom
            // 
            this.btnSaveToRom.Location = new System.Drawing.Point(480, 269);
            this.btnSaveToRom.Name = "btnSaveToRom";
            this.btnSaveToRom.Size = new System.Drawing.Size(86, 23);
            this.btnSaveToRom.TabIndex = 63;
            this.btnSaveToRom.Text = "写入ROM";
            this.btnSaveToRom.UseVisualStyleBackColor = true;
            this.btnSaveToRom.Click += new System.EventHandler(this.btnSaveToRom_Click);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(425, 271);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(44, 21);
            this.tbLength.TabIndex = 62;
            this.tbLength.Text = "14";
            this.tbLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(378, 274);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 61;
            this.label38.Text = "长度：";
            // 
            // tbRomAddr
            // 
            this.tbRomAddr.Location = new System.Drawing.Point(228, 271);
            this.tbRomAddr.Name = "tbRomAddr";
            this.tbRomAddr.ReadOnly = true;
            this.tbRomAddr.Size = new System.Drawing.Size(136, 21);
            this.tbRomAddr.TabIndex = 60;
            this.tbRomAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(157, 274);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 12);
            this.label37.TabIndex = 59;
            this.label37.Text = "ROM地址：";
            // 
            // tbRomData
            // 
            this.tbRomData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRomData.Location = new System.Drawing.Point(159, 324);
            this.tbRomData.Multiline = true;
            this.tbRomData.Name = "tbRomData";
            this.tbRomData.ReadOnly = true;
            this.tbRomData.Size = new System.Drawing.Size(570, 96);
            this.tbRomData.TabIndex = 65;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(157, 309);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 12);
            this.label39.TabIndex = 64;
            this.label39.Text = "ROM数据：";
            // 
            // CUnitView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 432);
            this.Controls.Add(this.tbRomData);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.btnSaveToRom);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.tbRomAddr);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ncParMax);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ncParInit);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ncHitMax);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ncHitInit);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ncDefMax);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ncDEFInit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ncCost);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ncWeight);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.searchTree1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CUnitView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "CarView";
            ((System.ComponentModel.ISupportInitialize)(this.ncWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEFInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDefMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncHitMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncHitInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncParMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncParInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncInitAbilityNum)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.SearchTree searchTree1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Controls.NumericControl ncWeight;
        private Controls.NumericControl ncCost;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private Controls.NumericControl ncDEFInit;
        private Controls.NumericControl ncDefMax;
        private System.Windows.Forms.Label label5;
        private Controls.NumericControl ncHitMax;
        private System.Windows.Forms.Label label6;
        private Controls.NumericControl ncHitInit;
        private System.Windows.Forms.Label label7;
        private Controls.NumericControl ncParMax;
        private System.Windows.Forms.Label label8;
        private Controls.NumericControl ncParInit;
        private System.Windows.Forms.Label label9;
        private Controls.NumericControl ncInitAbilityNum;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbbAbility3;
        private System.Windows.Forms.ComboBox cbbAbility2;
        private System.Windows.Forms.ComboBox cbbAbility1;
        private System.Windows.Forms.Button btnSaveToRom;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbRomAddr;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbRomData;
        private System.Windows.Forms.Label label39;
    }
}