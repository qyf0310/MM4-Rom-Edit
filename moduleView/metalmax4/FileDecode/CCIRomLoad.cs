﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using SharpConfig;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class CCIRomLoad
    {
        public static Object loadData(string filePath)
        {

            BroadCastService.createMessage(string.Format("开始加载rom文件:{0}", filePath), ProcessStateEnum.NORMAL, null, filePath);
            byte[] fileBytes = null;
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.Read))
            {
                fileBytes = new byte[(int)fs.Length];
                fs.Read(fileBytes, 0, fileBytes.Length);
            }
            BroadCastService.createMessage(string.Format("载入bin数据完毕,total:{0} Bytes,src:{1}",fileBytes.Length, filePath), ProcessStateEnum.SUCCESS, null,filePath);

            //            String configPath = DataBaseService.getConfigPath();
            //            Configuration offsetConfig = Configuration.LoadFromFile(configPath);
            //            BroadCastService.createMessage(string.Format("读取偏移配置完毕,{0}", configPath), ProcessStateEnum.SUCCESS, null, filePath);

            //            BroadCastService.createMessage(string.Format("加载道具名称数据..."), ProcessStateEnum.NORMAL, null, filePath);
            //            byte[] itemNameDatas=new byte[Convert.ToInt32(offsetConfig["itemlist"]["name_length"].StringValue,16)];
            //            int itemNameOffset = Convert.ToInt32(offsetConfig["itemlist"]["name_offset"].StringValue, 16);
            //            Buffer.BlockCopy(fileBytes, itemNameOffset, itemNameDatas, 0, itemNameDatas.Length);
            //            IList itemNameList = XlsNamDecode.loadData(itemNameDatas, "itemlist.nam");
            //            BroadCastService.createMessage(string.Format("加载道具名称数据完毕,total:{0}", itemNameList.Count), ProcessStateEnum.SUCCESS, null, filePath);


            loadDataRun(fileBytes, ConstanDef.ArrFileKey_itemlist, "道具属性", filePath, XlsItemArrDecode.loadData);
            loadDataRun(fileBytes, ConstanDef.ArrFileKey_btl_actdata, "战斗动作", filePath, XlsBtlActdataArrDecodse.loadData);
            loadDataRun(fileBytes, ConstanDef.ArrFileKey_carinit, "战车初始数据", filePath, XlsCarInitArrDecode.loadData);
            loadDataRun(fileBytes, ConstanDef.ArrFileKey_carwepon, "战车武器", filePath, XlsCarWeponArrDecode.loadData);
            loadDataRun(fileBytes, ConstanDef.ArrFileKey_chassis, "底盘", filePath, XlsChassisArrDecode.loadData);
            loadDataRun(fileBytes, ConstanDef.ArrFileKey_cunit, "C装置", filePath, XlsCUnitArrDecode.loadData);
            loadDataRun(fileBytes, ConstanDef.ArrFileKey_engine, "引擎", filePath, XlsEnginArrDecode.loadData);
            loadDataRun(fileBytes, ConstanDef.ArrFileKey_monster, "怪物", filePath, XlsMonsterArrDecode.loadData);
            loadDataRun(fileBytes, ConstanDef.ArrFileKey_Human_Wepon, "人类武器", filePath, XlsNingenBukiArrDecode.loadData);
            loadDataRun(fileBytes, ConstanDef.ArrFileKey_Human_Armors, "人类防具", filePath, XlsNingenBouguArrDecode.loadData);

            //            BroadCastService.createMessage(string.Format("加载道具属性数据..."), ProcessStateEnum.NORMAL, null, filePath);
            //            byte[] itemArrDatas = new byte[Convert.ToInt32(offsetConfig["itemlist"]["data_length"].StringValue, 16)];
            //            int itemArrOffset = Convert.ToInt32(offsetConfig["itemlist"]["data_offset"].StringValue, 16);
            //            Buffer.BlockCopy(fileBytes, itemArrOffset, itemArrDatas, 0, itemArrDatas.Length);
            //            IList itemDataList = XlsItemArrDecode.loadData(itemArrDatas, "itemlist.arr");
            //            BroadCastService.createMessage(string.Format("加载道具属性数据完毕,total:{0}", itemDataList.Count), ProcessStateEnum.SUCCESS, null, filePath);

            //            BroadCastService.createMessage(string.Format("加载战斗动作数据..."), ProcessStateEnum.NORMAL, null, filePath);
            //            byte[] btlActionDatas = new byte[Convert.ToInt32(offsetConfig["btl_action"]["data_length"].StringValue, 16)];
            //            int btlActionOffset = Convert.ToInt32(offsetConfig["btl_action"]["data_offset"].StringValue, 16);
            //            Buffer.BlockCopy(fileBytes, btlActionOffset, btlActionDatas, 0, btlActionDatas.Length);
            //            IList btlActionDataList = XlsBtlActdataArrDecodse.loadData(btlActionDatas, "btl_actdata.arr");
            //            BroadCastService.createMessage(string.Format("加载战斗动作数据完毕,total:{0}", btlActionDataList.Count), ProcessStateEnum.SUCCESS, null, filePath);

            //            BroadCastService.createMessage(string.Format("加载战车武器数据..."), ProcessStateEnum.NORMAL, null, filePath);
            //            byte[] carweponDatas = new byte[Convert.ToInt32(offsetConfig["carwepon"]["data_length"].StringValue, 16)];
            //            int carweponOffset = Convert.ToInt32(offsetConfig["carwepon"]["data_offset"].StringValue, 16);
            //            Buffer.BlockCopy(fileBytes, carweponOffset, carweponDatas, 0, carweponDatas.Length);
            //            IList carweponList = XlsCarWeponArrDecode.loadData(carweponDatas, "carwepon.arr");
            //            BroadCastService.createMessage(string.Format("加载战车武器数据完毕,total:{0}", carweponList.Count), ProcessStateEnum.SUCCESS, null, filePath);

            //            BroadCastService.createMessage(string.Format("加载底盘数据..."), ProcessStateEnum.NORMAL, null, filePath);
            //            byte[] chassisDatas = new byte[Convert.ToInt32(offsetConfig["chassis"]["data_length"].StringValue, 16)];
            //            int chassisOffset = Convert.ToInt32(offsetConfig["chassis"]["data_offset"].StringValue, 16);
            //            Buffer.BlockCopy(fileBytes, chassisOffset, chassisDatas, 0, chassisDatas.Length);
            //            IList chassisList = XlsChassisArrDecode.loadData(chassisDatas, "chassis.arr");
            //            BroadCastService.createMessage(string.Format("加载底盘数据完毕,total:{0}", chassisList.Count), ProcessStateEnum.SUCCESS, null, filePath);

            //            BroadCastService.createMessage(string.Format("加载C装置数据..."), ProcessStateEnum.NORMAL, null, filePath);
            //            byte[] cunitDatas = new byte[Convert.ToInt32(offsetConfig["cunit"]["data_length"].StringValue, 16)];
            //            int cunitOffset = Convert.ToInt32(offsetConfig["cunit"]["data_offset"].StringValue, 16);
            //            Buffer.BlockCopy(fileBytes, cunitOffset, cunitDatas, 0, cunitDatas.Length);
            //            IList cunitList = XlsCUnitArrDecode.loadData(cunitDatas, "cunit.arr");
            //            BroadCastService.createMessage(string.Format("加载C装置数据完毕,total:{0}", cunitList.Count), ProcessStateEnum.SUCCESS, null, filePath);

            //            BroadCastService.createMessage(string.Format("加载引擎数据..."), ProcessStateEnum.NORMAL, null, filePath);
            //            byte[] engineDatas = new byte[Convert.ToInt32(offsetConfig["engine"]["data_length"].StringValue, 16)];
            //            int engineOffset = Convert.ToInt32(offsetConfig["engine"]["data_offset"].StringValue, 16);
            //            Buffer.BlockCopy(fileBytes, engineOffset, engineDatas, 0, engineDatas.Length);
            //            IList engineList = XlsEnginArrDecode.loadData(engineDatas, "engine.arr");
            //            BroadCastService.createMessage(string.Format("加载引擎数据完毕,total:{0}", engineList.Count), ProcessStateEnum.SUCCESS, null, filePath);

            //            BroadCastService.createMessage(string.Format("加载怪物数据..."), ProcessStateEnum.NORMAL, null, filePath);
            //            byte[] monsterDatas = new byte[Convert.ToInt32(offsetConfig["monster"]["data_length"].StringValue, 16)];
            //            int monsterOffset = Convert.ToInt32(offsetConfig["monster"]["data_offset"].StringValue, 16);
            //            Buffer.BlockCopy(fileBytes, monsterOffset, monsterDatas, 0, monsterDatas.Length);
            //            IList monsterList = XlsMonsterArrDecode.loadData(monsterDatas, "monster.arr");
            //            BroadCastService.createMessage(string.Format("加载怪物数据完毕,total:{0}", monsterList.Count), ProcessStateEnum.SUCCESS, null, filePath);

            BroadCastService.createMessage(string.Format("rom加载完毕."), ProcessStateEnum.SUCCESS, null,filePath,true);

            return null;
        }

        private static Object loadDataRun(byte[] fileBytes, String configSectionName, String remarkInfo,String broadcastKey, DataProcessWorker.ProcessDataHandler processHandler)
        {
            try
            {
                BroadCastService.createMessage(string.Format("加载{0}数据...", remarkInfo), ProcessStateEnum.NORMAL, null, broadcastKey);
                byte[] arrDatas = new byte[Convert.ToInt32(DataBaseService.getOffsetConfigSection(configSectionName)["data_length"].StringValue, 16)];
                int arrOffset = Convert.ToInt32(Convert.ToInt32(DataBaseService.getOffsetConfigSection(configSectionName)["data_offset"].StringValue, 16));
                Buffer.BlockCopy(fileBytes, arrOffset, arrDatas, 0, arrDatas.Length);
                Object obj = processHandler(arrDatas, configSectionName);// + ".arr"
                BroadCastService.createMessage(string.Format("加载{0}数据完毕,total:{1}", remarkInfo, (obj as IList).Count), ProcessStateEnum.SUCCESS, null, broadcastKey);
                return obj;
            }
            catch (Exception e)
            {
                BroadCastService.createMessage(string.Format("加载{0}数据失败,{1}", remarkInfo, e.Message), ProcessStateEnum.FAILED, null, broadcastKey);
                return null;
            }
            
        }
    }
}
