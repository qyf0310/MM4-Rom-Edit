﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.model;
using SharpConfig;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class CarWeponView : Form
    {
        private XlsCarWepon currentItem = null;
        private Section animeSection = null;
        public CarWeponView()
        {
            InitializeComponent();
            init();
            
        }
        public void init()
        {
            // 获取武器列表
            List<XlsCarWepon> datas = DataBaseService.getData(ConstanDef.ArrFileKey_carwepon) as List<XlsCarWepon>;
            searchTree1.loadList(datas);

            //获取外观列表
            List<XlsCarWepon> apperenceList = new List<XlsCarWepon>(datas);
            cbbApperence.ValueMember = "apperence";
            cbbApperence.DisplayMember = "apperence";
            cbbApperence.DataSource = apperenceList;
            
            //获取攻击范围列表
            for (int i = 0; i < 42; i++)
            {
                String name = DataBaseService.getWeponAttkRange(i);
                cbbRange.Items.Add(name);
            }          
            
            //获取效果动画列表 
            List<AnimeNam> animeNams=DataBaseService.getAnimeList().Where(x=>x.Type==1).ToList();
            cbbAnime.ValueMember = "Code";
            cbbAnime.DisplayMember = "Text";
            cbbAnime.DataSource = animeNams;

            //获取动画台词列表
            List<XlsBattleActData> batActions = DataBaseService.getData(ConstanDef.ArrFileKey_btl_actdata) as List<XlsBattleActData>;
            List<XlsBattleActData> temp=new List<XlsBattleActData>(batActions);
            temp=temp.OrderBy(x => x.AnimeId).ToList();
            cbbAttribute.ValueMember = "ActionCode";
            cbbAttribute.DisplayMember = "Name";
            cbbAttribute.DataSource = temp;

        }
        private void searchTree1_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree)sender;
            if (control.TreeView.SelectedNode != null)
            {
                loadItem(Convert.ToInt32(control.TreeView.SelectedNode.Tag));
            }

        }
        private void loadItem(int index)
        {
            List<XlsCarWepon> datas = DataBaseService.getData(ConstanDef.ArrFileKey_carwepon) as List<XlsCarWepon>;
            XlsCarWepon o = datas[index];
            currentItem = o;
            tbName.Text = o.Name;
            tbRomAddr.Text = String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_carwepon)["data_offset"].StringValue, 16) + o.Offset);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in o.Data)
            {
                sb.Append(String.Format("{0:X2} ", b));
            }
            tbRomData.Text = sb.ToString();


            ncWeight.Value = (decimal)(o.Weight*1.0/100.0);
            ncCost.Value = o.RemodelCost;
            ncRemodelWeight.Value= (decimal)(o.RemodelWeight * 1.0 / 100.0);
            ncDEFInit.Value = o.InitDefen;
            ncDefMax.Value = o.MaxDefen;
            ncBombNumInit.Value = o.InitBombNum;
            ncBombNumMax.Value = o.MaxBombNum;
            ncATKInit.Value = o.InitAttack;
            ncAtkMax.Value = o.MaxAttack;
            cbbAnime.SelectedValue = o.AttAnime;
            //labelAnime.Text = "";
            //if (cbbAnime.SelectedValue == null)
            //{
            //    labelAnime.Text = String.Format("动画未知,code:{0},0x{0:X4}", o.AttAttribute);
            //}
            cbbRange.SelectedIndex = o.AttRange;
            cbbAttribute.SelectedValue = o.AttAttribute;
            tbAttackResult.Text = "";
            if (cbbAttribute.SelectedValue==null)
            {
                tbAttackResult.Text = String.Format("效果未知,code:{0},0x{0:X4}", o.AttAttribute);
            }
            else
            {
                List<XlsBattleActData> temp = DataBaseService.getData(ConstanDef.ArrFileKey_btl_actdata) as List<XlsBattleActData>;
                var wordsTemp = temp.FirstOrDefault(x => x.ActionCode == o.AttAttribute);
                tbAttackResult.Text = wordsTemp == null ? "" : wordsTemp.Words;
            }

            tbUnknown1.Text = o.Unknown1.ToString();
            tbUnknown2.Text = o.Unknown2.ToString();
            tbUnknown3.Text = o.Unknown3.ToString();
            cbbApperence.SelectedValue = o.Apperence;
            refreshSimilarItem(o.AttAnime, o.AttRange, o.AttAttribute);
        }
        private void refreshSimilarItem(int anime, int range,int effect)
        {
            List<XlsCarWepon> datas = DataBaseService.getData(ConstanDef.ArrFileKey_carwepon) as List<XlsCarWepon>;
            List<XlsCarWepon> similars = datas.FindAll(x => x.AttAnime == anime || x.AttRange==range || x.AttAttribute == effect).ToList();
            lvData_similar.Items.Clear();
            foreach (XlsCarWepon w in similars)
            {
                ListViewItem item = new ListViewItem(w.Name);
                item.SubItems.Add(w.AttAnime == anime ? "○":"");
                item.SubItems.Add(w.AttRange == range ? "○" : "");
                item.SubItems.Add(w.AttAttribute == effect ? "○" : "");
                lvData_similar.Items.Add(item);
            }
        }
        private void btnSaveToRom_Click(object sender, EventArgs e)
        {
            byte[] bytes = currentItem.Data.ToArray();
            
            bytes[0] = (byte)(ncCost.Value % 256);
            bytes[1] = (byte)(ncCost.Value / 256);

            bytes[2] = (byte)(ncWeight.Value*100 % 256);
            bytes[3] = (byte)(ncWeight.Value * 100 / 256);

            bytes[4] = (byte)ncDEFInit.Value;
            bytes[5] = (byte)ncDefMax.Value;

            bytes[6] = (byte)(ncATKInit.Value % 256);
            bytes[7] = (byte)(ncATKInit.Value / 256);

            if (cbbAttribute.SelectedValue!=null)
            {
                int selValue = Convert.ToInt32(cbbAttribute.SelectedValue);
                bytes[8] = (byte)(selValue % 256);
                bytes[9] = (byte)(selValue / 256);
            }

            if (cbbAnime.SelectedValue!=null)
            {
                int selValue = Convert.ToInt32(cbbAnime.SelectedValue);
                bytes[10] = (byte)(selValue % 256);
                bytes[11] = (byte)(selValue / 256);
            }



            bytes[12] = (byte)cbbRange.SelectedIndex;

            bytes[13] = (byte)ncBombNumInit.Value;
            bytes[16] = (byte)ncBombNumMax.Value;

            bytes[18] = (byte)(ncRemodelWeight.Value * 100 % 256);
            bytes[19] = (byte)(ncRemodelWeight.Value * 100 / 256);

            bytes[20] = (byte)(ncAtkMax.Value % 256);
            bytes[21] = (byte)(ncAtkMax.Value / 256);

            if (cbbApperence.SelectedValue != null)
            {
                int selValue = Convert.ToInt32(cbbApperence.SelectedValue);
                bytes[22] = (byte)(selValue % 256);
                bytes[23] = (byte)(selValue / 256);
            }
            try
            {
                byte[] newBytes = FileOperateHelper.modifyBin(DataBaseService.getRomPath(), Convert.ToInt32(tbRomAddr.Text, 16), bytes);
                XlsCarWepon newO = XlsCarWeponArrDecode.loadData(newBytes, "edit")[0];
                
                
                List<XlsCarWepon> datas = DataBaseService.getData(ConstanDef.ArrFileKey_carwepon) as List<XlsCarWepon>;
                int index = Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag);
                XlsCarWepon oldO = datas[index];
                newO.Name = oldO.Name;
                newO.SeqNo = oldO.SeqNo;
                newO.Offset = oldO.Offset;
                datas[index] = newO;
                loadItem(index);
                MessageBox.Show("写入完毕");
            }
            catch (Exception exception)
            {
                MessageBox.Show("写入失败," + exception.Message);
            }
        }

        private void cbbAttribute_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<XlsBattleActData> temp = DataBaseService.getData(ConstanDef.ArrFileKey_btl_actdata) as List<XlsBattleActData>;
            var wordsTemp = temp.FirstOrDefault(x => x.ActionCode == Convert.ToInt32(cbbAttribute.SelectedValue));
            tbAttackResult.Text = wordsTemp == null ? "" : wordsTemp.Words;
            refreshSimilarItem(-1, -1, Convert.ToInt32(cbbAttribute.SelectedValue));
        }

        private void cbbAnime_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshSimilarItem(Convert.ToInt32(cbbAnime.SelectedValue), -1, -1);
        }

        private void cbbRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshSimilarItem(-1, Convert.ToInt32(cbbRange.SelectedIndex), -1);
        }
    }
}
