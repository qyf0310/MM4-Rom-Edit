﻿using MM4RomEdit.Controls;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    partial class ChassisView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbbHole6 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbbHole5 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbbHole4 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbbHole3 = new System.Windows.Forms.ComboBox();
            this.cbbHole2 = new System.Windows.Forms.ComboBox();
            this.cbbHole1 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.btnSaveToRom = new System.Windows.Forms.Button();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbRomAddr = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbRomData = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbbAbility = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbbType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chebDC = new System.Windows.Forms.CheckBox();
            this.chebDE = new System.Windows.Forms.CheckBox();
            this.lvData_upgrade = new System.Windows.Forms.ListView();
            this.chName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chCode = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chFlag = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbbUpgradeRoute = new System.Windows.Forms.ComboBox();
            this.tbCode = new System.Windows.Forms.TextBox();
            this.ncLev = new MM4RomEdit.Controls.NumericControl();
            this.ncStorage = new MM4RomEdit.Controls.NumericControl();
            this.searchTree1 = new MM4RomEdit.Controls.SearchTree();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncLev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncStorage)).BeginInit();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(296, 6);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(241, 21);
            this.tbName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(225, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "名称    ：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 44;
            this.label4.Text = "货柜大小：";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cbbHole6);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbbHole5);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cbbHole4);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbbHole3);
            this.groupBox1.Controls.Add(this.cbbHole2);
            this.groupBox1.Controls.Add(this.cbbHole1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(808, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 446);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "穴";
            // 
            // cbbHole6
            // 
            this.cbbHole6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole6.FormattingEnabled = true;
            this.cbbHole6.Location = new System.Drawing.Point(77, 273);
            this.cbbHole6.Name = "cbbHole6";
            this.cbbHole6.Size = new System.Drawing.Size(166, 20);
            this.cbbHole6.TabIndex = 70;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 277);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 69;
            this.label9.Text = "未知 ：";
            // 
            // cbbHole5
            // 
            this.cbbHole5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole5.FormattingEnabled = true;
            this.cbbHole5.Location = new System.Drawing.Point(77, 229);
            this.cbbHole5.Name = "cbbHole5";
            this.cbbHole5.Size = new System.Drawing.Size(166, 20);
            this.cbbHole5.TabIndex = 68;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 233);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 12);
            this.label8.TabIndex = 67;
            this.label8.Text = "固武5：";
            // 
            // cbbHole4
            // 
            this.cbbHole4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole4.FormattingEnabled = true;
            this.cbbHole4.Location = new System.Drawing.Point(77, 182);
            this.cbbHole4.Name = "cbbHole4";
            this.cbbHole4.Size = new System.Drawing.Size(166, 20);
            this.cbbHole4.TabIndex = 66;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 186);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 12);
            this.label7.TabIndex = 65;
            this.label7.Text = "固武4：";
            // 
            // cbbHole3
            // 
            this.cbbHole3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole3.FormattingEnabled = true;
            this.cbbHole3.Location = new System.Drawing.Point(77, 128);
            this.cbbHole3.Name = "cbbHole3";
            this.cbbHole3.Size = new System.Drawing.Size(166, 20);
            this.cbbHole3.TabIndex = 64;
            // 
            // cbbHole2
            // 
            this.cbbHole2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole2.FormattingEnabled = true;
            this.cbbHole2.Location = new System.Drawing.Point(77, 81);
            this.cbbHole2.Name = "cbbHole2";
            this.cbbHole2.Size = new System.Drawing.Size(166, 20);
            this.cbbHole2.TabIndex = 63;
            // 
            // cbbHole1
            // 
            this.cbbHole1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole1.FormattingEnabled = true;
            this.cbbHole1.Location = new System.Drawing.Point(77, 32);
            this.cbbHole1.Name = "cbbHole1";
            this.cbbHole1.Size = new System.Drawing.Size(166, 20);
            this.cbbHole1.TabIndex = 62;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 132);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 12);
            this.label13.TabIndex = 61;
            this.label13.Text = "固武3：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 12);
            this.label12.TabIndex = 60;
            this.label12.Text = "固武2：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 59;
            this.label11.Text = "固武1：";
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Location = new System.Drawing.Point(543, 47);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(148, 22);
            this.textBox3.TabIndex = 78;
            this.textBox3.Text = "9 = 无名车库";
            // 
            // btnSaveToRom
            // 
            this.btnSaveToRom.Location = new System.Drawing.Point(459, 315);
            this.btnSaveToRom.Name = "btnSaveToRom";
            this.btnSaveToRom.Size = new System.Drawing.Size(78, 23);
            this.btnSaveToRom.TabIndex = 63;
            this.btnSaveToRom.Text = "写入ROM";
            this.btnSaveToRom.UseVisualStyleBackColor = true;
            this.btnSaveToRom.Click += new System.EventHandler(this.btnSaveToRom_Click);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(425, 317);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(28, 21);
            this.tbLength.TabIndex = 62;
            this.tbLength.Text = "20";
            this.tbLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(378, 320);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 61;
            this.label38.Text = "长度：";
            // 
            // tbRomAddr
            // 
            this.tbRomAddr.Location = new System.Drawing.Point(296, 317);
            this.tbRomAddr.Name = "tbRomAddr";
            this.tbRomAddr.ReadOnly = true;
            this.tbRomAddr.Size = new System.Drawing.Size(76, 21);
            this.tbRomAddr.TabIndex = 60;
            this.tbRomAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(225, 320);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 12);
            this.label37.TabIndex = 59;
            this.label37.Text = "ROM地址：";
            // 
            // tbRomData
            // 
            this.tbRomData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRomData.Location = new System.Drawing.Point(227, 358);
            this.tbRomData.Multiline = true;
            this.tbRomData.Name = "tbRomData";
            this.tbRomData.ReadOnly = true;
            this.tbRomData.Size = new System.Drawing.Size(570, 100);
            this.tbRomData.TabIndex = 65;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(225, 343);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 12);
            this.label39.TabIndex = 64;
            this.label39.Text = "ROM数据：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(472, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 66;
            this.label1.Text = "升级标识：";
            // 
            // cbbAbility
            // 
            this.cbbAbility.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAbility.FormattingEnabled = true;
            this.cbbAbility.Location = new System.Drawing.Point(296, 76);
            this.cbbAbility.Name = "cbbAbility";
            this.cbbAbility.Size = new System.Drawing.Size(88, 20);
            this.cbbAbility.TabIndex = 69;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(225, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 68;
            this.label2.Text = "底盘特性：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(225, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 70;
            this.label5.Text = "底盘系列：";
            // 
            // cbbType
            // 
            this.cbbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbType.FormattingEnabled = true;
            this.cbbType.Location = new System.Drawing.Point(296, 113);
            this.cbbType.Name = "cbbType";
            this.cbbType.Size = new System.Drawing.Size(157, 20);
            this.cbbType.TabIndex = 71;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(390, 47);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 72;
            this.label6.Text = "底盘世代：";
            // 
            // chebDC
            // 
            this.chebDC.AutoSize = true;
            this.chebDC.Location = new System.Drawing.Point(543, 78);
            this.chebDC.Name = "chebDC";
            this.chebDC.Size = new System.Drawing.Size(42, 16);
            this.chebDC.TabIndex = 74;
            this.chebDC.Text = "双C";
            this.chebDC.UseVisualStyleBackColor = true;
            // 
            // chebDE
            // 
            this.chebDE.AutoSize = true;
            this.chebDE.Location = new System.Drawing.Point(607, 78);
            this.chebDE.Name = "chebDE";
            this.chebDE.Size = new System.Drawing.Size(60, 16);
            this.chebDE.TabIndex = 75;
            this.chebDE.Text = "双引擎";
            this.chebDE.UseVisualStyleBackColor = true;
            // 
            // lvData_upgrade
            // 
            this.lvData_upgrade.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvData_upgrade.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chName,
            this.chCode,
            this.chFlag});
            this.lvData_upgrade.FullRowSelect = true;
            this.lvData_upgrade.GridLines = true;
            this.lvData_upgrade.Location = new System.Drawing.Point(6, 20);
            this.lvData_upgrade.Name = "lvData_upgrade";
            this.lvData_upgrade.Size = new System.Drawing.Size(454, 113);
            this.lvData_upgrade.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvData_upgrade.TabIndex = 0;
            this.lvData_upgrade.UseCompatibleStateImageBehavior = false;
            this.lvData_upgrade.View = System.Windows.Forms.View.Details;
            // 
            // chName
            // 
            this.chName.Text = "名称";
            this.chName.Width = 180;
            // 
            // chCode
            // 
            this.chCode.Text = "底盘系列";
            this.chCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chCode.Width = 120;
            // 
            // chFlag
            // 
            this.chFlag.Text = "升级标识";
            this.chFlag.Width = 120;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lvData_upgrade);
            this.groupBox2.Location = new System.Drawing.Point(225, 166);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(466, 139);
            this.groupBox2.TabIndex = 79;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "升级列表【待更新】";
            // 
            // cbbUpgradeRoute
            // 
            this.cbbUpgradeRoute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbUpgradeRoute.FormattingEnabled = true;
            this.cbbUpgradeRoute.Location = new System.Drawing.Point(543, 113);
            this.cbbUpgradeRoute.Name = "cbbUpgradeRoute";
            this.cbbUpgradeRoute.Size = new System.Drawing.Size(148, 20);
            this.cbbUpgradeRoute.TabIndex = 80;
            this.cbbUpgradeRoute.SelectedIndexChanged += new System.EventHandler(this.cbbUpgrade_SelectedIndexChanged);
            // 
            // tbCode
            // 
            this.tbCode.Location = new System.Drawing.Point(392, 76);
            this.tbCode.Multiline = true;
            this.tbCode.Name = "tbCode";
            this.tbCode.ReadOnly = true;
            this.tbCode.Size = new System.Drawing.Size(145, 22);
            this.tbCode.TabIndex = 81;
            // 
            // ncLev
            // 
            this.ncLev.Location = new System.Drawing.Point(459, 44);
            this.ncLev.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.ncLev.MaxNumberControl = null;
            this.ncLev.MinNumberControl = null;
            this.ncLev.Name = "ncLev";
            this.ncLev.Size = new System.Drawing.Size(78, 21);
            this.ncLev.TabIndex = 73;
            this.ncLev.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncStorage
            // 
            this.ncStorage.Location = new System.Drawing.Point(296, 42);
            this.ncStorage.Maximum = new decimal(new int[] {
            18,
            0,
            0,
            0});
            this.ncStorage.MaxNumberControl = null;
            this.ncStorage.MinNumberControl = null;
            this.ncStorage.Name = "ncStorage";
            this.ncStorage.Size = new System.Drawing.Size(88, 21);
            this.ncStorage.TabIndex = 45;
            this.ncStorage.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // searchTree1
            // 
            this.searchTree1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTree1.Location = new System.Drawing.Point(2, 2);
            this.searchTree1.Name = "searchTree1";
            this.searchTree1.Size = new System.Drawing.Size(217, 456);
            this.searchTree1.TabIndex = 0;
            this.searchTree1.DoubleClick += new System.EventHandler(this.searchTree1_DoubleClick);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(296, 138);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(276, 22);
            this.textBox1.TabIndex = 82;
            this.textBox1.Text = "状态外观及固武会改变,复原穴会变为该系列初始车";
            // 
            // ChassisView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1069, 470);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tbCode);
            this.Controls.Add(this.cbbUpgradeRoute);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.chebDE);
            this.Controls.Add(this.ncLev);
            this.Controls.Add(this.chebDC);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbbType);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbbAbility);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbRomData);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.btnSaveToRom);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.tbRomAddr);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ncStorage);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.searchTree1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ChassisView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "CarView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ncLev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncStorage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.SearchTree searchTree1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Controls.NumericControl ncStorage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbbHole3;
        private System.Windows.Forms.ComboBox cbbHole2;
        private System.Windows.Forms.ComboBox cbbHole1;
        private System.Windows.Forms.Button btnSaveToRom;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbRomAddr;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbRomData;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbbAbility;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbbType;
        private System.Windows.Forms.Label label6;
        private Controls.NumericControl ncLev;
        private System.Windows.Forms.CheckBox chebDC;
        private System.Windows.Forms.CheckBox chebDE;
        private System.Windows.Forms.ComboBox cbbHole6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbbHole5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbbHole4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ListView lvData_upgrade;
        private System.Windows.Forms.ColumnHeader chName;
        private System.Windows.Forms.ColumnHeader chCode;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbbUpgradeRoute;
        private System.Windows.Forms.ColumnHeader chFlag;
        private System.Windows.Forms.TextBox tbCode;
        private System.Windows.Forms.TextBox textBox1;
    }
}