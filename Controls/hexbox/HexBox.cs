﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using System.ComponentModel;
using System.Windows.Forms;
/*=======================================================
 * 控件描述：展示byte数组的十六进制数据
 * 开发作者：Crystal_lz
 * 作者博客：http://www.clzf.co
 * 邮箱地址：beckstreet221b@gmail.com
 * 完成时间：2014-11-13
 * ------------------------------------------------------
 * 其他说明：神马版权声明啥的我也不写了 我就只想说
 *          若使用此源码 请保留此注释头 好歹让原著装个逼
 *          这是对开源者最大的鼓励
 *=======================================================
 *>>>>>>>>>>>>>>>>>>>>春哥保佑 永无BUG<<<<<<<<<<<<<<<<<<<<
 */
namespace HexBoxControl
{
    /// <summary>
    /// 用于显示16进制数据的控件
    /// </summary>
    [Designer(typeof(HexBoxDesigner))]
    public class HexBox : Control
    {
        #region Properties

        private byte[] _ByteData;
        private int codeTableType = 0;
        private Dictionary<int, String> codeTable=null;
        private bool reverseCode = false;

        public int CodeTableType
        {
            get { return codeTableType; }
            set { codeTableType = value; }
        }

        public Dictionary<int, string> CodeTable
        {
            get { return codeTable; }
            set { codeTable = value; }
        }

        public bool ReverseCode
        {
            get { return reverseCode; }
            set { reverseCode = value; }
        }

        /// <summary>
        /// 获取或者设置需要显示的数据
        /// </summary>
        [Description("需要显示的数据"), Category("Customs")]
        public byte[] ByteData {
            get { return _ByteData; }
            set {
                if (value == _ByteData) return;
                if (value != null)
                    m_nLineCount = (int)Math.Ceiling((double)value.Length / 16);
                _ByteData = value;
                _SelectedDataIndex = 0;
                _SelectionStartIndex = 0;
                _SelectionEndIndex = 0;
                this.Invalidate();
            }
        }

        private Color _ScrollHighColor = Color.LightGray;
        /// <summary>
        /// 获取或者设置滚动条滑块默认颜色
        /// </summary>
        [Description("滚动条滑块高亮颜色"), DefaultValue(typeof(Color), "LightGray"), Category("Customs")]
        public Color ScrollHighColor {
            get { return _ScrollHighColor; }
            set { _ScrollHighColor = value; }
        }

        private Color _ScrollForeColor = Color.Gray;
        /// <summary>
        /// 获取或者设置滚动条滑块默认颜色
        /// </summary>
        [Description("滚动条滑块默认颜色"), DefaultValue(typeof(Color), "Gray"), Category("Customs")]
        public Color ScrollForeColor {
            get { return _ScrollForeColor; }
            set { _ScrollForeColor = value; }
        }

        private Color _ScrollBackColor = Color.Wheat;
        /// <summary>
        /// 获取或者设置滚动条背景颜色
        /// </summary>
        [Description("滚动条背景颜色"), DefaultValue(typeof(Color), "Wheat"), Category("Customs")]
        public Color ScrollBackColor {
            get { return _ScrollBackColor; }
            set { _ScrollBackColor = value; }
        }

        private Color _AddrColor = Color.Blue;
        /// <summary>
        /// 获取或者设置地址列颜色
        /// </summary>
        [Description("地址列颜色"), DefaultValue(typeof(Color), "Blue"), Category("Customs")]
        public Color AddrColor {
            get { return _AddrColor; }
            set { _AddrColor = value; }
        }

        private Color _HeadColor = Color.Green;
        /// <summary>
        /// 获取或者设置行头颜色
        /// </summary>
        [Description("行头颜色"), DefaultValue(typeof(Color), "Green"), Category("Customs")]
        public Color HeadColor {
            get { return _HeadColor; }
            set { _HeadColor = value; }
        }

        private Color _HeadLineColor = Color.Orange;
        /// <summary>
        /// 获取或者设置线条颜色
        /// </summary>
        [Description("线条颜色"), DefaultValue(typeof(Color), "Orange"), Category("Customs")]
        public Color HeadLineColor {
            get { return _HeadLineColor; }
            set { _HeadLineColor = value; }
        }

        private Color _HexForeColor = Color.Black;
        /// <summary>
        /// 获取或者设置16进制前景颜色
        /// </summary>
        [Description("16进制前景颜色"), DefaultValue(typeof(Color), "Black"), Category("Customs")]
        public Color HexForeColor {
            get { return _HexForeColor; }
            set { _HexForeColor = value; }
        }

        private Color _HexHighForeColor = Color.White;
        /// <summary>
        /// 获取或者设置16进制高亮数据前景颜色
        /// </summary>
        [Description("16进制高亮数据前景颜色"), DefaultValue(typeof(Color), "White"), Category("Customs")]
        public Color HexHighForeColor {
            get { return _HexHighForeColor; }
            set { _HexHighForeColor = value; }
        }

        private Color _HexHighBackColor = Color.Blue;
        /// <summary>
        /// 获取或者设置16进制高亮数据背景颜色
        /// </summary>
        [Description("16进制高亮数据背景颜色"), DefaultValue(typeof(Color), "Blue"), Category("Customs")]
        public Color HexHighBackColor {
            get { return _HexHighBackColor; }
            set { _HexHighBackColor = value; }
        }

        private Color _HexSelectedForeColor = Color.DarkCyan;
        /// <summary>
        /// 获取或者设置16进制选中数据前景颜色
        /// </summary>
        [Description("16进制选中数据前景颜色"), DefaultValue(typeof(Color), "DarkCyan"), Category("Customs")]
        public Color HexSelectedForeColor {
            get { return _HexSelectedForeColor; }
            set { _HexSelectedForeColor = value; }
        }

        private Color _HexSelectedBackColor = Color.LightGray;
        /// <summary>
        /// 获取或者设置16进制选中数据背景颜色
        /// </summary>
        [Description("16进制选中数据背景颜色"), DefaultValue(typeof(Color), "LightGray"), Category("Customs")]
        public Color HexSelectedBackColor {
            get { return _HexSelectedBackColor; }
            set { _HexSelectedBackColor = value; }
        }

        private Color _ASCIIForeColor = Color.Black;
        /// <summary>
        /// 获取或者设置ASCII前景颜色
        /// </summary>
        [Description("ASCII前景颜色"), DefaultValue(typeof(Color), "Black"), Category("Customs")]
        public Color ASCIIForeColor {
            get { return _ASCIIForeColor; }
            set { _ASCIIForeColor = value; }
        }

        private Color _ASCIIHighForeColor = Color.White;
        /// <summary>
        /// 获取或者设置ASCII高亮数据前景颜色
        /// </summary>
        [Description("ASCII高亮数据前景颜色"), DefaultValue(typeof(Color), "White"), Category("Customs")]
        public Color ASCIIHighForeColor {
            get { return _ASCIIHighForeColor; }
            set { _ASCIIHighForeColor = value; }
        }

        private Color _ASCIIHighBackColor = Color.Blue;
        /// <summary>
        /// 获取或者设置ASCII高亮数据背景颜色
        /// </summary>
        [Description("ASCII高亮数据背景颜色"), DefaultValue(typeof(Color), "Blue"), Category("Customs")]
        public Color ASCIIHighBackColor {
            get { return _ASCIIHighBackColor; }
            set { _ASCIIHighBackColor = value; }
        }

        private Color _ASCIISelectedForeColor = Color.DarkCyan;
        /// <summary>
        /// 获取或者设置ASCII选中数据前景颜色
        /// </summary>
        [Description("ASCII选中数据前景颜色"), DefaultValue(typeof(Color), "DarkCyan"), Category("Customs")]
        public Color ASCIISelectedForeColor {
            get { return _ASCIISelectedForeColor; }
            set { _ASCIISelectedForeColor = value; }
        }

        private Color _ASCIISelectedBackColor = Color.LightGray;
        /// <summary>
        /// 获取或者设置ASCII选中数据背景颜色
        /// </summary>
        [Description("ASCII选中数据背景颜色"), DefaultValue(typeof(Color), "LightGray"), Category("Customs")]
        public Color ASCIISelectedBackColor {
            get { return _ASCIISelectedBackColor; }
            set { _ASCIISelectedBackColor = value; }
        }

        private int _SelectionStartIndex;
        /// <summary>
        /// 获取或者设置选中数据开始索引
        /// </summary>
        [Description("选中数据开始索引"), Browsable(false), DefaultValue(0), Category("Customs")]
        public int SelectionStartIndex {
            get { return _SelectionStartIndex; }
            set {
                if (_SelectionStartIndex == value) return;
                _SelectionStartIndex = value;
                this.Invalidate();
            }
        }

        private int _SelectionEndIndex;
        /// <summary>
        /// 获取或者设置选中数据结束索引
        /// </summary>
        [Description("选中数据结束索引"), Browsable(false), DefaultValue(0), Category("Customs")]
        public int SelectionEndIndex {
            get { return _SelectionEndIndex; }
            set {
                if (_SelectionEndIndex == value) return;
                _SelectionEndIndex = value;
                this.Invalidate();
            }
        }

        private int _SelectedDataIndex;
        /// <summary>
        /// 获取或者设置选中高亮数据索引
        /// </summary>
        [Description("选中高亮数据索引"), Browsable(false), DefaultValue(0), Category("Customs")]
        public int SelectedDataIndex {
            get { return _SelectedDataIndex; }
            set {
                if (value < 0) return;
                if (this._ByteData == null) return;
                if (value >= this._ByteData.Length) value = this._ByteData.Length - 1;
                if (_SelectedDataIndex == value) return;
                _SelectedDataIndex = value;
                this.Invalidate();
            }
        }

        private int _StartAddr;
        /// <summary>
        /// 获取或者设置起始地址
        /// </summary>
        [Description("起始地址"), DefaultValue(0), Category("Customs")]
        public int StartAddr {
            get { return _StartAddr; }
            set {
                if (value < 0) return;
                _StartAddr = value - value % 16;
                this.Invalidate();
            }
        }

        private int _ScrollWidth = 5;
        /// <summary>
        /// 获取或者设置滚动条的宽度
        /// </summary>
        [Description("滚动条的宽度"), DefaultValue(5), Category("Customs")]
        public int ScrollWidth {
            get { return _ScrollWidth; }
            set { _ScrollWidth = value; }
        }

        private int _TopLineStartIndex;
        /// <summary>
        /// 获取或者设置起始绘制行索引
        /// </summary>
        [Description("起始绘制行索引"), Browsable(false), Category("Customs")]
        public int TopLineStartIndex {
            get { return _TopLineStartIndex; }
            set {
                this._TopLineStartIndex = value;
                if (_TopLineStartIndex > m_nLineCount - m_nLinesForDraw + 1) _TopLineStartIndex = m_nLineCount - m_nLinesForDraw + 1;
                if (_TopLineStartIndex < 0) _TopLineStartIndex = 0;
                this.Invalidate();
            }
        }

        private int _LeftOffset;
        /// <summary>
        /// 获取或者设置画布水平位置偏移量
        /// </summary>
        [Description("画布水平位置偏移量"), Browsable(false), DefaultValue(0), Category("Customs")]
        public int LeftOffset {
            get { return this._LeftOffset; }
            set {
                _LeftOffset = value;
                if (_LeftOffset < m_rectHScroll.Width - m_nCanvsWidth) _LeftOffset = m_rectHScroll.Width - m_nCanvsWidth;
                if (_LeftOffset > 0) _LeftOffset = 0;
                this.Invalidate();
            }
        }

        #endregion

        #region protected

        /// <summary>
        /// 绘制所需要的宽度
        /// </summary>
        protected int m_nCanvsWidth;
        /// <summary>
        /// 每个字符的宽度
        /// </summary>
        protected int m_nCharWidth;
        /// <summary>
        /// 总共需要绘制的行数
        /// </summary>
        protected int m_nLineCount;
        /// <summary>
        /// 当前控件高度需要绘制行数
        /// </summary>
        protected int m_nLinesForDraw;
        /// <summary>
        /// 左边线条偏移量
        /// </summary>
        protected int m_nLeftLineOffset;
        /// <summary>
        /// 右边线条偏移量
        /// </summary>
        protected int m_nRightLineOffset;
        /// <summary>
        /// 16进制数据每个字符的偏移量
        /// </summary>
        protected int[] m_nArrHexLeftOffset;
        /// <summary>
        /// ASCII数据每个字符的偏移量
        /// </summary>
        protected int[] m_nArrAscLeftOffset;
        /// <summary>
        /// 当前鼠标是否在垂直滚动条上
        /// </summary>
        protected bool m_bMouseOnVScrollTrack;
        /// <summary>
        /// 当前鼠标是否在水平滚动条上
        /// </summary>
        protected bool m_bMouseOnHScrollTrack;
        /// <summary>
        /// 当前是否正在移动垂直滚动条
        /// </summary>
        protected bool m_bMoveVScroll;
        /// <summary>
        /// 当前是否正在移动水平滚动条
        /// </summary>
        protected bool m_bMoveHScroll;
        /// <summary>
        /// 当前是否正在选择数据
        /// </summary>
        protected bool m_bRunSelect;
        /// <summary>
        /// 当前鼠标相对于控件坐标
        /// </summary>
        protected Point m_ptCurrent;

        #endregion

        private Rectangle m_rectVScroll;
        private Rectangle m_rectVScrollTrack;
        private Rectangle m_rectHScroll;
        private Rectangle m_rectHScrollTrack;

        public HexBox() {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.Font = new Font("Courier New", 10);
            this.BackColor = Color.White;
            this.Width = m_nCanvsWidth + this._ScrollWidth;
            this.Height = (int)(this.Width * 0.618);
        }

        #region override

        public override Font Font {
            get { return base.Font; }
            set {
                int temp = (int)TextRenderer.MeasureText("i", value).Width;
                if (temp != (int)TextRenderer.MeasureText("W", value).Width)
                    throw new ArgumentException("字体必须为等宽字体");
                base.Font = value;
                this.Init();
            }
        }

        protected override void OnResize(EventArgs e) {
            int temp = m_nLinesForDraw;//重新计算绘制行数和滚动条偏移
            m_nLinesForDraw = (int)Math.Ceiling((double)this.Height / this.Font.Height) - 1;
            if (m_nLinesForDraw - temp > 0) this._TopLineStartIndex -= m_nLinesForDraw - temp;
            if (this._TopLineStartIndex < 0) this._TopLineStartIndex = 0;
            if (this.Width - this._ScrollWidth - this._LeftOffset >= m_nCanvsWidth) {
                this._LeftOffset = this.Width - this._ScrollWidth - m_nCanvsWidth;
                if (this._LeftOffset > 0) this._LeftOffset = 0;
            }
            base.OnResize(e);
        }

        protected override void OnMouseMove(MouseEventArgs e) {
            m_ptCurrent = e.Location;
            if (m_rectVScrollTrack.Contains(m_ptCurrent) || m_bMoveVScroll) {
                if (!m_bMouseOnVScrollTrack) {
                    m_bMouseOnVScrollTrack = true;
                    this.Invalidate();
                }
            } else {
                if (m_bMouseOnVScrollTrack) {
                    m_bMouseOnVScrollTrack = false;
                    this.Invalidate();
                }
            }
            if (m_rectHScrollTrack.Contains(m_ptCurrent) || m_bMoveHScroll) {
                if (!m_bMouseOnHScrollTrack) {
                    m_bMouseOnHScrollTrack = true;
                    this.Invalidate();
                }
            } else {
                if (m_bMouseOnHScrollTrack) {
                    m_bMouseOnHScrollTrack = false;
                    this.Invalidate();
                }
            }
            if (m_bMoveVScroll) {
                this.SetVScrollPos(m_ptCurrent.Y);
            } else if (m_bMoveHScroll) {
                this.SetHScrollPos(m_ptCurrent.X);
            } else if (m_bRunSelect) {
                int temp = this.GetSelectedDataIndex();
                if (temp != -1) {
                    if (temp < this._SelectedDataIndex) {
                        this.SelectionEndIndex = this._SelectedDataIndex;
                        this.SelectionStartIndex = temp;
                    } else {
                        this.SelectionStartIndex = this._SelectedDataIndex;
                        this.SelectionEndIndex = temp;
                    }
                }
            }
            base.OnMouseMove(e);
        }

        protected override void OnMouseDown(MouseEventArgs e) {
            this.Focus();
            if (e.Button == MouseButtons.Left) {
                if (m_rectVScroll.Contains(m_ptCurrent)) {
                    this.SetVScrollPos(m_ptCurrent.Y);
                    m_bMoveVScroll = true;
                } else if (m_rectHScroll.Contains(m_ptCurrent)) {
                    this.SetHScrollPos(m_ptCurrent.X);
                    m_bMoveHScroll = true;
                } else {
                    int index = this.GetSelectedDataIndex();
                    if (index != -1) {
                        this.SelectedDataIndex = index;
                        this.SelectionEndIndex = this.SelectionStartIndex = index;
                        m_bRunSelect = true;
                    }
                }
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e) {
            m_bMoveVScroll = false;
            m_bMoveHScroll = false;
            m_bMouseOnHScrollTrack = false;
            m_bMouseOnVScrollTrack = false;
            m_bRunSelect = false;
            this.Invalidate();
            base.OnMouseUp(e);
        }

        protected override void OnMouseLeave(EventArgs e) {
            m_bMouseOnHScrollTrack = false;
            m_bMouseOnVScrollTrack = false;
            this.Invalidate();
            base.OnMouseLeave(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e) {
            if (m_rectHScroll.Contains(m_ptCurrent)) {//优先考虑鼠标是否在水平滚动条上 否则才是垂直滚动
                this.LeftOffset += e.Delta > 0 ? 10 : -10;
            } else {
                this.TopLineStartIndex += e.Delta > 0 ? -3 : 3;
            }
            base.OnMouseWheel(e);
        }

        protected override void OnPaint(PaintEventArgs e) {
            Graphics g = e.Graphics;
            if (this._ByteData != null) {
                this.DrawHex(g);
            }
            this.DrawHead(g);
            this.DrawVScroll(g);
            this.DrawHScroll(g);
            base.OnPaint(e);
        }

        #endregion

        #region customs

        /// <summary>
        /// 初始化数据 如：字符宽度等
        /// </summary>
        protected virtual void Init() {
            if (this.Font != null) {
                string strText = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";
                m_nLinesForDraw = (int)Math.Ceiling((double)(this.Height - this._ScrollWidth) / this.Font.Height) - 1;
                m_nCharWidth = (int)TextRenderer.MeasureText(strText, this.Font).Width / strText.Length;
                m_nLeftLineOffset = m_nCharWidth * 10;
                m_nRightLineOffset = m_nCharWidth * (16 * 3 + 3) + m_nLeftLineOffset;
                m_nCanvsWidth = m_nRightLineOffset + 18 * m_nCharWidth;
            }
            m_nArrHexLeftOffset = new int[16];
            m_nArrAscLeftOffset = new int[16];
            for (int i = 0; i < 16; i++) {
                m_nArrHexLeftOffset[i] = m_nLeftLineOffset + m_nCharWidth + (i * m_nCharWidth * 3 + (i > 7 ? m_nCharWidth : 0));
                m_nArrAscLeftOffset[i] = m_nRightLineOffset + m_nCharWidth + (i * m_nCharWidth);
            }
        }
        /// <summary>
        /// 绘制显示头部
        /// </summary>
        /// <param name="g">绘图设备</param>
        protected virtual void DrawHead(Graphics g) {
            TextRenderer.DrawText(g, "偏移地址", this.Font, new Point(this._LeftOffset, 0), this._HeadColor);
            for (int i = 0; i < 16; i++) {
                TextRenderer.DrawText(g, i.ToString("X").PadLeft(2, '0'), this.Font, new Point(m_nArrHexLeftOffset[i] + this._LeftOffset, 0), this._HeadColor);
            }
            TextRenderer.DrawText(g, codeTableType==0?"系统编码":String.Format("用户码表{0}",reverseCode? "(翻转)" : ""), this.Font, new Point(m_nRightLineOffset + m_nCharWidth + this._LeftOffset, 0), this._HeadColor);
            using (Pen p = new Pen(this._HeadLineColor)) {
                g.DrawLine(p, m_nLeftLineOffset + this._LeftOffset, 0, m_nLeftLineOffset + this._LeftOffset, this.Height);
                g.DrawLine(p, m_nRightLineOffset + this._LeftOffset, 0, m_nRightLineOffset + this._LeftOffset, this.Height);
                g.DrawLine(p, 0, this.Font.Height, this.Width - this._ScrollWidth - 1, this.Font.Height);
            }
        }
        /// <summary>
        /// 绘制16进制数据
        /// </summary>
        /// <param name="g">绘图设备</param>
        protected virtual void DrawHex(Graphics g) {
            int len = this._TopLineStartIndex + m_nLinesForDraw;
            if (len > m_nLineCount) len = m_nLineCount;
            for (int i = this._TopLineStartIndex; i < len; i++) {
                string strDraw = (this._StartAddr + i * 16).ToString("X").PadLeft(8, '0') + "  ";
                //g.DrawString(strDraw, this.Font, Brushes.Blue, this._LeftOffset, (i - this._TopLineStartIndex + 1) * this.Font.Height);
                TextRenderer.DrawText(g, strDraw, this.Font, new Point(this._LeftOffset, (i - this._TopLineStartIndex + 1) * this.Font.Height), this._AddrColor);
                int index = i * 16;
                for (int j = 0; j < 16; j++) {
                    Color clrHexForeColor = this._HexForeColor;
                    Color clrHexBackColor = Color.Transparent;
                    Color clrASCIIForeColor = this._ASCIIForeColor;
                    Color clrASCIIBackColor = Color.Transparent;
                    if (index == this._SelectedDataIndex) {
                        clrHexForeColor = this._HexHighForeColor;
                        clrHexBackColor = this._HexHighBackColor;
                        clrASCIIForeColor = this._ASCIIHighForeColor;
                        clrASCIIBackColor = this._ASCIIHighBackColor;
                    } else if (index >= this._SelectionStartIndex && index <= this._SelectionEndIndex) {
                        clrHexForeColor = this._HexSelectedForeColor;
                        clrHexBackColor = this._HexSelectedBackColor;
                        clrASCIIForeColor = this._ASCIISelectedForeColor;
                        clrASCIIBackColor = this._ASCIISelectedBackColor;
                    }
                    TextRenderer.DrawText(g, this._ByteData[index].ToString("X").PadLeft(2, '0'), this.Font,
                        new Point(m_nArrHexLeftOffset[j] + this._LeftOffset, (i - this._TopLineStartIndex + 1) * this.Font.Height),
                        clrHexForeColor, clrHexBackColor);
                    String dispTxt = "";
                       
                    if (codeTableType==1)
                    {
                        if (codeTable!=null &&(j % 2 == 1))
                        {
                            int dicKey = reverseCode
                                ? (this._ByteData[index] * 256 + this._ByteData[index - 1])
                                : (this._ByteData[index] + this._ByteData[index - 1] * 256);
                            dispTxt = codeTable.ContainsKey(dicKey) ? codeTable[dicKey] : ".";
                        }

                    }
                    else
                    {
                        dispTxt=((char)(this._ByteData[index] == 0 ? (byte)'.' : this._ByteData[index])).ToString();
                    }
                    TextRenderer.DrawText(g, dispTxt, this.Font,
                        new Point(m_nArrAscLeftOffset[j] + this._LeftOffset, (i - this._TopLineStartIndex + 1) * this.Font.Height),
                        clrASCIIForeColor, clrASCIIBackColor);
                    if (++index == this._ByteData.Length) return;
                }
            }
        }
        /// <summary>
        /// 绘制水平滚动条
        /// </summary>
        /// <param name="g">绘图设备</param>
        protected virtual void DrawVScroll(Graphics g) {
            using (SolidBrush sb = new SolidBrush(this._ScrollBackColor)) {
                m_rectVScroll = new Rectangle(this.Width - this._ScrollWidth, 0, this._ScrollWidth, this.Height);
                g.FillRectangle(sb, m_rectVScroll);
                if (this._ByteData == null) return;
                m_rectVScrollTrack = new Rectangle(m_rectVScroll.Left, 0, this._ScrollWidth, 10);
                m_rectVScrollTrack.Y = (int)(m_rectVScroll.Height * ((double)this._TopLineStartIndex / m_nLineCount));
                m_rectVScrollTrack.Height = (int)Math.Ceiling((m_rectVScroll.Height * ((double)(m_nLinesForDraw - 1) / m_nLineCount)));
                if (m_rectVScrollTrack.Height > m_rectVScroll.Height) m_rectVScrollTrack.Height = 0;
                sb.Color = m_bMouseOnVScrollTrack ? this._ScrollHighColor : this._ScrollForeColor;
                g.FillRectangle(sb, m_rectVScrollTrack);
            }
        }
        /// <summary>
        /// 绘制垂直滚动条
        /// </summary>
        /// <param name="g">绘图设备</param>
        protected virtual void DrawHScroll(Graphics g) {
            m_rectHScroll = new Rectangle(0, this.Height - this._ScrollWidth, this.Width - this._ScrollWidth, this._ScrollWidth);
            m_rectHScrollTrack = new Rectangle(0, m_rectHScroll.Top, 10, this._ScrollWidth);
            m_rectHScrollTrack.X = (int)(m_rectHScroll.Width * ((double)-this._LeftOffset / (m_nCanvsWidth)));
            m_rectHScrollTrack.Width = (int)Math.Ceiling(((double)(m_rectHScroll.Width) / m_nCanvsWidth * m_rectHScroll.Width));
            if (m_rectHScrollTrack.Width >= m_rectHScroll.Width) m_rectHScrollTrack.Width = 0;
            using (SolidBrush sb = new SolidBrush(this._ScrollBackColor)) {
                g.FillRectangle(sb, m_rectHScroll);
                sb.Color = m_bMouseOnHScrollTrack ? this._ScrollHighColor : this._ScrollForeColor;
                g.FillRectangle(sb, m_rectHScrollTrack);
            }
        }
        /// <summary>
        /// 设置垂直滚动条位置
        /// </summary>
        /// <param name="y">鼠标y坐标</param>
        protected virtual void SetVScrollPos(int y) {
            int temp = y - m_rectVScrollTrack.Height / 2;
            this.TopLineStartIndex = (int)(m_nLineCount * ((double)temp / m_rectVScroll.Height));
        }
        /// <summary>
        /// 设置水平滚动条位置
        /// </summary>
        /// <param name="x">鼠标x坐标</param>
        protected virtual void SetHScrollPos(int x) {
            int temp = x - m_rectHScrollTrack.Width / 2;
            this.LeftOffset = -(int)(m_nCanvsWidth * ((double)temp / m_rectHScroll.Width));
        }
        /// <summary>
        /// 获取当前鼠标下16进制对应数据索引
        /// </summary>
        /// <returns>索引 -1 表示无效</returns>
        protected virtual int GetSelectedDataIndex() {
            Point ptVirtual = new Point(m_ptCurrent.X + this._LeftOffset, m_ptCurrent.Y);
            if (this._ByteData == null) return -1;
            int x = 0;
            int y = 0;
            bool flag = false;
            y = ptVirtual.Y / this.Font.Height - 1;
            if (ptVirtual.X > m_nLeftLineOffset && ptVirtual.X < m_nArrHexLeftOffset[1]) {
                flag = true;
            } else if (ptVirtual.X > m_nRightLineOffset && ptVirtual.X < m_nArrAscLeftOffset[1]) {
                flag = true;
            } else if (ptVirtual.X > m_nArrHexLeftOffset[15] && ptVirtual.X < m_nRightLineOffset) {
                x = 15;
                flag = true;
            } else if (ptVirtual.X > m_nArrAscLeftOffset[15]) {
                x = 15;
                flag = true;
            } else {
                for (int i = 1; i < m_nArrHexLeftOffset.Length; i++) {
                    if (m_nArrHexLeftOffset[i] >= ptVirtual.X) {
                        x = i - 1;
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    for (int i = 1; i < m_nArrAscLeftOffset.Length; i++) {
                        if (m_nArrAscLeftOffset[i] >= ptVirtual.X) {
                            x = i - 1;
                            flag = true;
                            break;
                        }
                    }
                }
            }
            if (!flag) return -1;
            if (y > m_nLinesForDraw - 1) y = m_nLinesForDraw - 1;
            else if (y < 0) y = 0;
            int temp = (this._TopLineStartIndex + y) * 16 + x;
            return temp >= this._ByteData.Length ? -1 : temp;
        }
        /// <summary>
        /// 设置控件最佳宽度
        /// </summary>
        public virtual void BestWidth() {
            this.Width = m_nCanvsWidth + this._ScrollWidth;
        }

        #endregion
    }
}
