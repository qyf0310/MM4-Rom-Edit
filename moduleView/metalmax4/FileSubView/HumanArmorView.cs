﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.model;
using SharpConfig;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class HumanArmorView : Form
    {
        private XlsNingenBougu currentItem = null;
        public HumanArmorView()
        {
            InitializeComponent();
            init();
            
        }
        public void init()
        {
            List<XlsNingenBougu> datas = DataBaseService.getData(ConstanDef.ArrFileKey_Human_Armors) as List<XlsNingenBougu>;
            searchTree1.loadList(datas);

        }
        private void searchTree1_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree)sender;
            if (control.TreeView.SelectedNode != null)
            {
                loadItem(Convert.ToInt32(control.TreeView.SelectedNode.Tag));
            }

        }
        private void loadItem(int index)
        {
            List<XlsNingenBougu> datas = DataBaseService.getData(ConstanDef.ArrFileKey_Human_Armors) as List<XlsNingenBougu>;
            XlsNingenBougu o = datas[index];
            currentItem = o;
            tbName.Text = o.Name;
            tbRomAddr.Text = String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_Human_Armors)["data_offset"].StringValue, 16) + o.Offset);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in o.Data)
            {
                sb.Append(String.Format("{0:X2} ", b));
            }
            tbRomData.Text = sb.ToString();



            setMaleEquiSel((byte)o.Male);
            setFeMaleEquiSel((byte)o.Female);
            setAnimalEquiSel((byte)o.Animal);

            tbUnknown.Text = o.Unknown1.ToString();
            ncATK.Value = o.Attack;
            ncDEF.Value = o.Defence;
            //            ncSpeed.Value = ((o.Speed & 0x80) > 0 ? (-1) : 1) * (o.Speed & 0x7F);
            ncSpeed.Value = (o.Speed & 0x80) > 0 ? (o.Speed - 256) : o.Speed;
            //            ncMan.Value = ((o.Man & 0x80) > 0 ? -1:1) * (o.Man & 0x7F);
            ncMan.Value = (o.Man & 0x80) > 0 ? (o.Man - 256) : o.Man;

            ncUnknowP1.Value = o.Unknown2;
            ncUnknowP2.Value = o.Unknown3;

            ncFire.Value = (o.FireR & 0x80) > 1 ? 100 : 100 -o.FireR;
            ncLaser.Value =(o.LaserR&0x80)>1?100: 100 - o.LaserR; 
            ncElectri.Value = (o.ElectriR & 0x80) > 1 ? 100 : 100 - o.ElectriR;
            ncSound.Value = (o.SoundR & 0x80) > 1 ? 100 : 100 - o.SoundR; 
            ncGas.Value = (o.GasR & 0x80) > 1 ? 100 : 100 - o.GasR; 
            ncIce.Value = (o.IceR & 0x80) > 1 ? 100 : 100 - o.IceR; 

            tbPart.Text = o.TypeName;
        }

        private void btnSaveToRom_Click(object sender, EventArgs e)
        {
            byte[] bytes = currentItem.Data.ToArray();

            bytes[0] = getMaleEquip();
            bytes[1] = getFeMaleEquip();
            bytes[2] = getAnimalEquip();

            bytes[4] = (byte)ncATK.Value;
            bytes[5] = (byte)ncDEF.Value;
            bytes[6] = (byte) (ncSpeed.Value < 0 ? (ncSpeed.Value +256) : ncSpeed.Value);
            bytes[7] = (byte) (ncMan.Value < 0 ? (ncMan.Value +256) : ncMan.Value);

            bytes[9] = (byte)(100 - ncFire.Value);
            bytes[10] = (byte)(100 - ncLaser.Value);
            bytes[11] = (byte)(100 - ncElectri.Value );
            bytes[12] = (byte)(100 - ncSound.Value );
            bytes[13] = (byte)(100 - ncGas.Value);
            bytes[14] = (byte)(100 - ncIce.Value);

            try
            {
                byte[] newBytes = FileOperateHelper.modifyBin(DataBaseService.getRomPath(), Convert.ToInt32(tbRomAddr.Text, 16), bytes);
                XlsNingenBougu newO = XlsNingenBouguArrDecode.loadData(newBytes, "edit")[0];
                
                
                List<XlsNingenBougu> datas = DataBaseService.getData(ConstanDef.ArrFileKey_Human_Armors) as List<XlsNingenBougu>;
                int index = Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag);
                XlsNingenBougu oldO = datas[index];
                newO.Name = oldO.Name;
                newO.SeqNo = oldO.SeqNo;
                newO.Offset = oldO.Offset;
                datas[index] = newO;
                loadItem(index);
                MessageBox.Show("写入完毕");
            }
            catch (Exception exception)
            {
                MessageBox.Show("写入失败," + exception.Message);
            }
        }

        private void chebEquiAnime_CheckedChanged(object sender, EventArgs e)
        {
            chebEquiDog.Checked = chebEquiAnime.Checked;
            chebEquiBear.Checked = chebEquiAnime.Checked;
        }

        private void chebEquiFM_CheckedChanged(object sender, EventArgs e)
        {
            chebEquiFL.Checked = chebEquiFM.Checked;
            chebEquiFX.Checked = chebEquiFM.Checked;
            chebEquiFB.Checked = chebEquiFM.Checked;
            chebEquiFH.Checked = chebEquiFM.Checked;
            chebEquiFS.Checked = chebEquiFM.Checked;
            chebEquiFY.Checked = chebEquiFM.Checked;
            chebEquiFW.Checked = chebEquiFM.Checked;
            chebEquiFQ.Checked = chebEquiFM.Checked;
        }

        private void chebEquibM_CheckedChanged(object sender, EventArgs e)
        {
            chebEquiML.Checked = chebEquibM.Checked;
            chebEquiMX.Checked = chebEquibM.Checked;
            chebEquiMB.Checked = chebEquibM.Checked;
            chebEquiMH.Checked = chebEquibM.Checked;
            chebEquiMS.Checked = chebEquibM.Checked;
            chebEquiMY.Checked = chebEquibM.Checked;
            chebEquiMW.Checked = chebEquibM.Checked;
            chebEquiMQ.Checked = chebEquibM.Checked;
        }

        private void chebEquiAll_CheckedChanged(object sender, EventArgs e)
        {
            chebEquibM.Checked = chebEquiAll.Checked;
            chebEquiFM.Checked= chebEquiAll.Checked;
            chebEquiAnime.Checked= chebEquiAll.Checked;
        }

        private void setMaleEquiSel(byte data)
        {
            chebEquiML.Checked = (data & 0x01)>0;
            chebEquiMX.Checked = (data & 0x02) > 0;
            chebEquiMB.Checked = (data & 0x04) > 0;
            chebEquiMH.Checked = (data & 0x08) > 0;
            chebEquiMS.Checked = (data & 0x10) > 0;
            chebEquiMY.Checked = (data & 0x20) > 0;
            chebEquiMW.Checked = (data & 0x80) > 0;
            chebEquiMQ.Checked = (data & 0x40) > 0;
        }
        private void setFeMaleEquiSel(byte data)
        {
            chebEquiFL.Checked = (data & 0x01) > 0;
            chebEquiFX.Checked = (data & 0x02) > 0;
            chebEquiFB.Checked = (data & 0x04) > 0;
            chebEquiFH.Checked = (data & 0x08) > 0;
            chebEquiFS.Checked = (data & 0x10) > 0;
            chebEquiFY.Checked = (data & 0x20) > 0;
            chebEquiFW.Checked = (data & 0x80) > 0;
            chebEquiFQ.Checked = (data & 0x40) > 0;
        }
        private void setAnimalEquiSel(byte data)
        {
            chebEquiDog.Checked = (data & 0x01) > 0;
            chebEquiBear.Checked = (data & 0x02) > 0;
        }
        private byte getMaleEquip()
        {
            byte result = 0;
            if (chebEquiML.Checked)
            {
                result |= 0x01;
            }
            if (chebEquiMX.Checked)
            {
                result |= 0x02;
            }
            if (chebEquiMB.Checked)
            {
                result |= 0x04;
            }
            if (chebEquiMH.Checked)
            {
                result |= 0x08;
            }
            if (chebEquiMS.Checked)
            {
                result |= 0x10;
            }
            if (chebEquiMY.Checked)
            {
                result |= 0x20;
            }
            if (chebEquiMW.Checked)
            {
                result |= 0x80;
            }
            if (chebEquiMQ.Checked)
            {
                result |= 0x40;
            }
            return result;
        }
        private byte getFeMaleEquip()
        {
            byte result = 0;
            if (chebEquiFL.Checked)
            {
                result |= 0x01;
            }
            if (chebEquiFX.Checked)
            {
                result |= 0x02;
            }
            if (chebEquiFB.Checked)
            {
                result |= 0x04;
            }
            if (chebEquiFH.Checked)
            {
                result |= 0x08;
            }
            if (chebEquiFS.Checked)
            {
                result |= 0x10;
            }
            if (chebEquiFY.Checked)
            {
                result |= 0x20;
            }
            if (chebEquiFW.Checked)
            {
                result |= 0x80;
            }
            if (chebEquiFQ.Checked)
            {
                result |= 0x40;
            }
            return result;
        }
        private byte getAnimalEquip()
        {
            byte result = 0;
            if (chebEquiDog.Checked)
            {
                result |= 0x01;
            }
            if (chebEquiBear.Checked)
            {
                result |= 0x02;
            }
            return result;
        }
    }
}
