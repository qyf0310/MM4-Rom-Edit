﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsMonsterArrDecode
    {
        public static List<XlsMonster> loadData(byte[] datas, string key)
        {
            List<XlsItemList> itemNames = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            List<XlsNam> monsterNames = DataBaseService.getData("ene_monster.nam") as List<XlsNam>;
            List<XlsMonster> result = new List<XlsMonster>();
            int curLength = 0;
            int offset = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + 117 >= datas.Length)
                {
                    break;
                }
                XlsMonster o = new XlsMonster();
                result.Add(o);
                //                o.SeqNo = index;
                o.Offset = i;


                o.NameOffsetAddr = datas[i + 1] * 256 + datas[i];
                o.Name = "unknown";
                if (monsterNames != null)
                {
                    var firstOrDefault = monsterNames.FirstOrDefault(x => x.Offset == o.NameOffsetAddr);
                    if (firstOrDefault != null)
                        o.Name = firstOrDefault.Text;
                }

                o.Appearance = datas[i + 3] * 256 + datas[i + 2];
                o.Hp = datas[i + 5] * 256 + datas[i + 4];
                o.Attack = datas[i + 7] * 256 + datas[i + 6];
                o.Defence = datas[i + 9] * 256 + datas[i + 8];
                o.Speed = datas[i + 11] * 256 + datas[i + 10];
                o.Exp = datas[i + 13] * 256 + datas[i + 12];
                o.Gold = datas[i + 15] * 256 + datas[i + 14];

                o.SeqNoSub = datas[i + 17] * 256 + datas[i + 16];
                o.SeqNo = datas[i + 19] * 256 + datas[i + 18];

                o.Level = datas[i + 25];
                o.Hit = datas[i + 27];
                o.Parry = datas[i + 28];
                o.ActionTimes = datas[i + 31];

                o.UnknownResist1 = datas[i + 42];// datas[i + 43] * 256 + datas[i + 42];
                o.NormalResist = datas[i + 44];//datas[i + 45] * 256 + datas[i + 44];
                o.FireResist = datas[i + 46];//datas[i + 47] * 256 + datas[i + 46];
                o.IceResist = datas[i + 48];//datas[i + 49] * 256 + datas[i + 48];
                o.ElectriResist = datas[i + 50];// datas[i + 51] * 256 + datas[i + 50];
                o.SoundResist = datas[i + 52];//datas[i + 53] * 256 + datas[i + 52];
                o.GasResist = datas[i + 54];// datas[i + 55] * 256 + datas[i + 54];
                o.LaserRsist = datas[i + 56];//datas[i + 57] * 256 + datas[i + 56];
                o.UnknownResist2 = datas[i + 58];//datas[i + 59] * 256 + datas[i + 58];

                o.Action1Code.AddRange(new List<byte> { datas[i + 60], datas[i + 61], datas[i + 62], datas[i + 63], datas[i + 64], datas[i + 65] });
                o.Action2Code.AddRange(new List<byte> { datas[i + 66], datas[i + 67], datas[i + 68], datas[i + 69], datas[i + 70], datas[i + 71] });
                o.Action3Code.AddRange(new List<byte> { datas[i + 72], datas[i + 73], datas[i + 74], datas[i + 75], datas[i + 76], datas[i + 77] });
                o.Action4Code.AddRange(new List<byte> { datas[i + 78], datas[i + 79], datas[i + 80], datas[i + 81], datas[i + 82], datas[i + 83] });
                o.Action5Code.AddRange(new List<byte> { datas[i + 84], datas[i + 85], datas[i + 86], datas[i + 87], datas[i + 88], datas[i + 89] });
                o.Action6Code.AddRange(new List<byte> { datas[i + 90], datas[i + 91], datas[i + 92], datas[i + 93], datas[i + 94], datas[i + 95] });
                o.Action7Code.AddRange(new List<byte> { datas[i + 96], datas[i + 97], datas[i + 98], datas[i + 99], datas[i + 100], datas[i + 101] });

                o.Drop1 = datas[i + 103] * 256 + datas[i + 102];
                o.Drop1Rate = datas[i + 105] * 256 + datas[i + 104];
                o.Drop1Remark = getDropItemName(itemNames, o.Drop1);

                o.Drop2 = datas[i + 107] * 256 + datas[i + 106];
                o.Drop2Rate = datas[i + 109] * 256 + datas[i + 108];
                o.Drop2Remark = getDropItemName(itemNames, o.Drop2);

                o.Drop3 = datas[i + 111] * 256 + datas[i + 110];
                o.Drop3Rate = datas[i + 113] * 256 + datas[i + 112];
                o.Drop3Remark = getDropItemName(itemNames, o.Drop3);

                o.Drop4 = datas[i + 115] * 256 + datas[i + 114];
                o.Drop4Rate = datas[i + 117] * 256 + datas[i + 116];
                o.Drop4Remark = getDropItemName(itemNames, o.Drop4);




                for (int j = 0; j < 118; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }

                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o,key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null,key,true);
            DataBaseService.addData(key, result);
            return result;
        }


        private static String getDropItemName(List<XlsItemList> itemNames, int data)
        {
            if (data==0)
            {
                return "";
            }
            if (itemNames==null || itemNames.Count==0)
            {
                return "unknown";
            }
            return itemNames[data - 1].Name;
        }
    }
}
