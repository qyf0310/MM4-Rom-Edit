﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.baseService
{
    class HexHelper
    {
        public static byte GetByteHigh(byte val)
        {
            return (byte)(val >> 4);
        }
        public static byte GetByteLow(byte val)
        {
            return (byte)(val % 16);
        }
    }
}
