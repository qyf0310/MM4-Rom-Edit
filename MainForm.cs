﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.baseService;
using MM4RomEdit.module.metalmax4;
using MM4RomEdit.moduleView.metalmax4;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.FileSubView;

namespace MM4RomEdit
{
    public partial class MainForm : Form
    {
        Dictionary<TabPage, Form> page2forms = new Dictionary<TabPage, Form>();
        Dictionary<String,int> infoKeyExist=new Dictionary<string, int>();
        public MainForm()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;//虽然不推荐这种方式，不过这种工具影响不大
            tsbCloseRom.Enabled = false;
            tsbOpenRom.Enabled = true;
            DataBaseService.setOffsetConfigFilePath(System.Windows.Forms.Application.StartupPath + @"\config\config.cfg");
            DataBaseService.setAnimeNamConfigFilePath(System.Windows.Forms.Application.StartupPath + @"\config\animeNam.cfg");
            DataBaseService.setFiledebugConfiguration(System.Windows.Forms.Application.StartupPath + @"\config\filedebug.cfg");
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            FileListView fileListView=new FileListView();
            loadView(tpMM4View, fileListView);
            fileListView.init(System.Windows.Forms.Application.StartupPath + @"\3dstooldump\");//

            StartupLoad startupLoad=new StartupLoad();
            startupLoad.ProgressChange += StartupLoad_ProgressChange;
            startupLoad.begin(null);
            BroadCastService.ProgressChange += BroadCastService_ProgressChange;
        }

        private void BroadCastService_ProgressChange(object sender, BroadCastService.ProgressHandleArgs e)
        {
            if (infoKeyExist.ContainsKey(e.key))
            {
                tbSysInfo.AppendText(String.Format("{0} {1} {2}\r\n", DateTime.Now.ToString("HH:mm:ss.fff"), e.Code, e.Msg));
                tbSysInfo.ScrollToCaret();
                if (e.isComplete)
                {
                    if (e.Code == ProcessStateEnum.SUCCESS || e.Code == ProcessStateEnum.NORMAL)
                    {
                        addDynamicTabView(tcMain, "所有物品", new ItemView());
                        addDynamicTabView(tcMain, "怪物数据", new MonsterView());
                        addDynamicTabView(tcMain, "战车初始数据", new CarInitView());
                        addDynamicTabView(tcMain, "C装置", new CUnitView());
                        addDynamicTabView(tcMain, "引擎", new EngineView());
                        addDynamicTabView(tcMain, "底盘", new ChassisView());
                        addDynamicTabView(tcMain, "战车武器", new CarWeponView());
                        addDynamicTabView(tcMain, "人类武器", new HumanWeponView());
                        addDynamicTabView(tcMain, "人类防具", new HumanArmorView());
                    }
                    else
                    {
                        tsbCloseRom.Enabled = false;
                        tsbOpenRom.Enabled = true;
                    }

                }
            }
        }

        private void StartupLoad_ProgressChange(object sender, baseService.AbstractDataProcess.ProgressHandleArgs e)
        {
            tbSysInfo.AppendText(String.Format("{0} {1} {2}\r\n",DateTime.Now.ToString("HH:mm:ss.fff"),e.Code,e.Msg));
            tbSysInfo.ScrollToCaret();
        }

        private void addDynamicTabView(TabControl tabControl,String title,Form view)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { addDynamicTabView(tabControl,title ,view); }));
                return;
            }

            TabPage tp = new TabPage(title);
            tabControl.TabPages.Add(tp);
            loadView(tp, view);
        }
        private void loadView(TabPage page, Form form)
        {

            if (!page2forms.ContainsKey(page))
            {

                form.FormBorderStyle = FormBorderStyle.None;
                form.TopLevel = false;
                form.Show();
                form.Parent = page;
                form.Dock = DockStyle.Fill;
                page2forms.Add(page, form);
            }
        }

        private void tsbOpenRom_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog=new OpenFileDialog();
            dialog.Multiselect = false;
//            dialog.DefaultExt = "cci";
            dialog.RestoreDirectory = true;
            if (dialog.ShowDialog()==DialogResult.OK)
            {
                tcMain.SelectedIndex = 0;
                while (tcMain.TabCount>2)
                {
                    tcMain.TabPages.RemoveAt(2);
                }
                infoKeyExist.Clear();
//                String filePath = @"F:\GameRoms\3ds\MM4Fix515Decrypt.cci";
                String filePath = dialog.FileNames[0];
                DataBaseService.setRomPath(filePath);
                
                infoKeyExist.Add(filePath,1);
                DataProcessWorker.run(filePath, CCIRomLoad.loadData);
                tsbOpenRom.Enabled = false;
                tsbCloseRom.Enabled = true;
            }
        }

        private void tsbCloseRom_Click(object sender, EventArgs e)
        {
            while (tcMain.TabCount > 2)
            {
                tcMain.TabPages.RemoveAt(2);
                tsbCloseRom.Enabled = false;
                tsbOpenRom.Enabled = true;
            }
        }
    }
}
