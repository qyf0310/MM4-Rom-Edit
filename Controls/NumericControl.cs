﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MM4RomEdit.Controls
{
    public partial class NumericControl : NumericUpDown
    {
        protected bool ClickFlag;

        protected override void OnGotFocus(EventArgs e)
        {
            Select(0, Text.Length);
            base.OnGotFocus(e);
        }

        protected override void OnLostFocus(EventArgs e)
        {
            ClickFlag = false;
            base.OnLostFocus(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (!ClickFlag)
            {
                Select(0, Text.Length);
                ClickFlag = true;
            }
            base.OnMouseClick(e);
        }

        protected override void OnValueChanged(EventArgs e)
        {
            if (MinNumberControl != null && Value < MinNumberControl.Value)
            {
                Value = MinNumberControl.Value;
            }
            if (MaxNumberControl != null && Value > MaxNumberControl.Value)
            {
                Value = MaxNumberControl.Value;
            }
            base.OnValueChanged(e);
        }

        public NumericControl1 MinNumberControl { get; set; }

        public NumericControl1 MaxNumberControl { get; set; }
    }
}
