﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.baseService;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.FileSubView;
using SharpConfig;

namespace MM4RomEdit.module.metalmax4
{
    public partial class FileListView : Form
    {
        Dictionary<String, Form> file2forms = new Dictionary<String, Form>();
        public FileListView()
        {
            InitializeComponent();
        }

        private void MetalMax4View_Load(object sender, EventArgs e)
        {
            splitContainerMain.Dock=DockStyle.Fill;
            searchTree.Parent = splitContainerMain.Panel1;
            searchTree.Dock=DockStyle.Fill;

        }

        public void init(String dumpSrcDir)
        {
            if (!Directory.Exists(dumpSrcDir))
            {
                return;
            }
            List<String> ignoreExtList=new List<string> { ".exe", ".bat"};//, ".nds"
            String resultMsg=searchTree.loadDir(dumpSrcDir, ignoreExtList);
            if (!String.IsNullOrEmpty(resultMsg))
                MessageBox.Show("加载dumpfiles失败," + resultMsg);
        }

        private void searchTree_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree) sender;
            if (control.TreeView.SelectedNode != null)
            {

                String filePath = control.TreeView.SelectedNode.Name;
                loadSubView(filePath);
                

//                refreshHexBox(filePath);
            }
            
        }

        private void loadSubView(String filePath)
        {
            //            key = @"D:\Study\5-hack\rom\mm4\cci\cxi0\romfs\xls\data\itemlist.nam";
            //            String charsetFile = @"D:\Study\5-hack\rom\mm4\cci\cxi0\romfs\font_main_charset.txt";
            //            String charsetXllt = @"D:\Study\5-hack\rom\mm4\cci\cxi0\romfs\font_main_charset.xllt";
            TextListView form = null;
            string key = filePath.Substring(filePath.LastIndexOf("\\") + 1);
            if (file2forms.ContainsKey(key))
            {
                form = file2forms[key] as TextListView;
                form.BringToFront();
                return;
            }
            
            
            if (key.EndsWith(".nam", StringComparison.OrdinalIgnoreCase) || key.EndsWith(".msg", StringComparison.OrdinalIgnoreCase) || key.EndsWith(".set", StringComparison.OrdinalIgnoreCase))
            {
                byte[] datas = FileOperateHelper.readAllBytes(filePath);
                form = new TextListView(XlsNamDecode.loadData,datas, key);

            }
//            else if (key.EndsWith("itemlist.arr", StringComparison.OrdinalIgnoreCase))
//            {
//                if (!DataBaseService.containsData("itemlist.nam"))
//                {
//                    MessageBox.Show("需要先加载itemList.nam文件的数据");
//                    return;
//                }
//                byte[] datas = FileOperateHelper.readAllBytes(filePath);
//                form = new TextListView(XlsItemArrDecode.loadData, datas, key);
//
//            }
            else if(key.EndsWith(".arr", StringComparison.OrdinalIgnoreCase) || key.EndsWith(".bin", StringComparison.OrdinalIgnoreCase))
            {
                byte[] datas = FileOperateHelper.readAllBytes(filePath);
                Section section = DataBaseService.getFiledebugConfiguration("block");
                key = section["splitlen"].StringValue + "#" + key;
                if (file2forms.ContainsKey(key))
                {
                    form = file2forms[key] as TextListView;
                    form.BringToFront();
                    return;
                }
                form = new TextListView(XlsArrDecode.loadData, datas, key);
            }
            //if (key.EndsWith("cunit.arr", StringComparison.OrdinalIgnoreCase))
            //{
            //    byte[] datas = FileOperateHelper.readAllBytes(filePath);
            //    form = new TextListView(XlsCUnitArrDecode.loadData, datas, key);
            //}
            //else if (key.EndsWith("engine.arr", StringComparison.OrdinalIgnoreCase))
            //{
            //    byte[] datas = FileOperateHelper.readAllBytes(filePath);
            //    form = new TextListView(XlsEnginArrDecode.loadData, datas, key);
            //}
            //else if (key.EndsWith("carwepon.arr", StringComparison.OrdinalIgnoreCase))
            //{
            //    byte[] datas = FileOperateHelper.readAllBytes(filePath);
            //    form = new TextListView(XlsCarWeponArrDecode.loadData,datas, key);
            //}
            //else if (key.EndsWith("chassis.arr", StringComparison.OrdinalIgnoreCase))
            //{
            //    byte[] datas = FileOperateHelper.readAllBytes(filePath);
            //    form = new TextListView(XlsChassisArrDecode.loadData,datas, key);
            //}
            //else if (key.EndsWith("ene_monster.arr", StringComparison.OrdinalIgnoreCase))
            //{
            //    byte[] datas = FileOperateHelper.readAllBytes(filePath);
            //    form = new TextListView(XlsMonsterArrDecode.loadData,datas, key);
            //}
            //else if (key.EndsWith("btl_actdata.arr", StringComparison.OrdinalIgnoreCase))
            //{
            //    byte[] datas = FileOperateHelper.readAllBytes(filePath);
            //    form = new TextListView(XlsBtlActdataArrDecodse.loadData, datas, key);
            //}
            //else if (key.EndsWith("btl_animlist.arr", StringComparison.OrdinalIgnoreCase))
            //{
            //    byte[] datas = FileOperateHelper.readAllBytes(filePath);
            //    form = new TextListView(XlsBtlAnimeListArrDecodse.loadData, datas, key);
            //}
            //else if (key.EndsWith("carinit_initdata.arr", StringComparison.OrdinalIgnoreCase))
            //{
            //    byte[] datas = FileOperateHelper.readAllBytes(filePath);
            //    form = new TextListView(XlsCarInitArrDecode.loadData, datas, key);
            //}
            if (form != null)
            {
                file2forms.Add(key, form);
                form.FormBorderStyle = FormBorderStyle.None;
                form.TopLevel = false;
                form.Show();
                form.Parent = groupBox1;
                form.Dock = DockStyle.Fill;
                form.ReLoadData();
                form.BringToFront();
            }
        }

    }
}
