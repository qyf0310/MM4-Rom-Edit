﻿using MM4RomEdit.Controls;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    partial class HumanWeponView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbAttackResult = new System.Windows.Forms.TextBox();
            this.cbbAttribute = new System.Windows.Forms.ComboBox();
            this.cbbRange = new System.Windows.Forms.ComboBox();
            this.cbbAnime = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSaveToRom = new System.Windows.Forms.Button();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbRomAddr = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbRomData = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tbUnknown1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ncATK = new MM4RomEdit.Controls.NumericControl();
            this.searchTree1 = new MM4RomEdit.Controls.SearchTree();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chebEquiFQ = new System.Windows.Forms.CheckBox();
            this.chebEquiMQ = new System.Windows.Forms.CheckBox();
            this.chebEquiFW = new System.Windows.Forms.CheckBox();
            this.chebEquiMW = new System.Windows.Forms.CheckBox();
            this.chebEquiFY = new System.Windows.Forms.CheckBox();
            this.chebEquiMY = new System.Windows.Forms.CheckBox();
            this.chebEquiFS = new System.Windows.Forms.CheckBox();
            this.chebEquiMS = new System.Windows.Forms.CheckBox();
            this.chebEquiFH = new System.Windows.Forms.CheckBox();
            this.chebEquiMH = new System.Windows.Forms.CheckBox();
            this.chebEquiFB = new System.Windows.Forms.CheckBox();
            this.chebEquiMB = new System.Windows.Forms.CheckBox();
            this.chebEquiFX = new System.Windows.Forms.CheckBox();
            this.chebEquiMX = new System.Windows.Forms.CheckBox();
            this.chebEquiFL = new System.Windows.Forms.CheckBox();
            this.chebEquiML = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chebEquiBear = new System.Windows.Forms.CheckBox();
            this.chebEquiDog = new System.Windows.Forms.CheckBox();
            this.chebEquiAnime = new System.Windows.Forms.CheckBox();
            this.chebEquiFM = new System.Windows.Forms.CheckBox();
            this.chebEquibM = new System.Windows.Forms.CheckBox();
            this.chebEquiAll = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.chName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAnime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chRange = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chEffect = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncATK)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(228, 6);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(240, 21);
            this.tbName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "名称    ：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(364, 44);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 48;
            this.label7.Text = "攻击：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.tbAttackResult);
            this.groupBox1.Controls.Add(this.cbbAttribute);
            this.groupBox1.Controls.Add(this.cbbRange);
            this.groupBox1.Controls.Add(this.cbbAnime);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(480, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 260);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "攻击属性";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 192);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 12);
            this.label18.TabIndex = 77;
            this.label18.Text = "效果台词：";
            // 
            // tbAttackResult
            // 
            this.tbAttackResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAttackResult.Location = new System.Drawing.Point(76, 192);
            this.tbAttackResult.Multiline = true;
            this.tbAttackResult.Name = "tbAttackResult";
            this.tbAttackResult.ReadOnly = true;
            this.tbAttackResult.Size = new System.Drawing.Size(167, 57);
            this.tbAttackResult.TabIndex = 76;
            // 
            // cbbAttribute
            // 
            this.cbbAttribute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAttribute.FormattingEnabled = true;
            this.cbbAttribute.Location = new System.Drawing.Point(77, 164);
            this.cbbAttribute.Name = "cbbAttribute";
            this.cbbAttribute.Size = new System.Drawing.Size(166, 20);
            this.cbbAttribute.TabIndex = 64;
            this.cbbAttribute.SelectedIndexChanged += new System.EventHandler(this.cbbAttribute_SelectedIndexChanged);
            // 
            // cbbRange
            // 
            this.cbbRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbRange.FormattingEnabled = true;
            this.cbbRange.Location = new System.Drawing.Point(77, 108);
            this.cbbRange.Name = "cbbRange";
            this.cbbRange.Size = new System.Drawing.Size(166, 20);
            this.cbbRange.TabIndex = 63;
            // 
            // cbbAnime
            // 
            this.cbbAnime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAnime.FormattingEnabled = true;
            this.cbbAnime.Location = new System.Drawing.Point(77, 48);
            this.cbbAnime.Name = "cbbAnime";
            this.cbbAnime.Size = new System.Drawing.Size(166, 20);
            this.cbbAnime.TabIndex = 62;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 167);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 61;
            this.label13.Text = "效果：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 111);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 60;
            this.label12.Text = "范围：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 59;
            this.label11.Text = "动画：";
            // 
            // btnSaveToRom
            // 
            this.btnSaveToRom.Location = new System.Drawing.Point(480, 278);
            this.btnSaveToRom.Name = "btnSaveToRom";
            this.btnSaveToRom.Size = new System.Drawing.Size(86, 23);
            this.btnSaveToRom.TabIndex = 63;
            this.btnSaveToRom.Text = "写入ROM";
            this.btnSaveToRom.UseVisualStyleBackColor = true;
            this.btnSaveToRom.Click += new System.EventHandler(this.btnSaveToRom_Click);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(425, 280);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(44, 21);
            this.tbLength.TabIndex = 62;
            this.tbLength.Text = "12";
            this.tbLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(378, 283);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 61;
            this.label38.Text = "长度：";
            // 
            // tbRomAddr
            // 
            this.tbRomAddr.Location = new System.Drawing.Point(228, 280);
            this.tbRomAddr.Name = "tbRomAddr";
            this.tbRomAddr.ReadOnly = true;
            this.tbRomAddr.Size = new System.Drawing.Size(136, 21);
            this.tbRomAddr.TabIndex = 60;
            this.tbRomAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(157, 283);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 12);
            this.label37.TabIndex = 59;
            this.label37.Text = "ROM地址：";
            // 
            // tbRomData
            // 
            this.tbRomData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRomData.Location = new System.Drawing.Point(159, 324);
            this.tbRomData.Multiline = true;
            this.tbRomData.Name = "tbRomData";
            this.tbRomData.ReadOnly = true;
            this.tbRomData.Size = new System.Drawing.Size(882, 163);
            this.tbRomData.TabIndex = 65;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(157, 309);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 12);
            this.label39.TabIndex = 64;
            this.label39.Text = "ROM数据：";
            // 
            // tbUnknown1
            // 
            this.tbUnknown1.Location = new System.Drawing.Point(411, 86);
            this.tbUnknown1.Name = "tbUnknown1";
            this.tbUnknown1.ReadOnly = true;
            this.tbUnknown1.Size = new System.Drawing.Size(58, 21);
            this.tbUnknown1.TabIndex = 69;
            this.tbUnknown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(364, 89);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 12);
            this.label15.TabIndex = 68;
            this.label15.Text = "不明1：";
            // 
            // ncATK
            // 
            this.ncATK.Location = new System.Drawing.Point(411, 42);
            this.ncATK.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncATK.MaxNumberControl = null;
            this.ncATK.MinNumberControl = null;
            this.ncATK.Name = "ncATK";
            this.ncATK.Size = new System.Drawing.Size(58, 21);
            this.ncATK.TabIndex = 49;
            this.ncATK.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // searchTree1
            // 
            this.searchTree1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTree1.Location = new System.Drawing.Point(2, 2);
            this.searchTree1.Name = "searchTree1";
            this.searchTree1.Size = new System.Drawing.Size(149, 495);
            this.searchTree1.TabIndex = 0;
            this.searchTree1.DoubleClick += new System.EventHandler(this.searchTree1_DoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chebEquiFQ);
            this.groupBox2.Controls.Add(this.chebEquiMQ);
            this.groupBox2.Controls.Add(this.chebEquiFW);
            this.groupBox2.Controls.Add(this.chebEquiMW);
            this.groupBox2.Controls.Add(this.chebEquiFY);
            this.groupBox2.Controls.Add(this.chebEquiMY);
            this.groupBox2.Controls.Add(this.chebEquiFS);
            this.groupBox2.Controls.Add(this.chebEquiMS);
            this.groupBox2.Controls.Add(this.chebEquiFH);
            this.groupBox2.Controls.Add(this.chebEquiMH);
            this.groupBox2.Controls.Add(this.chebEquiFB);
            this.groupBox2.Controls.Add(this.chebEquiMB);
            this.groupBox2.Controls.Add(this.chebEquiFX);
            this.groupBox2.Controls.Add(this.chebEquiMX);
            this.groupBox2.Controls.Add(this.chebEquiFL);
            this.groupBox2.Controls.Add(this.chebEquiML);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.chebEquiBear);
            this.groupBox2.Controls.Add(this.chebEquiDog);
            this.groupBox2.Controls.Add(this.chebEquiAnime);
            this.groupBox2.Controls.Add(this.chebEquiFM);
            this.groupBox2.Controls.Add(this.chebEquibM);
            this.groupBox2.Controls.Add(this.chebEquiAll);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(157, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(201, 241);
            this.groupBox2.TabIndex = 70;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "装备";
            // 
            // chebEquiFQ
            // 
            this.chebEquiFQ.AutoSize = true;
            this.chebEquiFQ.Location = new System.Drawing.Point(101, 222);
            this.chebEquiFQ.Name = "chebEquiFQ";
            this.chebEquiFQ.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFQ.TabIndex = 100;
            this.chebEquiFQ.UseVisualStyleBackColor = true;
            // 
            // chebEquiMQ
            // 
            this.chebEquiMQ.AutoSize = true;
            this.chebEquiMQ.Location = new System.Drawing.Point(59, 221);
            this.chebEquiMQ.Name = "chebEquiMQ";
            this.chebEquiMQ.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMQ.TabIndex = 99;
            this.chebEquiMQ.UseVisualStyleBackColor = true;
            // 
            // chebEquiFW
            // 
            this.chebEquiFW.AutoSize = true;
            this.chebEquiFW.Location = new System.Drawing.Point(101, 197);
            this.chebEquiFW.Name = "chebEquiFW";
            this.chebEquiFW.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFW.TabIndex = 98;
            this.chebEquiFW.UseVisualStyleBackColor = true;
            // 
            // chebEquiMW
            // 
            this.chebEquiMW.AutoSize = true;
            this.chebEquiMW.Location = new System.Drawing.Point(59, 196);
            this.chebEquiMW.Name = "chebEquiMW";
            this.chebEquiMW.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMW.TabIndex = 97;
            this.chebEquiMW.UseVisualStyleBackColor = true;
            // 
            // chebEquiFY
            // 
            this.chebEquiFY.AutoSize = true;
            this.chebEquiFY.Location = new System.Drawing.Point(101, 172);
            this.chebEquiFY.Name = "chebEquiFY";
            this.chebEquiFY.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFY.TabIndex = 96;
            this.chebEquiFY.UseVisualStyleBackColor = true;
            // 
            // chebEquiMY
            // 
            this.chebEquiMY.AutoSize = true;
            this.chebEquiMY.Location = new System.Drawing.Point(59, 171);
            this.chebEquiMY.Name = "chebEquiMY";
            this.chebEquiMY.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMY.TabIndex = 95;
            this.chebEquiMY.UseVisualStyleBackColor = true;
            // 
            // chebEquiFS
            // 
            this.chebEquiFS.AutoSize = true;
            this.chebEquiFS.Location = new System.Drawing.Point(101, 147);
            this.chebEquiFS.Name = "chebEquiFS";
            this.chebEquiFS.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFS.TabIndex = 94;
            this.chebEquiFS.UseVisualStyleBackColor = true;
            // 
            // chebEquiMS
            // 
            this.chebEquiMS.AutoSize = true;
            this.chebEquiMS.Location = new System.Drawing.Point(59, 146);
            this.chebEquiMS.Name = "chebEquiMS";
            this.chebEquiMS.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMS.TabIndex = 93;
            this.chebEquiMS.UseVisualStyleBackColor = true;
            // 
            // chebEquiFH
            // 
            this.chebEquiFH.AutoSize = true;
            this.chebEquiFH.Location = new System.Drawing.Point(101, 123);
            this.chebEquiFH.Name = "chebEquiFH";
            this.chebEquiFH.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFH.TabIndex = 92;
            this.chebEquiFH.UseVisualStyleBackColor = true;
            // 
            // chebEquiMH
            // 
            this.chebEquiMH.AutoSize = true;
            this.chebEquiMH.Location = new System.Drawing.Point(59, 122);
            this.chebEquiMH.Name = "chebEquiMH";
            this.chebEquiMH.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMH.TabIndex = 91;
            this.chebEquiMH.UseVisualStyleBackColor = true;
            // 
            // chebEquiFB
            // 
            this.chebEquiFB.AutoSize = true;
            this.chebEquiFB.Location = new System.Drawing.Point(101, 100);
            this.chebEquiFB.Name = "chebEquiFB";
            this.chebEquiFB.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFB.TabIndex = 90;
            this.chebEquiFB.UseVisualStyleBackColor = true;
            // 
            // chebEquiMB
            // 
            this.chebEquiMB.AutoSize = true;
            this.chebEquiMB.Location = new System.Drawing.Point(59, 99);
            this.chebEquiMB.Name = "chebEquiMB";
            this.chebEquiMB.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMB.TabIndex = 89;
            this.chebEquiMB.UseVisualStyleBackColor = true;
            // 
            // chebEquiFX
            // 
            this.chebEquiFX.AutoSize = true;
            this.chebEquiFX.Location = new System.Drawing.Point(101, 76);
            this.chebEquiFX.Name = "chebEquiFX";
            this.chebEquiFX.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFX.TabIndex = 88;
            this.chebEquiFX.UseVisualStyleBackColor = true;
            // 
            // chebEquiMX
            // 
            this.chebEquiMX.AutoSize = true;
            this.chebEquiMX.Location = new System.Drawing.Point(59, 75);
            this.chebEquiMX.Name = "chebEquiMX";
            this.chebEquiMX.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMX.TabIndex = 87;
            this.chebEquiMX.UseVisualStyleBackColor = true;
            // 
            // chebEquiFL
            // 
            this.chebEquiFL.AutoSize = true;
            this.chebEquiFL.Location = new System.Drawing.Point(101, 51);
            this.chebEquiFL.Name = "chebEquiFL";
            this.chebEquiFL.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFL.TabIndex = 86;
            this.chebEquiFL.UseVisualStyleBackColor = true;
            // 
            // chebEquiML
            // 
            this.chebEquiML.AutoSize = true;
            this.chebEquiML.Location = new System.Drawing.Point(59, 50);
            this.chebEquiML.Name = "chebEquiML";
            this.chebEquiML.Size = new System.Drawing.Size(15, 14);
            this.chebEquiML.TabIndex = 85;
            this.chebEquiML.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 223);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 84;
            this.label10.Text = "骑";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 83;
            this.label9.Text = "舞";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 173);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 82;
            this.label8.Text = "艺";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 81;
            this.label6.Text = "摔";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 80;
            this.label5.Text = "护";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 79;
            this.label4.Text = "兵";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 78;
            this.label2.Text = "械";
            // 
            // chebEquiBear
            // 
            this.chebEquiBear.AutoSize = true;
            this.chebEquiBear.Location = new System.Drawing.Point(147, 74);
            this.chebEquiBear.Name = "chebEquiBear";
            this.chebEquiBear.Size = new System.Drawing.Size(36, 16);
            this.chebEquiBear.TabIndex = 77;
            this.chebEquiBear.Text = "熊";
            this.chebEquiBear.UseVisualStyleBackColor = true;
            // 
            // chebEquiDog
            // 
            this.chebEquiDog.AutoSize = true;
            this.chebEquiDog.Location = new System.Drawing.Point(147, 52);
            this.chebEquiDog.Name = "chebEquiDog";
            this.chebEquiDog.Size = new System.Drawing.Size(36, 16);
            this.chebEquiDog.TabIndex = 76;
            this.chebEquiDog.Text = "犬";
            this.chebEquiDog.UseVisualStyleBackColor = true;
            // 
            // chebEquiAnime
            // 
            this.chebEquiAnime.AutoSize = true;
            this.chebEquiAnime.Location = new System.Drawing.Point(147, 20);
            this.chebEquiAnime.Name = "chebEquiAnime";
            this.chebEquiAnime.Size = new System.Drawing.Size(48, 16);
            this.chebEquiAnime.TabIndex = 75;
            this.chebEquiAnime.Text = "动物";
            this.chebEquiAnime.UseVisualStyleBackColor = true;
            this.chebEquiAnime.CheckedChanged += new System.EventHandler(this.chebEquiAnime_CheckedChanged);
            // 
            // chebEquiFM
            // 
            this.chebEquiFM.AutoSize = true;
            this.chebEquiFM.Location = new System.Drawing.Point(101, 20);
            this.chebEquiFM.Name = "chebEquiFM";
            this.chebEquiFM.Size = new System.Drawing.Size(36, 16);
            this.chebEquiFM.TabIndex = 74;
            this.chebEquiFM.Text = "女";
            this.chebEquiFM.UseVisualStyleBackColor = true;
            this.chebEquiFM.CheckedChanged += new System.EventHandler(this.chebEquiFM_CheckedChanged);
            // 
            // chebEquibM
            // 
            this.chebEquibM.AutoSize = true;
            this.chebEquibM.Location = new System.Drawing.Point(59, 20);
            this.chebEquibM.Name = "chebEquibM";
            this.chebEquibM.Size = new System.Drawing.Size(36, 16);
            this.chebEquibM.TabIndex = 73;
            this.chebEquibM.Text = "男";
            this.chebEquibM.UseVisualStyleBackColor = true;
            this.chebEquibM.CheckedChanged += new System.EventHandler(this.chebEquibM_CheckedChanged);
            // 
            // chebEquiAll
            // 
            this.chebEquiAll.AutoSize = true;
            this.chebEquiAll.Location = new System.Drawing.Point(8, 20);
            this.chebEquiAll.Name = "chebEquiAll";
            this.chebEquiAll.Size = new System.Drawing.Size(36, 16);
            this.chebEquiAll.TabIndex = 72;
            this.chebEquiAll.Text = "全";
            this.chebEquiAll.UseVisualStyleBackColor = true;
            this.chebEquiAll.CheckedChanged += new System.EventHandler(this.chebEquiAll_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 71;
            this.label1.Text = "猎";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.listView1);
            this.groupBox3.Location = new System.Drawing.Point(735, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(306, 266);
            this.groupBox3.TabIndex = 79;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "同属性武器";
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chName,
            this.chAnime,
            this.chRange,
            this.chEffect});
            this.listView1.Location = new System.Drawing.Point(6, 20);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(294, 240);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // chName
            // 
            this.chName.Text = "名称";
            this.chName.Width = 120;
            // 
            // chAnime
            // 
            this.chAnime.Text = "动画";
            this.chAnime.Width = 30;
            // 
            // chRange
            // 
            this.chRange.Text = "范围";
            this.chRange.Width = 30;
            // 
            // chEffect
            // 
            this.chEffect.Text = "效果";
            this.chEffect.Width = 30;
            // 
            // HumanWeponView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 499);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tbUnknown1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tbRomData);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.btnSaveToRom);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.tbRomAddr);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ncATK);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.searchTree1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HumanWeponView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "CarView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncATK)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.SearchTree searchTree1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private Controls.NumericControl ncATK;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbbAttribute;
        private System.Windows.Forms.ComboBox cbbRange;
        private System.Windows.Forms.ComboBox cbbAnime;
        private System.Windows.Forms.Button btnSaveToRom;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbRomAddr;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbRomData;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbUnknown1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbAttackResult;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chebEquiAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chebEquiAnime;
        private System.Windows.Forms.CheckBox chebEquiFM;
        private System.Windows.Forms.CheckBox chebEquibM;
        private System.Windows.Forms.CheckBox chebEquiBear;
        private System.Windows.Forms.CheckBox chebEquiDog;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chebEquiFL;
        private System.Windows.Forms.CheckBox chebEquiML;
        private System.Windows.Forms.CheckBox chebEquiFQ;
        private System.Windows.Forms.CheckBox chebEquiMQ;
        private System.Windows.Forms.CheckBox chebEquiFW;
        private System.Windows.Forms.CheckBox chebEquiMW;
        private System.Windows.Forms.CheckBox chebEquiFY;
        private System.Windows.Forms.CheckBox chebEquiMY;
        private System.Windows.Forms.CheckBox chebEquiFS;
        private System.Windows.Forms.CheckBox chebEquiMS;
        private System.Windows.Forms.CheckBox chebEquiFH;
        private System.Windows.Forms.CheckBox chebEquiMH;
        private System.Windows.Forms.CheckBox chebEquiFB;
        private System.Windows.Forms.CheckBox chebEquiMB;
        private System.Windows.Forms.CheckBox chebEquiFX;
        private System.Windows.Forms.CheckBox chebEquiMX;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader chName;
        private System.Windows.Forms.ColumnHeader chAnime;
        private System.Windows.Forms.ColumnHeader chRange;
        private System.Windows.Forms.ColumnHeader chEffect;
    }
}