﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsEngine:ModelBase
    {
        private int seqNo;
        private String name;
        private int remodelCost;
        private int weight;
        private int initDefen;
        private int maxDefen;
        private int loadPower;
        
        private int generation;
        private int updateLimit;
        private String updateDesc;
        private int ability;
        private String abilityName;
        private int unknown;
        
        private List<byte> data = new List<byte>();
        private long offset = 0;
        public override string getText()
        {
            return name;
        }
        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int RemodelCost
        {
            get { return remodelCost; }
            set { remodelCost = value; }
        }

        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public int InitDefen
        {
            get { return initDefen; }
            set { initDefen = value; }
        }

        public int MaxDefen
        {
            get { return maxDefen; }
            set { maxDefen = value; }
        }

        public int LoadPower
        {
            get { return loadPower; }
            set { loadPower = value; }
        }

        public int Generation
        {
            get { return generation; }
            set { generation = value; }
        }

        public int UpdateLimit
        {
            get { return updateLimit; }
            set { updateLimit = value; }
        }

        public string UpdateDesc
        {
            get { return updateDesc; }
            set { updateDesc = value; }
        }

        public int Ability
        {
            get { return ability; }
            set { ability = value; }
        }

        public string AbilityName
        {
            get { return abilityName; }
            set { abilityName = value; }
        }

        public int Unknown
        {
            get { return unknown; }
            set { unknown = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
