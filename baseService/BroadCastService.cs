﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.baseService
{
    class BroadCastService
    {

        #region 事件定义
        public class ProgressHandleArgs : EventArgs
        {
            public readonly String Msg;
            public readonly DateTime Time = DateTime.Now;
            public readonly ProcessStateEnum Code;//0：成功   1：警告  2：失败
            public readonly Boolean isComplete = false;
            public readonly Object Data = null;
            public readonly String key;
            public ProgressHandleArgs(string msg, ProcessStateEnum code, Object data,String key, Boolean isEnd)
            {
                this.Msg = msg;
                this.Code = code;
                this.Data = data;
                this.isComplete = isEnd;
                this.key = key;
            }
        }
        public delegate void ProgressChangeHandle(object sender, ProgressHandleArgs e);
        public static event ProgressChangeHandle ProgressChange;
        private static void OnProgressChange(ProgressHandleArgs e)
        {
            if (ProgressChange != null)
            {
                ProgressChange(null, e);
            }
        }
        #endregion


        public static void createMessage(String msg, ProcessStateEnum code, Object data,String key, bool complete = false)
        {
            OnProgressChange(new ProgressHandleArgs(msg, code, data,key, complete));
        }
    }
}
