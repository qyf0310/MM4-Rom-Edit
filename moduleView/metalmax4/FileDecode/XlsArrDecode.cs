﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsArrDecode
    {
        public static List<XlsArr> loadData(byte[] datas, string key)
        {
            //            List<XlsNam> itemNames = DataBaseService.getData("btl_act_msg.nam") as List<XlsNam>;
            List<XlsArr> result = new List<XlsArr>();
            int len = Convert.ToInt32(key.Split('#')[0]);
            int index = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + len-1 >= datas.Length)
                {

                    XlsArr eo = new XlsArr();
                    result.Add(eo);
                    eo.SeqNo = index;
                    eo.Offset = i;
                    
                    eo.Name = "unknown";
                    while (i < datas.Length)
                    {
                        eo.Data.Add(datas[i++]);
                    }
                    eo.BlockLen = eo.Data.Count;
                    BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, eo, key);
                    break;
                }
                XlsArr o = new XlsArr();
                result.Add(o);
                o.SeqNo = index;
                o.Offset = i;
                o.BlockLen = len;
                o.Name = "unknown";
                //                var temp = itemNames.FirstOrDefault(x => x.Offset == o.NameOffAddr);
                //                o.Name = temp == null ? "unknown" : temp.Text;

                for (int j = 0; j < len; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }


                index++;


                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o, key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null, key, true);
            DataBaseService.addData(key, result);
            return result;
        }
    }
}
