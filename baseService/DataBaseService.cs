﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using MM4RomEdit.moduleView.metalmax4.model;
using SharpConfig;

namespace MM4RomEdit.moduleView.metalmax4
{
    class DataBaseService
    {
        private static Dictionary<String, Object> dataDict = new Dictionary<string, object>();

        private static String romPath = "";
        private static String filedebugPath = "";
        private static Configuration offsetConfiguration=null;
        private static Configuration animeNamConfiguration = null;
        private static Configuration filedebugConfiguration = null;
        private static readonly List<AnimeNam> animeNamList=new List<AnimeNam>();
        //        static Dictionary<String, Object> dataDictRom = new Dictionary<string, object>();
        //        static Dictionary<String, Object> dataDictDump = new Dictionary<string, object>();

        public static void setOffsetConfigFilePath(String path)
        {
            offsetConfiguration = Configuration.LoadFromFile(path);
        }
        public static void setFiledebugConfiguration(String path)
        {
            filedebugPath = path;
            
        }
        public static void setAnimeNamConfigFilePath(String path)
        {
            animeNamConfiguration = Configuration.LoadFromFile(path);
            animeNamList.Clear();
            Section section = animeNamConfiguration["human"];
            for (int i = 0; i < section.Count(); i++)
            {
                animeNamList.Add(new AnimeNam(Convert.ToInt32(section[i].Name),section[i].StringValue,2));
            }
            section = animeNamConfiguration["car"];
            for (int i = 0; i < section.Count(); i++)
            {
                animeNamList.Add(new AnimeNam(Convert.ToInt32(section[i].Name), section[i].StringValue, 1));
            }
        }
        public static String getRomPath()
        {
            return romPath;
        }

        public static Section getFiledebugConfiguration(String sectionName)
        {
            return Configuration.LoadFromFile(filedebugPath)[sectionName];
        }
        public static Section getOffsetConfigSection(String sectionName)
        {
            return offsetConfiguration[sectionName];
        }
        public static List<AnimeNam> getAnimeList()
        {
            return animeNamList;
        }
        public static void setRomPath(String path)
        {
            romPath = path;
//            dataDict = rom ? dataDictRom : dataDictDump;
        }
        public static void addData(String key,Object data)
        {
            if (dataDict.ContainsKey(key))
            {
                dataDict.Remove(key);
            }
            dataDict.Add(key, data);
        }

        public static Object getData(String key)
        {
            return dataDict.ContainsKey(key) ? dataDict[key] : null;
        }

        public static bool containsData(String key)
        {
            return dataDict.ContainsKey(key) && dataDict[key]!=null;
        }

        public static String getItemType(int code)
        {
            switch (code)
            {
                case 1: return "人间武器";
                case 2: return "人间防具-帽";
                case 3: return "人间防具-衣";
                case 4: return "人间防具-鞋";
                case 5: return "人间防具-手";
                case 6: return "人间防具-饰";
                case 7: return "人间道具";
                case 8: return "回复道具";
                case 9: return "攻击道具";
                case 11: return "底盘";
                case 12: return "引擎";
                case 13: return "C装置";
                case 14: return "大炮";
                case 15: return "机枪";
                case 16: return "S-E";
                case 17: return "固武大炮";
                case 18: return "固武机枪";
                case 19: return "固武S-E";
                case 20: return "车辆道具";
                case 21: return "车辆装置";
                case 22: return "车辆饰品";
                case 23: return "防护膜";
                case 24: return "铁";
                case 25: return "铁";
                case 26: return "蘑菇,粪";
                case 30: return "家具";
                case 31: return "酒食";
                default: return "unknown";
            }
        }
        public static String getEngineBonusName(int code)
        {
            switch (code)
            {
                case 0: return "普通";
                case 1: return "轮胎奖励";
                case 2: return "战车奖励";
                case 3: return "汽车奖励";
                case 4: return "摩托奖励";
                case 5: return "单引擎奖励";
                case 6: return "无双奖励";
                case 7: return "副引擎奖励";
                case 8: return "双子奖励";
                case 9: return "主引擎奖励";
                case 10: return "重底盘奖励";
                case 11: return "生化奖励";
                case 12: return "辅助增压奖励";
                case 13: return "四神奖励";
                case 14: return "特殊双子奖励";
                case 15: return "破坏不能";
                default: return "unknown";
            }
        }
        public static String getWeponAttkRange(int code)
        {
            switch (code)
            {
                case 0: return "1体";
                case 1: return "敌2回";
                case 2: return "敌3回";
                case 3: return "敌4回";
                case 4: return "敌8回";
                case 5: return "敌12回";
                case 6: return "2连射";
                case 7: return "3连射";
                case 8: return "4连射";
                case 9: return "5连射";
                case 10: return "6连射";
                case 11: return "扇范围小1";
                case 12: return "扇范围大1";
                case 13: return "扇范围可变";
                case 14: return "扇范围小2";
                case 15: return "扇范围大2";
                case 16: return "扇范围,3连射";
                case 17: return "圆范围小";
                case 18: return "圆范围大";
                case 19: return "圆范围可变";
                case 20: return "圆范围特大";
                case 21: return "圆范围特大2回";
                case 22: return "圆范围,3体";
                case 23: return "圆范围,4体";
                case 24: return "圆范围,5体";
                case 25: return "圆范围,6体";
                case 26: return "圆范围,7体";
                case 27: return "圆范围,8体";
                case 28: return "圆范围,10体";
                case 29: return "貫通小";
                case 30: return "貫通大";
                case 31: return "貫通可变";
                case 32: return "貫通2回";
                case 33: return "全体1";
                case 34: return "全体2";
                case 35: return "全体3";
                case 36: return "全体2回";
                case 37: return "全体3回";
                case 38: return "全体4回";
                case 39: return "敌全体";
                case 40: return "迎击";
                case 41: return "车本体";
                case 42: return "我方全体";
                case 43: return "我方1体";
                case 44: return "我方1体（自身）";
                case 45: return "我方1体（他人）";
                case 46: return "我方全体";
                case 47: return "自身以外";
                case 48: return "自身以外1体";
                case 49: return "接する者";
                case 50: return "全体（内）";
                case 51: return "敌全体随机";
                case 52: return "内（自）";
                case 53: return "反射光束用";
                case 54: return "我方全（系统XX）";//55以后为单体，与0类似
                default: return "unknown（默认单体）";
            }
        }
        public static String getTankAbility(int code)
        {
            switch (code)
            {
                case 0: return "";
                case 1: return "闪避行驶";
                case 2: return "闪避疾驰";
                case 3: return "耐热规格";
                case 4: return "耐冷规格";
                case 5: return "防音规格";
                case 6: return "抗光束规格";
                case 7: return "耐电规格";
                case 8: return "接地装置";
                case 9: return "特殊装甲";
                case 10: return "会心一击";
                case 11: return "二重连射";
                case 12: return "三重连射";
                case 13: return "大炮齐射";
                case 14: return "机枪齐射";
                case 15: return "SE齐射";
                case 16: return "随机齐射";
                case 17: return "持续射击";
                case 18: return "三门齐射";
                case 19: return "双重攻击";
                case 20: return "三重攻击";
                case 21: return "对空能力";
                case 22: return "迎击能力";
                case 23: return "迎击辅助";
                case 24: return "迎击回避能力";
                case 25: return "野性的愤怒";
                case 26: return "特性可变";
                case 27: return "神之怒";
                case 28: return "主炮支持";
                case 29: return "机枪支持";
                case 30: return "SE支持";
                case 31: return "引擎辅助";
                case 32: return "生物支持";
                case 33: return "会心连发";
                case 34: return "全穴连射 + 1";
                case 35: return "二门齐射";
                case 36: return "四门齐射";
                case 37: return "五门齐射";
                case 38: return "四重攻击";
                case 39: return "五重攻击";
                case 40: return "攻击加倍";
                case 41: return "会心加倍";
                case 42: return "持续射击";
                case 43: return "第六感";
                case 44: return "翘头行驶";
                case 45: return "顶棚乘坐";
                case 46: return "破坏不能";
                case 47: return "限界突破";
                case 48: return "变形能力";
                case 49: return "火箭钻头";
                default: return "unknown";
            }
        }
        public static String getChassisRouteList(int code)
        {
            switch (code)
            {
                case 0: return "--";
                case 1: return "狼-1";
                case 2: return "Ｒ狼-2";
                case 3: return "Ｒ狼Ⅷ-3";
                case 4: return "Ｓ狼-4";
                case 5: return "Ｓ狼ⅥF-5";
                case 6: return "Ｔ狼-6";
                case 7: return "Ｔ狼C-7";
                case 8: return "Ｔ狼W-8";
                case 9: return "迪玛尔-9";
                case 10: return "迪玛尔-10";
                case 11: return "迪玛尔三月兔子-11";
                case 12: return "迪玛尔飞翼-12";
                case 13: return "超级跑车-13";
                case 14: return "超级跑车泽菲罗斯-14";
                case 15: return "超级跑车伽维因-15";
                case 16: return "超级跑车马凯-16";
                case 17: return "芝巴鲛,芝巴塔,芝巴Ｓ,芝巴基";
                case 18: return "芝巴塔-18";
                case 19: return "芝巴Ｓ-19";
                case 20: return "芝巴基-20";
                case 21: return "１０式-21";
                case 22: return "１０式三-22";
                case 23: return "１０式双改-23";
                case 24: return "１０式能改-猛犸象-24";
                case 25: return "１０式特改-25";
                case 26: return "１０式四-26";
                case 27: return "１０式陆巡-27";
                case 28: return "１０式不沉-28";
                case 29: return "麻雀猎人-29";
                case 30: return "闪电猎人-30";
                case 31: return "柯盖尔猎人-31";
                case 32: return "特隆贝猎人-32";
                case 33: return "卡诺奈猎人-33";
                case 34: return "人马座-34";
                case 35: return "人马座Ｌ-35";
                case 36: return "人马座Ａ-36";
                case 37: return "人马座Ｆ-37";
                case 38: return "塞洛斯-38";
                case 39: return "希尔巴诺斯-39";
                case 40: return "提本-40";
                case 41: return "白摩托-41";
                case 42: return "铁山车-42";
                case 43: return "摩托赛车-43";
                case 44: return "摩托赛车亚瑟-44";
                case 45: return "摩托赛车地狱-45";
                case 46: return "摩托赛车４-46";
                case 47: return "风暴亚瑟-47";
                case 48: return "暗黑骑士-48";
                case 49: return "欧凯瑙斯-49";
                case 50: return "鸣动欧凯瑙斯-50";
                case 51: return "结晶欧凯瑙斯-51";
                case 52: return "红莲欧凯瑙斯-52";
                case 53: return "锹形虫卡姆依-53";
                case 54: return "野巴士-54";
                case 55: return "猎豹-55";
                case 56: return "流星-56";
                case 57: return "光速流星-57";
                case 58: return "爆速流星-58";
                case 59: return "柯爱莎-59";
                case 60: return "拉斯普金-60";
                case 61: return "索伊亚漫步者-61";
                case 62: return "伊萨漫步者-62";
                case 63: return "何伊萨漫步者-63";
                case 64: return "哇塞漫步者-64";
                case 65: return "索伊索伊漫步者-65";
                case 66: return "月伊莎漫步者-66";
                case 67: return "魁漫步者-67";
                case 68: return "祭漫步者-68";
                case 69: return "天漫步者-69";
                case 70: return "祗园漫步者-70";
                case 71: return "富士漫步者-71";
                case 72: return "东京漫步者-72";
                case 73: return "水星漫步者-73";
                case 74: return "赤裸萨莎-74";
                case 75: return "X艾布拉姆斯-75";
                case 76: return "西布拉姆斯-76";
                case 77: return "Ｓ１比布拉姆斯-77";
                case 78: return "Ｓ２艾布拉姆斯-78";
                case 79: return "艾布拉姆斯-79";
                case 80: return "艾布拉姆斯-80";
                case 81: return "Ｋ１迪布拉姆斯-81";
                case 82: return "Ｋ２法伊布拉姆斯-82";
                case 83: return "虎式Ⅱ-83";
                case 84: return "格瓦拉-84";
                case 85: return "格瓦拉ＳＳＳ-85";
                case 86: return "格瓦拉ＣＳ-86";
                case 87: return "格瓦拉ＡＸ-87";
                case 88: return "格瓦拉ＳＸ-88";
                case 89: return "格瓦拉ＣＸ-89";
                case 90: return "格瓦拉ＡＳ-90";
                case 91: return "蚊式-91";
                case 92: return "Ⅱ号蚊式-92";
                case 93: return "蚊式Ａ-93";
                case 94: return "蚊式Ｆ-94";
                case 95: return "Ⅳ号蚊式-95";
                case 96: return "蚊式Ａ４-96";
                case 97: return "蚊式Ｆ４-97";
                case 98: return "科里昂-98";
                case 99: return "科里昂ＧＴ-99";
                case 100: return "科里昂ＣＳ２-100";
                case 101: return "科里昂Ｃ３-101";
                case 102: return "科里昂１２２-102";
                default: return "unknown";
            }
        }
        public static void getChassisRouteMap()
        {

        } 
        public static String getChassisSeriesName(int code)
        {
            switch (code)
            {
                case 0: return "萨莎-0";
                case 1: return "萨莎-1";
                case 2: return "迪玛尔-2";
                case 3: return "迪玛尔-3";
                case 4: return "迪玛尔-4";
                case 5: return "摩托赛车-5";
                case 6: return "摩托赛车-6";
                case 7: return "摩托赛车-7";
                case 8: return "摩托赛车-8";
                case 9: return "摩托赛车-9";
                case 10: return "超级跑车-10";
                case 11: return "超级跑车-11";
                case 12: return "超级跑车-12";
                case 13: return "超级跑车-13";
                case 14: return "科里昂-14";
                case 15: return "科里昂-15";
                case 16: return "科里昂-16";
                case 17: return "科里昂-17";
                case 18: return "芝巴-18";
                case 19: return "芝巴-19";
                case 20: return "猎人-20";
                case 21: return "猎人-21";
                case 22: return "猎人-22";
                case 23: return "猎人-23";
                case 24: return "生物1-猛犸象-24";
                case 25: return "生物2-甲虫-25";
                case 26: return "生物2-甲虫-26";
                case 27: return "生物2-甲虫-27";
                case 28: return "生物2-甲虫-28";
                case 29: return "生物3-章鱼-29";
                case 30: return "生物3-章鱼-30";
                case 31: return "生物4-恐龙-31";
                case 32: return "10式-32";
                case 33: return "10式-33";
                case 34: return "10式-34";
                case 35: return "10式-35";
                case 36: return "R狼-36";
                case 37: return "R狼-37";
                case 38: return "究级R狼-38";
                case 39: return "S狼-39";
                case 40: return "T狼-40";
                case 41: return "白摩托-41";
                case 43: return "白摩托-43";
                case 44: return "人马座-44";
                case 45: return "人马座-45";
                case 46: return "铁山车-46";
                case 47: return "野巴士-47";
                case 48: return "野巴士-48";
                case 49: return "野巴士-49";
                case 50: return "猎豹-50";
                case 51: return "塞法伊-51";
                case 52: return "塞法伊-52";
                case 53: return "塞法伊-53";
                case 54: return "漫步者-54";
                case 55: return "漫步者-55";
                case 56: return "艾布拉姆斯-56";
                case 57: return "艾布拉姆斯-57";
                case 58: return "艾布拉姆斯-58";
                case 59: return "艾布拉姆斯-59";
                case 60: return "虎式-60";
                case 61: return "虎式-61";
                case 62: return "格瓦拉-62";
                case 63: return "格瓦拉-63";
                case 64: return "格瓦拉-64";
                case 65: return "格瓦拉-65";
                case 66: return "蚊式-66";
                case 67: return "预置-67";
                case 68: return "预置-68";
                case 69: return "预置-69";
                case 70: return "预置-70";
                default: return "unknown";
            }
        }
        public static String getChassisFixedWeapone(int code)
        {
            switch (code)
            {
                case 149: return "天使之翼";
                case 150: return "恶魔之翼";
                case 151: return "奥菲尔马尔斯";
                case 152: return "前置式火神炮";
                case 153: return "前置式火神炮Ⅱ";
                case 154: return "前部导弹";
                case 155: return "前部导弹Ⅱ";
                case 156: return "天使之翼";
                case 157: return "恶魔之翼";
                case 158: return "奥菲尔马尔斯";
                case 159: return "氧元素导弹";
                case 160: return "恶魔导弹";
                case 161: return "牙狼导弹";
                case 162: return "恶魔之翼";
                case 163: return "奥菲尔马尔斯";
                case 164: return "恶魔导弹";
                case 165: return "牙狼导弹";
                case 166: return "饿狼火花炮";
                case 167: return "饿狼火花炮Ⅱ";
                case 168: return "饿狼火花炮Ⅲ";
                case 169: return "二重加农炮";
                case 170: return "二重加农炮Ⅱ";
                case 171: return "二重加农炮Ⅲ";
                case 172: return "天使之翼";
                case 173: return "恶魔之翼";
                case 174: return "奥菲尔马尔斯";
                case 175: return "托塔尔克莱夫";
                case 176: return "氧元素导弹";
                case 177: return "恶魔导弹";
                case 178: return "牙狼导弹";
                case 179: return "42mm对空炮";
                case 180: return "天箭";
                case 181: return "4连机关炮";
                case 182: return "二重拉斯特";
                case 183: return "决胜拉斯特";
                case 184: return "对空导弹炮";
                case 185: return "多弹头巴尔蒙克";
                case 186: return "钻头拉斯特";
                case 187: return "钻头拉斯特Ⅱ";
                case 188: return "对空速射炮";
                case 189: return "对空速射炮Ⅱ";
                case 190: return "超电磁拉斯特";
                case 191: return "超电磁拉斯特Ⅱ";
                case 192: return "对空加农炮";
                case 193: return "对空加农炮Ⅱ";
                case 194: return "龙卷钻头";
                case 195: return "龙卷钻头Ⅱ";
                case 196: return "拳击家手臂";
                case 197: return "泰森手臂";
                case 198: return "恶魔手臂";
                case 199: return "前置式火神炮";
                case 200: return "前置式火神炮Ⅱ";
                case 201: return "前部导弹";
                case 202: return "前部导弹Ⅱ";
                case 203: return "亿万钻头";
                case 204: return "亿兆钻头";
                case 205: return "泰森手臂";
                case 206: return "恶魔手臂";
                case 207: return "微型火神炮";
                case 208: return "微型火神炮Ⅱ";
                case 209: return "微型火神炮Ⅲ";
                case 210: return "激光竞速器";
                case 211: return "激光竞速器Ⅱ";
                case 212: return "激光竞速器Ⅱ";
                case 213: return "骑手导弹";
                case 214: return "骑手导弹Ⅱ";
                case 215: return "骑手之翼";
                case 216: return "微型火神炮Ⅱ";
                case 217: return "微型火神炮Ⅲ";
                case 218: return "激光竞速器Ⅱ";
                case 219: return "激光赛车手";
                case 220: return "骑手激光";
                case 221: return "骑手导弹Ⅱ";
                case 222: return "钛合金巨剑";
                case 223: return "骑手激光";
                case 224: return "骑手导弹Ⅱ";
                case 225: return "L门式火神炮";
                case 226: return "L门式火神炮Ⅱ";
                case 227: return "大门导弹舱";
                case 228: return "大门导弹舱Ⅱ";
                case 229: return "R门式火神炮";
                case 230: return "R门式火神炮Ⅱ";
                case 231: return "大门导弹舱";
                case 232: return "大门导弹舱Ⅱ";
                case 233: return "W大门导弹舱";
                case 234: return "W大门导弹舱Ⅱ";
                case 235: return "钛合金钻头";
                case 236: return "锆石钻头";
                case 237: return "外卖车机关枪";
                case 238: return "外卖车机关枪Ⅱ";
                case 239: return "外卖车机关枪Ⅲ";
                case 240: return "外卖车机关枪Ⅳ";
                case 241: return "百万钻头";
                case 242: return "亿万钻头";
                case 243: return "亿兆钻头";
                case 244: return "迪玛尔火神炮";
                case 245: return "迪玛尔火神炮Ⅱ";
                case 246: return "迪玛尔火神炮Ⅲ";
                case 247: return "迪玛尔导弹舱";
                case 248: return "迪玛尔导弹舱Ⅱ";
                case 249: return "迪玛尔导弹舱Ⅲ";
                case 250: return "骑手火神炮";
                case 251: return "骑手火神炮Ⅱ";
                case 252: return "骑手火神炮Ⅲ";
                case 253: return "骑手导弹";
                case 254: return "骑手导弹Ⅱ";
                case 255: return "鲨鱼导弹舱";
                case 256: return "鲨鱼导弹舱Ⅱ";
                case 257: return "小型火神炮";
                case 258: return "小型火神炮Ⅱ";
                case 259: return "狱炎炮";
                case 260: return "狱炎炮Ⅱ";
                case 261: return "狱炎炮Ⅲ";
                case 262: return "鲨鱼导弹舱";
                case 263: return "鲨鱼导弹舱Ⅱ";
                case 264: return "狱炎炮";
                case 265: return "狱炎炮Ⅱ";
                case 266: return "狱炎炮Ⅲ";
                case 267: return "芝巴火焰";
                case 268: return "狱炎炮";
                case 269: return "狱炎炮Ⅱ";
                case 270: return "狱炎炮Ⅲ";
                case 271: return "狱炎炮";
                case 272: return "狱炎炮Ⅱ";
                case 273: return "狱炎炮Ⅲ";
                case 274: return "狱炎炮";
                case 275: return "狱炎炮Ⅱ";
                case 276: return "狱炎炮Ⅲ";
                case 277: return "百万钻头";
                case 278: return "亿万钻头";
                case 279: return "亿兆钻头";
                case 280: return "尖刺保险杠";
                case 281: return "金属重剑";
                case 282: return "钛合金钻头";
                case 283: return "锆石钻头";
                case 284: return "第1大和炮";
                case 285: return "第2大和炮";
                case 286: return "第1武藏炮";
                case 287: return "第2武藏炮";
                case 288: return "钛合金钻头";
                case 289: return "锆石钻头";
                case 290: return "钛合金钻头";
                case 291: return "锆石钻头";
                case 292: return "二重加农炮Ⅳ";
                case 293: return "饿狼火神炮";
                case 294: return "饿狼包装袋";
                case 295: return "42mm对空炮W";
                case 296: return "天箭W";
                case 297: return "4连机关枪W";
                case 298: return "2重拉斯特W";
                case 299: return "饿狼拉斯特";
                case 300: return "对空导弹炮W";
                case 301: return "多弹头巴尔蒙克W";
                case 302: return "钻头拉斯特W";
                case 303: return "钻头拉斯特WⅡ";
                case 304: return "2重反转钻头";
                case 305: return "2重反转钻头Ⅱ";
                case 306: return "双管彼得";
                case 307: return "42mm对空炮W";
                case 308: return "天箭";
                case 309: return "4连机关炮";
                case 310: return "二重拉斯特";
                case 311: return "决胜拉斯特";
                case 312: return "对空导弹炮";
                case 313: return "多弹头巴尔蒙克";
                case 314: return "钻头拉斯特";
                case 315: return "钻头拉斯特Ⅱ";
                case 316: return "导弹塔";
                case 317: return "导弹塔Ⅱ";
                case 318: return "塔楼导弹舱";
                case 319: return "塔楼导弹舱Ⅱ";
                case 320: return "塔楼加农炮";
                case 321: return "塔楼加农炮Ⅱ";
                case 322: return "塔式火神炮";
                case 323: return "塔式火神炮Ⅱ";
                case 324: return "双子加农炮";
                case 325: return "双子加农炮Ⅱ";
                case 326: return "双子钻头";
                case 327: return "双子钻头Ⅱ";
                case 328: return "大型火神炮";
                case 329: return "大型火神炮Ⅱ";
                case 330: return "狱炎炮Ⅱ";
                case 331: return "狱炎炮Ⅲ";
                case 332: return "大型弓弩 ";
                case 333: return "大型弓弩Ⅱ";
                case 334: return "火箭火焰";
                case 335: return "螺旋银角";
                case 336: return "超螺旋银角";
                case 337: return "螺旋银角";
                case 338: return "超螺旋银角";
                case 339: return "花车钻头";
                case 340: return "花车钻头Ⅱ";
                case 341: return "花车激光";
                case 342: return "花车激光Ⅱ";
                case 343: return "花车加农炮";
                case 344: return "花车加农炮Ⅱ";
                case 345: return "钛合金钻头";
                case 346: return "锆石钻头";
                case 347: return "亿万钻头";
                case 348: return "亿兆钻头";
                case 349: return "花车火箭";
                case 350: return "花车火箭Ⅱ";
                case 351: return "花车火神炮";
                case 352: return "花车火神炮Ⅱ";
                case 353: return "犬火祭";
                case 354: return "犬大火祭";
                case 355: return "犬激光祭典";
                case 356: return "犬大激光祭典";
                case 357: return "犬电击祭";
                case 358: return "犬大电击祭";
                case 359: return "犬火神炮";
                case 360: return "犬火神炮Ⅱ";
                case 361: return "祭典导弹";
                case 362: return "大祭典导弹";
                case 363: return "赛车钻头";
                case 364: return "赛车钻头Ⅱ";
                case 365: return "微型火神炮";
                case 366: return "微型火神炮Ⅱ";
                case 367: return "微型火神炮Ⅲ";
                case 368: return "激光竞速器";
                case 369: return "激光竞速器Ⅱ";
                case 370: return "激光赛车手";
                case 371: return "火箭之路";
                case 372: return "骑手火神炮";
                case 373: return "骑手火神炮Ⅱ";
                case 374: return "骑手火神炮Ⅲ";
                case 375: return "车轮导弹";
                case 376: return "车轮导弹Ⅱ";
                case 377: return "车轮导弹Ⅲ";
                case 378: return "公路爆弹魔";
                case 379: return "公路爆弹魔Ⅱ";
                case 380: return "公路明星";
                case 381: return "公路明星Ⅱ";
                case 382: return "迎击光速尾翼";
                case 383: return "双管机枪";
                case 384: return "双管火神炮";
                case 385: return "双管火神炮A";
                case 386: return "尖刺保险杠";
                case 387: return "金属重剑";
                case 388: return "亿兆钻头";
                case 389: return "火箭罗德";
                case 390: return "萨沙加农炮";
                case 391: return "萨沙加农炮Ⅱ";
                case 392: return "萨沙加农炮Ⅲ";
                case 393: return "萨沙加农炮";
                case 394: return "萨沙加农炮Ⅱ";
                case 395: return "萨沙加农炮Ⅲ";
                case 396: return "萨沙加农炮";
                case 397: return "萨沙加农炮Ⅱ";
                case 398: return "萨沙加农炮Ⅲ";
                case 399: return "萨沙加农炮";
                case 400: return "萨沙加农炮Ⅱ";
                case 401: return "萨沙加农炮Ⅲ";
                case 402: return "萨沙加农炮";
                case 403: return "萨沙加农炮Ⅱ";
                case 404: return "萨沙加农炮Ⅲ";
                case 405: return "天使萨沙";
                case 406: return "天使萨沙Ⅱ";
                case 407: return "天使萨沙Ⅲ";
                case 408: return "天使萨沙";
                case 409: return "天使萨沙Ⅱ";
                case 410: return "天使萨沙Ⅲ";
                case 411: return "天使萨沙";
                case 412: return "天使萨沙Ⅱ";
                case 413: return "天使萨沙Ⅲ";
                case 414: return "天使萨沙";
                case 415: return "天使萨沙Ⅱ";
                case 416: return "天使萨沙Ⅲ";
                case 417: return "天使萨沙";
                case 418: return "天使萨沙Ⅱ";
                case 419: return "天使萨沙Ⅲ";
                case 420: return "萨沙火神炮";
                case 421: return "萨沙火神炮Ⅱ";
                case 422: return "萨沙火神炮Ⅲ";
                case 423: return "萨沙火神炮";
                case 424: return "萨沙火神炮Ⅱ";
                case 425: return "萨沙火神炮Ⅲ";
                case 426: return "萨沙火神炮";
                case 427: return "萨沙火神炮Ⅱ";
                case 428: return "萨沙火神炮Ⅲ";
                case 429: return "萨沙火神炮";
                case 430: return "萨沙火神炮Ⅱ";
                case 431: return "萨沙火神炮Ⅲ";
                case 432: return "萨沙火神炮";
                case 433: return "萨沙火神炮Ⅱ";
                case 434: return "萨沙火神炮Ⅲ";
                case 435: return "萨沙导弹发射舱";
                case 436: return "萨沙导弹发射舱Ⅱ";
                case 437: return "萨沙导弹发射舱Ⅲ";
                case 438: return "萨沙导弹发射舱";
                case 439: return "萨沙导弹发射舱Ⅱ";
                case 440: return "萨沙导弹发射舱Ⅲ";
                case 441: return "萨沙导弹发射舱";
                case 442: return "萨沙导弹发射舱Ⅱ";
                case 443: return "萨沙导弹发射舱Ⅲ";
                case 444: return "萨沙导弹发射舱";
                case 445: return "萨沙导弹发射舱Ⅱ";
                case 446: return "萨沙导弹发射舱Ⅲ";
                case 447: return "萨沙导弹发射舱";
                case 448: return "萨沙导弹发射舱Ⅱ";
                case 449: return "萨沙导弹发射舱Ⅲ";
                case 450: return "萨沙光束";
                case 451: return "萨沙光束Ⅱ";
                case 452: return "萨沙光束Ⅲ";
                case 453: return "萨沙电击";
                case 454: return "萨沙电击Ⅱ";
                case 455: return "萨沙音波";
                case 456: return "萨沙闪电";
                case 457: return "萨沙钻头";
                case 458: return "萨沙导弹";
                case 459: return "萨沙导弹Ⅱ";
                case 460: return "萨沙导弹Ⅲ";
                case 461: return "萨沙导弹Ⅳ";
                case 462: return "萨沙凝固汽油弹";
                case 463: return "萨沙铁球";
                case 464: return "萨沙大蛇导弹";
                case 465: return "萨沙寒冰";
                case 466: return "萨沙钻孔器";
                case 467: return "萨沙猎犬导弹";
                case 468: return "萨沙保护罩";
                case 469: return "生化飞毛腿";
                case 470: return "生化飞毛腿Ⅱ";
                case 471: return "生化加农炮";
                case 472: return "生化加农炮";
                case 473: return "生化加农炮";
                case 474: return "生化加农炮";
                case 475: return "生化加农炮";
                case 476: return "生化加农炮";
                case 477: return "生化加农炮";
                case 478: return "生化加农炮";
                case 479: return "生化加农炮";
                case 480: return "生化加农炮";
                case 481: return "生化加农炮";
                case 482: return "生化加农炮";
                case 483: return "生化加农炮";
                case 484: return "生化加农炮";
                case 485: return "生化加农炮";
                case 486: return "生化加农炮";
                case 487: return "生化加农炮";
                case 488: return "生化火神炮";
                case 489: return "生化火神炮";
                case 490: return "生化火神炮";
                case 491: return "生化火神炮";
                case 492: return "生化火神炮";
                case 493: return "生化火神炮";
                case 494: return "生化火神炮";
                case 495: return "生化火神炮";
                case 496: return "生化火神炮";
                case 497: return "生化火神炮";
                case 498: return "生化火神炮";
                case 499: return "生化火神炮";
                case 500: return "生化导弹舱";
                case 501: return "生化导弹舱";
                case 502: return "生化导弹舱";
                case 503: return "生化导弹舱";
                case 504: return "生化导弹舱";
                case 505: return "生化导弹舱";
                case 506: return "生化导弹舱";
                case 507: return "生化导弹舱";
                case 508: return "生化导弹舱";
                case 509: return "鼻子加农炮";
                case 510: return "鼻子加农炮Ⅱ";
                case 511: return "鼻子加农炮Ⅲ";
                case 512: return "鼻子加农炮Ⅳ";
                case 513: return "鼻子加农炮Ⅴ";
                case 514: return "鼻子加农炮Ⅵ";
                case 515: return "鼻子加农炮Ⅶ";
                case 516: return "鼻子钻头";
                case 517: return "鼻子钻头Ⅱ";
                case 518: return "鼻子钻头Ⅲ";
                case 519: return "鼻子钻头Ⅳ";
                case 520: return "鼻子钻头Ⅴ";
                case 521: return "鼻子钻头Ⅵ";
                case 522: return "鼻子钻头Ⅶ";
                case 523: return "鼻子暴风雪";
                case 524: return "鼻子暴风雪Ⅱ";
                case 525: return "鼻子暴风雪Ⅲ";
                case 526: return "鼻子暴风雪Ⅳ";
                case 527: return "鼻子暴风雪Ⅴ";
                case 528: return "鼻子暴风雪Ⅵ";
                case 529: return "鼻子暴风雪Ⅶ";
                case 530: return "化学鼻子";
                case 531: return "化学鼻子Ⅱ";
                case 532: return "化学鼻子Ⅲ";
                case 533: return "化学鼻子Ⅳ";
                case 534: return "化学鼻子Ⅴ";
                case 535: return "化学鼻子Ⅵ";
                case 536: return "化学鼻子Ⅶ";
                case 537: return "猛犸象激光";
                case 538: return "猛犸象侧翼";
                case 539: return "猛犸象侧翼Ⅱ";
                case 540: return "猛犸象侧翼Ⅲ";
                case 541: return "猛犸象侧翼Ⅳ";
                case 542: return "猛犸象冲击";
                case 543: return "毒章鱼炮";
                case 544: return "毒章鱼炮Ⅱ";
                case 545: return "毒章鱼炮Ⅲ";
                case 546: return "毒章鱼炮Ⅳ";
                case 547: return "毒章鱼炮Ⅴ";
                case 548: return "毒章鱼炮Ⅵ";
                case 549: return "毒章鱼炮Ⅶ";
                case 550: return "噪音喷射器";
                case 551: return "噪音喷射器Ⅱ";
                case 552: return "噪音喷射器Ⅲ";
                case 553: return "噪音喷射器Ⅳ";
                case 554: return "噪音喷射器Ⅴ";
                case 555: return "噪音喷射器Ⅵ";
                case 556: return "噪音喷射器Ⅶ";
                case 557: return "寒冰喷射炮";
                case 558: return "寒冰喷射炮Ⅱ";
                case 559: return "寒冰喷射炮Ⅲ";
                case 560: return "寒冰喷射炮Ⅳ";
                case 561: return "寒冰喷射炮Ⅴ";
                case 562: return "寒冰喷射炮Ⅵ";
                case 563: return "寒冰喷射炮Ⅶ";
                case 564: return "红莲喷射炮";
                case 565: return "红莲喷射炮Ⅱ";
                case 566: return "红莲喷射炮Ⅲ";
                case 567: return "红莲喷射炮Ⅳ";
                case 568: return "红莲喷射炮Ⅴ";
                case 569: return "红莲喷射炮Ⅵ";
                case 570: return "红莲喷射炮Ⅶ";
                case 571: return "章鱼之眼";
                case 572: return "生化导弹";
                case 573: return "生化导弹Ⅱ";
                case 574: return "生化导弹Ⅲ";
                case 575: return "生化导弹Ⅳ";
                case 576: return "甲虫音波";
                case 577: return "甲虫音波Ⅱ";
                case 578: return "甲虫音波Ⅲ";
                case 579: return "甲虫音波Ⅳ";
                case 580: return "甲虫音波Ⅴ";
                case 581: return "甲虫音波Ⅵ";
                case 582: return "甲虫音波Ⅶ";
                case 583: return "甲虫打击者";
                case 584: return "甲虫打击者Ⅱ";
                case 585: return "甲虫打击者Ⅲ";
                case 586: return "甲虫打击者Ⅳ";
                case 587: return "甲虫打击者Ⅴ";
                case 588: return "甲虫打击者Ⅵ";
                case 589: return "甲虫打击者Ⅶ";
                case 590: return "甲虫冲击波";
                case 591: return "甲虫冲击波Ⅱ";
                case 592: return "甲虫冲击波Ⅲ";
                case 593: return "甲虫冲击波Ⅳ";
                case 594: return "甲虫冲击波Ⅴ";
                case 595: return "甲虫冲击波Ⅵ";
                case 596: return "甲虫冲击波Ⅶ";
                case 597: return "甲虫电火花";
                case 598: return "甲虫电火花Ⅱ";
                case 599: return "甲虫电火花Ⅲ";
                case 600: return "甲虫电火花Ⅳ";
                case 601: return "甲虫电火花Ⅴ";
                case 602: return "甲虫电火花Ⅵ";
                case 603: return "甲虫电火花Ⅶ";
                case 604: return "甲虫高斯炮";
                case 605: return "甲虫高斯炮Ⅱ";
                case 606: return "甲虫高斯炮Ⅲ";
                case 607: return "甲虫高斯炮Ⅳ";
                case 608: return "甲虫高斯炮Ⅴ";
                case 609: return "甲虫高斯炮Ⅵ";
                case 610: return "甲虫管炮";
                case 611: return "灭绝加农炮";
                case 612: return "灭绝加农炮Ⅱ";
                case 613: return "灭绝加农炮Ⅲ";
                case 614: return "灭绝加农炮Ⅳ";
                case 615: return "灭绝加农炮Ⅴ";
                case 616: return "灭绝加农炮Ⅵ";
                case 617: return "灭绝加农炮Ⅶ";
                case 618: return "恐龙火焰吐息";
                case 619: return "恐龙火焰吐息Ⅱ";
                case 620: return "恐龙火焰吐息Ⅲ";
                case 621: return "恐龙火焰吐息Ⅳ";
                case 622: return "恐龙火焰吐息Ⅴ";
                case 623: return "恐龙火焰吐息Ⅵ";
                case 624: return "恐龙火焰吐息Ⅶ";
                case 625: return "恐龙激光";
                case 626: return "恐龙激光Ⅱ";
                case 627: return "恐龙激光Ⅲ";
                case 628: return "恐龙激光Ⅳ";
                case 629: return "恐龙激光Ⅴ";
                case 630: return "恐龙激光Ⅵ";
                case 631: return "恐龙激光Ⅶ";
                case 632: return "灭绝闪电";
                case 633: return "灭绝闪电Ⅱ";
                case 634: return "灭绝闪电Ⅲ";
                case 635: return "灭绝闪电Ⅳ";
                case 636: return "灭绝闪电Ⅴ";
                case 637: return "灭绝闪电Ⅵ";
                case 638: return "灭绝闪电Ⅶ";
                case 639: return "恐龙侧翼";
                case 640: return "恐龙侧翼Ⅱ";
                case 641: return "恐龙侧翼Ⅲ";
                case 642: return "恐龙侧翼Ⅳ";
                case 643: return "恐龙钢尾";
                case 644: return "超级尾巴";
                case 645: return "究极加农炮";
                case 646: return "究极加农炮Ⅱ";
                case 647: return "究极加农炮Ⅲ";
                case 648: return "究极加农炮Ⅳ";
                case 649: return "神龛发射器";
                case 650: return "神龛导弹";
                case 651: return "神龛之翼";
                case 652: return "神龛之翼Ⅱ";
                case 653: return "神龛大厅";
                case 654: return "完全宇宙炮";
                case 655: return "闪电雷霆炮";
                case 656: return "电磁热线炮";
                case 657: return "超电磁热线炮";
                case 658: return "老虎连装炮L";
                case 659: return "老虎连装炮LⅡ";
                case 660: return "老虎连装炮R";
                case 661: return "老虎连装炮RⅡ";
                case 662: return "迎击电光象牙";
                case 663: return "迎击电光象牙";
                case 664: return "迎击电光象牙";
                case 665: return "短发状导弹";
                case 666: return "赛车钻头";
                case 667: return "赛车钻头Ⅱ";
                case 668: return "大型火神炮";
                case 669: return "大型火神炮Ⅱ";
                case 670: return "狱炎炮Ⅱ";
                case 671: return "狱炎炮Ⅲ";
                case 672: return "钛合金钻头";
                case 673: return "锆石钻头";
                case 674: return "大型火神炮";
                case 675: return "大型火神炮Ⅱ";
                case 676: return "狱炎炮Ⅱ";
                case 677: return "狱炎炮Ⅲ";
                case 678: return "钛合金钻头";
                case 679: return "锆石钻头";
                case 680: return "大型火神炮";
                case 681: return "大型火神炮Ⅱ";
                case 682: return "狱炎炮Ⅱ";
                case 683: return "狱炎炮Ⅲ";
                case 684: return "钛合金钻头";
                case 685: return "锆石钻头";
                case 686: return "大型火神炮";
                case 687: return "大型火神炮Ⅱ";
                case 688: return "狱炎炮Ⅱ";
                case 689: return "狱炎炮Ⅲ";
                case 690: return "钛合金钻头";
                case 691: return "锆石钻头";
                case 692: return "大型火神炮";
                case 693: return "大型火神炮Ⅱ";
                case 694: return "狱炎炮Ⅱ";
                case 695: return "狱炎炮Ⅲ";
                case 696: return "钛合金钻头";
                case 697: return "锆石钻头";
                case 698: return "大炮类型";
                case 699: return "基德拉侧翼";
                case 700: return "基德拉侧翼Ⅱ";
                case 701: return "基德拉侧翼Ⅲ";
                case 702: return "基德拉侧翼Ⅳ";
                default: return "unknown";
            }
        }
    }
}
