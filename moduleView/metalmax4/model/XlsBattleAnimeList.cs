﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsBattleAnimeList : ModelBase
    {

        private int seqNo;
        private String name;
        private int nameOffAddr;
        private List<byte> data=new List<byte>();
        private long offset = 0;

        public override string getText()
        {
            return name;
        }

        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int NameOffAddr
        {
            get { return nameOffAddr; }
            set { nameOffAddr = value; }
        }
        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
