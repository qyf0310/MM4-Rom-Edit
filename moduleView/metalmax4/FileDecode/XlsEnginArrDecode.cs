﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsEnginArrDecode 
    {
        public static List<XlsEngine> loadData(byte[] datas, string key)
        {

            List<XlsItemList> itemNames = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            List<XlsEngine> result = new List<XlsEngine>();

            int index = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + 11 >= datas.Length)
                {
                    break;
                }
                XlsEngine o = new XlsEngine();
                result.Add(o);
                o.SeqNo = index;
                o.Offset = i;
                o.RemodelCost = datas[i + 1] * 256 + datas[i];
                o.Weight = datas[i + 3] * 256 + datas[i + 2];
                o.InitDefen = datas[i + 4];
                o.MaxDefen = datas[i + 5];
                o.LoadPower = datas[i + 7] * 256 + datas[i + 6];


                o.Name = "";
                int starGet = 0;
                if (itemNames != null)
                {
                    List<XlsItemList> temp = itemNames.Where(x => x.TypeId == 12).OrderBy(x => x.Offset).ToList();
                    if (temp.Any())
                    {
                        o.Name = temp[index].Name;
                        if (index + 1 < temp.Count)
                        {
                            try
                            {
                                starGet = Convert.ToInt32(temp[index + 1].StarGet);
                            }
                            catch (Exception e)
                            {
                                ;
                            }
                        }

                    }
                }

                o.UpdateLimit = datas[i + 8];
                switch (o.UpdateLimit)
                {
                    case 0: o.UpdateDesc = "不可升级"; break;
                    case 1: o.UpdateDesc = "可升级"; break;
                    case 2: o.UpdateDesc = String.Format("{0}星及以上可升级", starGet); break;
                    default: o.UpdateDesc = "unknown"; break;
                }

                o.Generation = datas[i + 9];
                o.Ability = datas[i + 10];
                o.AbilityName = DataBaseService.getEngineBonusName(o.Ability);
                o.Unknown = datas[i + 11];

                for (int j = 0; j < 12; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }

                index++;


                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o,key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null,key,true);
            DataBaseService.addData(key, result);
            return result;
        }


    }
}
