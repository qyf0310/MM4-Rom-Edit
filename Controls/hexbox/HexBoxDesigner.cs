﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms.Design;//System.Design.dll
using System.ComponentModel.Design;
using System.ComponentModel;
using System.Reflection;

namespace HexBoxControl
{
    internal class HexBoxDesigner : ControlDesigner
    {
        public override DesignerVerbCollection Verbs {
            get {
                DesignerVerb[] verbs = new DesignerVerb[]{
                    new DesignerVerb("&BestWidth",new EventHandler(OnBestWidth))
                };
                return new DesignerVerbCollection(verbs);
            }
        }

        private void OnBestWidth(object sender, EventArgs e) {
            int w = (int)(typeof(HexBox).GetField("m_nCanvsWidth", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(this.Control));
            w += ((HexBox)this.Control).ScrollWidth;
            TypeDescriptor
                .GetProperties(this.Control.GetType())["Width"]
                .SetValue(this.Control, w);
        }
    }
}
