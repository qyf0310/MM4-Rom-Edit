﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class CUnitView : Form
    {
        private XlsCUnit currentItem = null;
        public CUnitView()
        {
            InitializeComponent();
            init();
        }
        public void init()
        {
            List<XlsCUnit> datas = DataBaseService.getData(ConstanDef.ArrFileKey_cunit) as List<XlsCUnit>;
            searchTree1.loadList(datas);
            for (int i = 0; i < 50; i++)
            {
                String name = DataBaseService.getTankAbility(i);
                cbbAbility1.Items.Add(name);
                cbbAbility2.Items.Add(name);
                cbbAbility3.Items.Add(name);
            }
        }
        private void searchTree1_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree)sender;
            if (control.TreeView.SelectedNode != null)
            {
                loadItem(Convert.ToInt32(control.TreeView.SelectedNode.Tag));
            }
        }
        private void loadItem(int index)
        {
            List<XlsCUnit> datas = DataBaseService.getData(ConstanDef.ArrFileKey_cunit) as List<XlsCUnit>;
            XlsCUnit o = datas[index];
            currentItem = o;
            tbName.Text = o.Name;
            tbRomAddr.Text = String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_cunit)["data_offset"].StringValue, 16) + o.Offset);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in o.Data)
            {
                sb.Append(String.Format("{0:X2} ", b));
            }
            tbRomData.Text = sb.ToString();


            ncWeight.Value = (decimal)(o.Weight*1.0/100.0);
            ncCost.Value = o.RemodelCost;
            ncDEFInit.Value = o.InitDefen;
            ncDefMax.Value = o.MaxDefen;
            ncHitInit.Value = o.InitHitRate;
            ncHitMax.Value = o.MaxHitRate;
            ncParInit.Value = o.InitParryRate;
            ncParMax.Value = o.MaxParryRate;
            ncInitAbilityNum.Value = o.InitAbilityNum;


            cbbAbility1.SelectedIndex = o.Ability1;
            cbbAbility2.SelectedIndex = o.Ability2;
            cbbAbility3.SelectedIndex = o.Ability3;

        }

        private void btnSaveToRom_Click(object sender, EventArgs e)
        {
            byte[] bytes = currentItem.Data.ToArray();
            
            bytes[0] = (byte)(ncCost.Value % 256);
            bytes[1] = (byte)(ncCost.Value / 256);

            bytes[2] = (byte)(ncWeight.Value*100 % 256);
            bytes[3] = (byte)(ncWeight.Value * 100 / 256);
            
            

            bytes[4] = (byte)ncDEFInit.Value;
            bytes[5] = (byte)ncDefMax.Value;
            bytes[6] = (byte)ncHitInit.Value;
            bytes[7] = (byte)ncParInit.Value;
            bytes[8] = (byte)ncHitMax.Value;
            bytes[9] = (byte)ncParMax.Value;
            bytes[10] = (byte)ncInitAbilityNum.Value;
            bytes[11] = (byte)cbbAbility1.SelectedIndex;
            bytes[12] = (byte)cbbAbility2.SelectedIndex;
            bytes[13] = (byte)cbbAbility3.SelectedIndex;
            try
            {
                byte[] newBytes = FileOperateHelper.modifyBin(DataBaseService.getRomPath(), Convert.ToInt32(tbRomAddr.Text, 16), bytes);
                XlsCUnit newO = XlsCUnitArrDecode.loadData(newBytes, "edit")[0];
                
                
                List<XlsCUnit> datas = DataBaseService.getData(ConstanDef.ArrFileKey_cunit) as List<XlsCUnit>;
                int index = Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag);
                XlsCUnit oldO = datas[index];
                newO.Name = oldO.Name;
                newO.SeqNo = oldO.SeqNo;
                newO.Offset = oldO.Offset;
                datas[index] = newO;
                loadItem(index);
                MessageBox.Show("写入完毕");
            }
            catch (Exception exception)
            {
                MessageBox.Show("写入失败," + exception.Message);
            }
        }


    }
}
