﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MM4RomEdit.moduleView.metalmax4;

namespace MM4RomEdit.baseService
{
    public class DataProcessWorker
    {
        public delegate object ProcessDataHandler(byte[] data,String key);
        public delegate object ProcessFileHandler(String filePath);

        public static void run(string key,byte[] data, ProcessDataHandler processHandler)

        {
            if (processHandler==null)
            {
                return;
            }
            Thread task = new Thread(new ThreadStart(delegate
            {
                try
                {
                    processHandler(data,key);
                    
                }
                catch (Exception ex)
                {
                    BroadCastService.createMessage("process data error," + ex.Message, ProcessStateEnum.FAILED, null, key, true);
                }
            }));
            task.Start();
        }

        public static void run(String filePath,ProcessFileHandler processHandler)

        {
            if (processHandler == null)
            {
                return;
            }
            Thread task = new Thread(new ThreadStart(delegate
            {
                try
                {
                    processHandler(filePath);
                }
                catch (Exception ex)
                {
                    BroadCastService.createMessage("process file error," + ex.Message, ProcessStateEnum.FAILED, null, filePath, true);
                }
            }));
            task.Start();
        }
    }
}
