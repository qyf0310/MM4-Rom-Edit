﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsItemArrDecode 
    {
        public static List<XlsItemList> loadData(byte[] datas, string key)
        {
            List<XlsNam> itemNames = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist+ ".nam") as List<XlsNam>;
            List<XlsItemList> result = new List<XlsItemList>();

            int curLength = 0;
            int offset = 0;
            int index = 0;
            for (int i = 10; i < datas.Length;)
            {
                if (i + 11 >= datas.Length)
                {
                    break;
                }
                XlsItemList o = new XlsItemList();
                result.Add(o);
                o.SeqNo = index++;
                o.Offset = i;
                o.DataId = datas[i + 1] * 256 + datas[i];
                o.NameOffsetAddr = datas[i + 3] * 256 + datas[i + 2];
                o.TypeId = datas[i + 5] * 256 + datas[i + 4];
                o.Cost = datas[i + 7] * 256 + datas[i + 6];
                o.IsDlc = datas[i + 11] == 1;

                switch (datas[i + 10])
                {
                    case 0xF:
                        o.StarRandom = false;
                        o.StarGet = "其它物品";
                        break;
                    case 0xE:
                        o.StarRandom = false;
                        o.StarGet = "宇宙玩偶机能";
                        break;
                    case 0xD:
                        o.StarRandom = false;
                        o.StarGet = "剧情物品，周目不继承";
                        break;
                    default:
                        o.StarRandom = datas[i + 10] >= 8;
                        if (o.StarRandom)
                        {
                            int maxStar = datas[i + 10] - 8;
                            o.StarGet = "0";
                            StringBuilder sbStar = new StringBuilder("0");
                            for (int j = 1; j <= maxStar; j++)
                            {
                                sbStar.Append(",").Append(j.ToString());
                            }
                            o.StarGet = sbStar.ToString();
                        }
                        else
                        {
                            o.StarGet = datas[i + 10].ToString();
                        }

                        break;
                }
                var temp = itemNames.FirstOrDefault(x => x.Offset == o.NameOffsetAddr);
                o.Name = temp == null ? "unknown" : temp.Text;
                for (int j = 0; j < 12; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }
                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o,key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null, key,true);
            DataBaseService.addData(key, result);
            return result;
        }

    }
}
