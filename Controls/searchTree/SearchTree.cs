﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.Controls
{
    public partial class SearchTree : UserControl
    {


        private TreeNode selectedNode = null;


        public TextBox TxtSearch => txtSearch;

        public TreeView TreeView => treeView;


        public SearchTree()
        {
            InitializeComponent();
            this.treeView.DoubleClick += treeView_DoubleClick;
            this.treeView.Click += treeView_Click;
            this.treeView.HideSelection = false;
            this.treeView.DrawMode = TreeViewDrawMode.OwnerDrawText;
            this.treeView.DrawNode+= new DrawTreeNodeEventHandler(treeView_DrawNode);
        }
        private void treeView_Click(object sender, EventArgs e)
        {
            OnClick(e);
        }
        private void treeView_DoubleClick(object sender, EventArgs e)
        {
            OnDoubleClick(e);
        }

        private void treeView_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            e.DrawDefault = true; //我这里用默认颜色即可，只需要在TreeView失去焦点时选中节点仍然突显  
            return;

            if ((e.State & TreeNodeStates.Selected) != 0)
            {
                //演示为绿底白字  
                e.Graphics.FillRectangle(Brushes.DarkBlue, e.Node.Bounds);

                Font nodeFont = e.Node.NodeFont;
                if (nodeFont == null) nodeFont = ((TreeView)sender).Font;
                e.Graphics.DrawString(e.Node.Text, nodeFont, Brushes.White, Rectangle.Inflate(e.Bounds, 2, 0));
            }
            else
            {
                e.DrawDefault = true;
            }

            if ((e.State & TreeNodeStates.Focused) != 0)
            {
                using (Pen focusPen = new Pen(Color.Black))
                {
                    focusPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                    Rectangle focusBounds = e.Node.Bounds;
                    focusBounds.Size = new Size(focusBounds.Width - 1,
                        focusBounds.Height - 1);
                    e.Graphics.DrawRectangle(focusPen, focusBounds);
                }
            }
        }

        public void loadList(IList list)
        {
            treeView.Nodes.Clear();
            int index = 0;
            foreach (ModelBase modelBase in list)
            {
                TreeNode tn=new TreeNode(modelBase.getText());
                tn.Tag = (index++).ToString();
                treeView.Nodes.Add(tn);
            }
            txtSearch.Enabled = treeView.Nodes.Count > 0;
        }
        public String loadDir(String dirPath,List<String> ignoreExtList)
        {
            treeView.Nodes.Clear();
            if (!Directory.Exists(dirPath))
            {
                return "dir not exist:" + dirPath;
            }
            
            loadNode(treeView.TopNode,new DirectoryInfo(dirPath), ignoreExtList);
            treeView.Nodes[0].Expand();
            txtSearch.Enabled = treeView.Nodes.Count > 0;
            return String.Empty;
        }

        private void loadNode(TreeNode parent,DirectoryInfo dirInfo, List<String> ignoreExtList)
        {
            TreeNode treeNode = parent==null? treeView.Nodes.Add(dirInfo.FullName, dirInfo.Name) :parent.Nodes.Add(dirInfo.FullName, dirInfo.Name);
            foreach (FileInfo fileInfo in dirInfo.GetFiles())
            {
                if (ignoreExtList.Contains(fileInfo.Extension))
                {
                    continue;
                }
                treeNode.Nodes.Add(fileInfo.FullName, fileInfo.Name);
            }
            foreach (DirectoryInfo subDir in dirInfo.GetDirectories())
            {
                loadNode(treeNode, subDir, ignoreExtList);
            }
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            var control = (TextBox)sender;
            if (treeView.Nodes.Count == 0)
            {
                control.Enabled = false;
                return;
            }
            if (control.Text .Equals("搜索当前列表.....",StringComparison.OrdinalIgnoreCase) )
            {
                return;
            }
            treeView.SelectedNode=findNodeByText(treeView.Nodes,txtSearch.Text);
        }

        TreeNode findNodeByText(TreeNodeCollection treeNodeCollection, String searchContent)
        {
            foreach (TreeNode node in treeNodeCollection)
            {
                if (node.Text.Contains(searchContent))
                    return node;
                if (node.Nodes.Count>0)
                {
                    TreeNode result = findNodeByText(node.Nodes, searchContent);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }

        private void txtSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text.Equals("搜索当前列表....."))
            {
                txtSearch.Text = "";
            }
        }
    }
}
