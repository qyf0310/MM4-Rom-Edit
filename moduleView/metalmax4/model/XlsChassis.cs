﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsChassis:ModelBase
    {
        private int seqNo;
        private String name;
        private int storage;
        private int ability;
        private String abilityName;
        private int type;
        private String typeName;
        
        private int level;
        private int hole1;
        private String hole1Remark;
        private int hole2;
        private String hole2Remark;
        private int hole3;
        private String hole3Remark;
        private int hole4;
        private String hole4Remark;
        private int hole5;
        private String hole5Remark;
        private int hole6;
        private String hole6Remark;

        private int remodel;
        private bool doubleC;
        private bool doubleE;


        private List<byte> data = new List<byte>();
        private long offset = 0;
        public override string getText()
        {
            return name;
        }
        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public bool DoubleC
        {
            get { return doubleC; }
            set { doubleC = value; }
        }

        public bool DoubleE
        {
            get { return doubleE; }
            set { doubleE = value; }
        }

        public int Storage
        {
            get { return storage; }
            set { storage = value; }
        }

        public int Ability
        {
            get { return ability; }
            set { ability = value; }
        }

        public string AbilityName
        {
            get { return abilityName; }
            set { abilityName = value; }
        }

        public int Type
        {
            get { return type; }
            set { type = value; }
        }

        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }

        public int Level
        {
            get { return level; }
            set { level = value; }
        }

        public int Hole1
        {
            get { return hole1; }
            set { hole1 = value; }
        }

        public String Hole1Remark
        {
            get { return hole1Remark; }
            set { hole1Remark = value; }
        }

        public int Hole2
        {
            get { return hole2; }
            set { hole2 = value; }
        }

        public String Hole2Remark
        {
            get { return hole2Remark; }
            set { hole2Remark = value; }
        }

        public int Hole3
        {
            get { return hole3; }
            set { hole3 = value; }
        }

        public String Hole3Remark
        {
            get { return hole3Remark; }
            set { hole3Remark = value; }
        }

        public int Hole4
        {
            get { return hole4; }
            set { hole4 = value; }
        }

        public String Hole4Remark
        {
            get { return hole4Remark; }
            set { hole4Remark = value; }
        }

        public int Hole5
        {
            get { return hole5; }
            set { hole5 = value; }
        }

        public String Hole5Remark
        {
            get { return hole5Remark; }
            set { hole5Remark = value; }
        }

        public int Hole6
        {
            get { return hole6; }
            set { hole6 = value; }
        }

        public String Hole6Remark
        {
            get { return hole6Remark; }
            set { hole6Remark = value; }
        }

        public int Remodel
        {
            get { return remodel; }
            set { remodel = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
