﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class EngineView : Form
    {
        private XlsEngine currentItem = null;
        public EngineView()
        {
            InitializeComponent();
            init();
        }
        public void init()
        {
            List<XlsEngine> datas = DataBaseService.getData(ConstanDef.ArrFileKey_engine) as List<XlsEngine>;
            searchTree1.loadList(datas);
            for (int i = 0; i < 16; i++)
            {
                String name = DataBaseService.getEngineBonusName(i);
                cbbAbility.Items.Add(name);
            }
        }
        private void searchTree1_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree)sender;
            if (control.TreeView.SelectedNode != null)
            {
                loadItem(Convert.ToInt32(control.TreeView.SelectedNode.Tag));
            }
        }
        private void loadItem(int index)
        {
            List<XlsEngine> datas = DataBaseService.getData(ConstanDef.ArrFileKey_engine) as List<XlsEngine>;
            XlsEngine o = datas[index];
            currentItem = o;
            tbName.Text = o.Name;
            tbRomAddr.Text = String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_engine)["data_offset"].StringValue, 16) + o.Offset);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in o.Data)
            {
                sb.Append(String.Format("{0:X2} ", b));
            }
            tbRomData.Text = sb.ToString();
            tbGeneration.Text = o.Generation.ToString();

            ncWeight.Value = (decimal)(o.Weight*1.0/100.0);
            ncCost.Value = o.RemodelCost;
            ncDEFInit.Value = o.InitDefen;
            ncDefMax.Value = o.MaxDefen;
            ncLoadPower.Value = (decimal)(o.LoadPower * 1.0 / 100.0);
            cbbAbility.SelectedIndex = o.Ability;
            cbbUpdate.SelectedIndex = o.UpdateLimit;
            tbUpdateDesc.Text = o.UpdateDesc;
        }

        private void btnSaveToRom_Click(object sender, EventArgs e)
        {
            byte[] bytes = currentItem.Data.ToArray();
            
            bytes[0] = (byte)(ncCost.Value % 256);
            bytes[1] = (byte)(ncCost.Value / 256);

            bytes[2] = (byte)(ncWeight.Value*100 % 256);
            bytes[3] = (byte)(ncWeight.Value * 100 / 256);
            
            

            bytes[4] = (byte)ncDEFInit.Value;
            bytes[5] = (byte)ncDefMax.Value;

            bytes[6] = (byte)(ncLoadPower.Value * 100 % 256);
            bytes[7] = (byte)(ncLoadPower.Value * 100 / 256);

            bytes[8] = (byte)cbbUpdate.SelectedIndex;

            bytes[10] = (byte) cbbAbility.SelectedIndex;

            try
            {
                byte[] newBytes = FileOperateHelper.modifyBin(DataBaseService.getRomPath(), Convert.ToInt32(tbRomAddr.Text, 16), bytes);
                XlsEngine newO = XlsEnginArrDecode.loadData(newBytes, "edit")[0];
                
                
                List<XlsEngine> datas = DataBaseService.getData(ConstanDef.ArrFileKey_engine) as List<XlsEngine>;
                int index = Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag);
                XlsEngine oldO = datas[index];
                newO.Name = oldO.Name;
                newO.SeqNo = oldO.SeqNo;
                newO.Offset = oldO.Offset;
                newO.UpdateDesc = oldO.UpdateDesc;
                datas[index] = newO;
                loadItem(index);
                MessageBox.Show("写入完毕");
            }
            catch (Exception exception)
            {
                MessageBox.Show("写入失败," + exception.Message);
            }
        }

        private void cbbUpdate_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag);
            List<XlsEngine> datas = DataBaseService.getData(ConstanDef.ArrFileKey_engine) as List<XlsEngine>;

            if (index + 1 >= datas.Count || (cbbUpdate.SelectedIndex>0 &&  currentItem.Generation > datas[index + 1].Generation))
            {
                MessageBox.Show("不存在下一世代引擎");
                cbbUpdate.SelectedIndex = 0;
            }


        }
    }
}
