﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsCarWepon:ModelBase
    {
        private int seqNo;
        private String name;
        private int remodelCost;
        private int remodelWeight;
        private int weight;
        private int initDefen;
        private int maxDefen;
        private int initAttack;
        private int maxAttack;
        private int initBombNum;
        private int maxBombNum;
//        private int bombPrice;
        private int attAttribute;
        private int attDistance;
        private String attDistanceName;
        private int attRange;
        private String attRangeName;
        private int attAnime;
        private int apperence;

        private int unknown1;
        private int unknown2;
        private int unknown3;
        private List<byte> data = new List<byte>();
        private long offset = 0;
        public override string getText()
        {
            return name;
        }
        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int RemodelCost
        {
            get { return remodelCost; }
            set { remodelCost = value; }
        }

        public int RemodelWeight
        {
            get { return remodelWeight; }
            set { remodelWeight = value; }
        }

        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public int InitDefen
        {
            get { return initDefen; }
            set { initDefen = value; }
        }

        public int MaxDefen
        {
            get { return maxDefen; }
            set { maxDefen = value; }
        }

        public int InitAttack
        {
            get { return initAttack; }
            set { initAttack = value; }
        }

        public int MaxAttack
        {
            get { return maxAttack; }
            set { maxAttack = value; }
        }

        public int InitBombNum
        {
            get { return initBombNum; }
            set { initBombNum = value; }
        }

        public int MaxBombNum
        {
            get { return maxBombNum; }
            set { maxBombNum = value; }
        }

//        public int BombPrice
//        {
//            get { return bombPrice; }
//            set { bombPrice = value; }
//        }

        public int AttAttribute
        {
            get { return attAttribute; }
            set { attAttribute = value; }
        }


//        public int AttDistance
//        {
//            get { return attDistance; }
//            set { attDistance = value; }
//        }

//        public string AttDistanceName
//        {
//            get { return attDistanceName; }
//            set { attDistanceName = value; }
//        }

        public int AttRange
        {
            get { return attRange; }
            set { attRange = value; }
        }

        public string AttRangeName
        {
            get { return attRangeName; }
            set { attRangeName = value; }
        }

        public int AttAnime
        {
            get { return attAnime; }
            set { attAnime = value; }
        }

        public int Unknown1
        {
            get { return unknown1; }
            set { unknown1 = value; }
        }

        public int Unknown2
        {
            get { return unknown2; }
            set { unknown2 = value; }
        }

        public int Unknown3
        {
            get { return unknown3; }
            set { unknown3 = value; }
        }

        public int Apperence
        {
            get { return apperence; }
            set { apperence = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
