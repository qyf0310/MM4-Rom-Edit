﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsNingenBuki:ModelBase
    {
        private int seqNo;
        private String name;

        private int male;
        private int female;
        private int animal;
        private int attack;
        private int attribute;
        private int animeId;
        private int range;
        private int unknown;

        private List<byte> data = new List<byte>();
        private long offset = 0;

        public override string getText()
        {
            return name;
        }

        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Male
        {
            get { return male; }
            set { male = value; }
        }

        public int Female
        {
            get { return female; }
            set { female = value; }
        }

        public int Animal
        {
            get { return animal; }
            set { animal = value; }
        }

        public int Attack
        {
            get { return attack; }
            set { attack = value; }
        }

        public int Attribute
        {
            get { return attribute; }
            set { attribute = value; }
        }

        public int AnimeId
        {
            get { return animeId; }
            set { animeId = value; }
        }

        public int Range
        {
            get { return range; }
            set { range = value; }
        }

        public int Unknown
        {
            get { return unknown; }
            set { unknown = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
