﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsCUnitArrDecode 
    {
        public static List<XlsCUnit> loadData(byte[] datas, string key)
        {
            List<XlsItemList> itemNames = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            List<XlsCUnit> result = new List<XlsCUnit>();
            int curLength = 0;
            int offset = 0;
            int index = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + 13 >= datas.Length)
                {
                    break;
                }
                XlsCUnit o = new XlsCUnit();
                result.Add(o);
                o.SeqNo = index;
                o.Offset = i;
                o.RemodelCost = datas[i + 1] * 256 + datas[i];
                o.Weight = datas[i + 3] * 256 + datas[i + 2];
                o.InitDefen = datas[i + 4];
                o.MaxDefen = datas[i + 5];
                o.InitHitRate = datas[i + 6];
                o.InitParryRate = datas[i + 7];
                o.MaxHitRate =datas[i + 8];
                o.MaxParryRate =datas[i + 9] ;
                o.InitAbilityNum = datas[i + 10];
                o.Ability1 = datas[i + 11];
                o.Ability2 = datas[i + 12];
                o.Ability3 = datas[i + 13];
                String ability1 = DataBaseService.getTankAbility(o.Ability1);
                String ability2 = DataBaseService.getTankAbility(o.Ability2);
                String ability3 = DataBaseService.getTankAbility(o.Ability3);
                o.AbilityName = "";
                if (o.Ability1 != 0)
                {
                    o.AbilityName = ability1
                                    + (String.IsNullOrEmpty(ability2) ? "" : ("," + ability2))
                                    + (String.IsNullOrEmpty(ability3) ? "" : ("," + ability3));
                }
                o.Name = "";
                for (int j = 0; j < 14; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }

                if (itemNames != null)
                {
                    List<XlsItemList> temp = itemNames.Where(x => x.TypeId == 0x0D).OrderBy(x => x.Offset).ToList();
                    if (temp.Any())
                    {
                        o.Name = temp[index].Name;
                    }
                }
                index++;


                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o,key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null,key,true);
            DataBaseService.addData(key, result);
            return result;
        }
    }
}
