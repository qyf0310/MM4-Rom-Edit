﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM4RomEdit.baseService
{
    public enum ProcessStateEnum
    {
        NORMAL=0,
        SUCCESS=1,
        WARNING=2,
        FAILED=3
    }
}
