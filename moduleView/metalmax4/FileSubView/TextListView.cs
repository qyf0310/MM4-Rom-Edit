﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.FileDecode;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class TextListView : Form
    {
        private DataProcessWorker.ProcessDataHandler processHandler = null;
        private String key = "";
        private byte[] data;
        private PropertyInfo[] property = null;
        public TextListView(DataProcessWorker.ProcessDataHandler processHandler, byte[] data,String key)
        {
            InitializeComponent();
            this.processHandler = processHandler;
            this.key = key;
            this.data = data;
            BroadCastService.ProgressChange += BroadCastService_ProgressChange; ;
        }

        public void ReLoadData()
        {
            DataProcessWorker.run(key,data,processHandler);
        }

        private void BroadCastService_ProgressChange(object sender, BroadCastService.ProgressHandleArgs e)
        {
            if (!e.key.Equals(key,StringComparison.OrdinalIgnoreCase))
            {
                return;
            }
            tsslInfo.Text = e.Msg;
            if (e.Data==null)
            {
                return;
            }
            if (property==null)
            {
                listView1.Columns.Clear();
                listView1.Columns.Add("序号");
                Type objType = e.Data.GetType();
                property = objType.GetProperties();
                foreach (PropertyInfo item in property)
                {
                    listView1.Columns.Add(item.Name);
                }
            }
//            listView1.BeginUpdate();
            ListViewItem listViewItem = new ListViewItem();
            listViewItem.Text = String.Format("{0}", listView1.Items.Count);
            foreach (PropertyInfo item in property)
            {
                try
                {
                    if (typeof(System.Collections.IList).IsAssignableFrom(item.PropertyType))
                    {
                        IList list = (IList)item.GetValue(e.Data, null);
                        StringBuilder sb=new StringBuilder();
                        foreach (object o in list)
                        {
                            sb.Append(String.Format("{0:X2} ", o));
                        }
                        listViewItem.SubItems.Add(sb.ToString());
                    }
                    else if(item.PropertyType==typeof(bool))
                    {
                        bool flag=(bool)item.GetValue(e.Data, null);
                        listViewItem.SubItems.Add(flag?"是":"");
                    }
                    else
                    {
                        if (item.Name.ToLower().IndexOf("offset")>=0)
                        {
                            listViewItem.SubItems.Add(String.Format("{0:X2}",item.GetValue(e.Data, null)));
                        }
                        else
                        {
                            listViewItem.SubItems.Add(item.GetValue(e.Data, null).ToString());
                        }
                        
                    }
                    
                }
                catch (Exception ex)
                {
                    listViewItem.SubItems.Add("error:"+ex.Message);
                }
                           
            }
            listView1.Items.Add(listViewItem);
            listView1.EnsureVisible(listView1.Items.Count-1);
            
        }

        private void tsmiExportCsv_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count==0)
            {
                MessageBox.Show("列表中无数据.");
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = " csv files(*.csv)|*.csv";
            String fileName =key;
//            fileName.Replace(".", "");
//            if (fileName.IndexOf(".")>0)
//            {
//                fileName = fileName.Substring(0, fileName.IndexOf("."));
//            }
            saveFileDialog.FileName = fileName;

            saveFileDialog.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                String filepath = saveFileDialog.FileName.ToString();

                StringBuilder sbTotal = new StringBuilder();
                StringBuilder sbLine = new StringBuilder();
                int columnsTotal = 0;
                foreach (ColumnHeader column in listView1.Columns)
                {
                    sbLine.Append(sbLine.Length > 0 ? "," : "").Append(column.Text);
                    columnsTotal++;
                }
                sbTotal.Append(sbLine);
                foreach (ListViewItem lviItem in listView1.Items)
                {
                    sbLine.Clear();
                    sbLine.Append(lviItem.Text);
                    for (int colIdx = 1; colIdx < columnsTotal; colIdx++)//每行中的每项
                    {
                        sbLine.Append(",").Append(lviItem.SubItems[colIdx].Text.Replace(",","，"));
                    }
                    sbTotal.Append("\r").Append(sbLine);
                }
                try
                {
                    FileOperateHelper.outputTextFile(filepath, sbTotal.ToString());
                    MessageBox.Show("导出完毕");
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
                
            }


        }
    }
}
