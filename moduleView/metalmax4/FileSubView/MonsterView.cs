﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class MonsterView : Form
    {
        private XlsMonster currentItem = null;
        public MonsterView()
        {
            InitializeComponent();
            init();
        }
        public void init()
        {
            List<XlsMonster> datas = DataBaseService.getData(ConstanDef.ArrFileKey_monster) as List<XlsMonster>;
            searchTree1.loadList(datas);

            List<XlsItemList> dropItems = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            List<XlsItemList> list1 = dropItems
                .Where(x => x.TypeId <= 16 || x.TypeId == 20 || x.TypeId == 24 || x.TypeId == 25).Where(x=>x.TypeId!=11).ToList();
            List<XlsItemList> list2 = dropItems
                .Where(x => x.TypeId <= 16 || x.TypeId == 20 || x.TypeId == 24 || x.TypeId == 25).Where(x => x.TypeId != 11).ToList();
            List<XlsItemList> list3 = dropItems
                .Where(x => x.TypeId <= 16 || x.TypeId == 20 || x.TypeId == 24 || x.TypeId == 25).Where(x => x.TypeId != 11).ToList();
            List<XlsItemList> list4 = dropItems
                .Where(x => x.TypeId <= 16 || x.TypeId == 20 || x.TypeId == 24 || x.TypeId == 25).Where(x => x.TypeId != 11).ToList();
            cbbDrop1.DataSource = list1;
            cbbDrop1.DisplayMember = "Name";
            cbbDrop1.ValueMember = "SeqNo";

            cbbDrop2.DataSource = list2;
            cbbDrop2.DisplayMember = "Name";
            cbbDrop2.ValueMember = "SeqNo";

            cbbDrop3.DataSource = list3;
            cbbDrop3.DisplayMember = "Name";
            cbbDrop3.ValueMember = "SeqNo";

            cbbDrop4.DataSource = list4;
            cbbDrop4.DisplayMember = "Name";
            cbbDrop4.ValueMember = "SeqNo";

            List<XlsBattleActData> batActions = DataBaseService.getData(ConstanDef.ArrFileKey_btl_actdata) as List<XlsBattleActData>;
            foreach (XlsBattleActData actData in batActions)
            {
                cbbAct1.Items.Add(actData.Name);
                cbbAct2.Items.Add(actData.Name);
                cbbAct3.Items.Add(actData.Name);
                cbbAct4.Items.Add(actData.Name);
                cbbAct5.Items.Add(actData.Name);
                cbbAct6.Items.Add(actData.Name);
                cbbAct7.Items.Add(actData.Name);


            }
        }

        private void searchTree1_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree)sender;
            if (control.TreeView.SelectedNode != null)
            {
                loadItem(Convert.ToInt32(control.TreeView.SelectedNode.Tag));
            }
        }
        private void loadItem(int index)
        {
            List<XlsMonster> datas = DataBaseService.getData(ConstanDef.ArrFileKey_monster) as List<XlsMonster>;
            XlsMonster o = datas[index];
            currentItem = o;
            tbName.Text = o.Name;
            tbRomAddr.Text = String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_monster)["data_offset"].StringValue, 16) + o.Offset);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in o.Data)
            {
                sb.Append(String.Format("{0:X2} ", b));
            }
            tbRomData.Text = sb.ToString();

            tbNameAddr.Text= String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_itemlist)["name_offset"].StringValue, 16) + o.NameOffsetAddr);
            tbAppearance.Text = o.Appearance.ToString();
            ncActionTimes.Value = o.ActionTimes;

            ncLev.Value = o.Level;
            ncExp.Value = o.Exp;
            ncMoney.Value = o.Gold;
            ncHP.Value = o.Hp;
            ncATK.Value = o.Attack;
            ncDEF.Value = o.Defence;
            ncSPD.Value = o.Speed;
            ncHIT.Value = o.Hit;
            ncPAR.Value = o.Parry;

            ncUnknowP1.Value = o.UnknownResist1;
            ncUnknowP2.Value = o.UnknownResist2;
            ncNormal.Value = o.NormalResist;
            ncFire.Value = o.FireResist;
            ncIce.Value = o.IceResist;
            ncElectri.Value = o.ElectriResist;
            ncSound.Value = o.SoundResist;
            ncGas.Value = o.GasResist;
            ncLaser.Value = o.LaserRsist;


            cbbDrop1.SelectedValue = o.Drop1 - 1;
            cbbDrop2.SelectedValue = o.Drop2 - 1;
            cbbDrop3.SelectedValue = o.Drop3 - 1;
            cbbDrop4.SelectedValue = o.Drop4 - 1;

            ncDrop1.Value = o.Drop1Rate;
            ncDrop2.Value = o.Drop2Rate;
            ncDrop3.Value = o.Drop3Rate;
            ncDrop4.Value = o.Drop4Rate;

            cbbAct1.SelectedIndex = o.Action1Code[1]*256+o.Action1Code[0];
            cbbAct2.SelectedIndex = o.Action2Code[1] * 256 + o.Action2Code[0];
            cbbAct3.SelectedIndex = o.Action3Code[1] * 256 + o.Action3Code[0];
            cbbAct4.SelectedIndex = o.Action4Code[1] * 256 + o.Action4Code[0];
            cbbAct5.SelectedIndex = o.Action5Code[1] * 256 + o.Action5Code[0];
            cbbAct6.SelectedIndex = o.Action6Code[1] * 256 + o.Action6Code[0];
            cbbAct7.SelectedIndex = o.Action7Code[1] * 256 + o.Action7Code[0];

            ncAct1.Value = o.Action1Code[2] == 0xFF ? 0 : o.Action1Code[2];
            ncAct2.Value = o.Action2Code[2] == 0xFF ? 0 : o.Action2Code[2];
            ncAct3.Value = o.Action3Code[2] == 0xFF ? 0 : o.Action3Code[2];
            ncAct4.Value = o.Action4Code[2] == 0xFF ? 0 : o.Action4Code[2];
            ncAct5.Value = o.Action5Code[2] == 0xFF ? 0 : o.Action5Code[2];
            ncAct6.Value = o.Action6Code[2] == 0xFF ? 0 : o.Action6Code[2];
            ncAct7.Value = o.Action7Code[2] == 0xFF ? 0 : o.Action7Code[2];

            tbUKAct1.Text=String.Format("{0:X2} {1:X2} {2:X2}", o.Action1Code[3], o.Action1Code[4], o.Action1Code[5]);
            tbUKAct2.Text = String.Format("{0:X2} {1:X2} {2:X2}", o.Action2Code[3], o.Action2Code[4], o.Action2Code[5]);
            tbUKAct3.Text = String.Format("{0:X2} {1:X2} {2:X2}", o.Action3Code[3], o.Action3Code[4], o.Action3Code[5]);
            tbUKAct4.Text = String.Format("{0:X2} {1:X2} {2:X2}", o.Action4Code[3], o.Action4Code[4], o.Action4Code[5]);
            tbUKAct5.Text = String.Format("{0:X2} {1:X2} {2:X2}", o.Action5Code[3], o.Action5Code[4], o.Action5Code[5]);
            tbUKAct6.Text = String.Format("{0:X2} {1:X2} {2:X2}", o.Action6Code[3], o.Action6Code[4], o.Action6Code[5]);
            tbUKAct7.Text = String.Format("{0:X2} {1:X2} {2:X2}", o.Action7Code[3], o.Action7Code[4], o.Action7Code[5]);

        }

        private void btnSaveToRom_Click(object sender, EventArgs e)
        {
            byte[] bytes = currentItem.Data.ToArray();
            bytes[4] =(byte)(ncHP.Value % 256) ;
            bytes[5] = (byte) (ncHP.Value / 256);

            bytes[6] = (byte)(ncATK.Value % 256);
            bytes[7] = (byte)(ncATK.Value / 256);

            bytes[8] = (byte)(ncDEF.Value % 256);
            bytes[9] = (byte)(ncDEF.Value / 256);

            bytes[10] = (byte)(ncSPD.Value % 256);
            bytes[11] = (byte)(ncSPD.Value / 256);

            bytes[12] = (byte)(ncExp.Value % 256);
            bytes[13] = (byte)(ncExp.Value / 256);

            bytes[14] = (byte)(ncMoney.Value % 256);
            bytes[15] = (byte)(ncMoney.Value / 256);

            bytes[25] = (byte)ncLev.Value;
            bytes[27] = (byte)ncHIT.Value;
            bytes[28] = (byte)ncPAR.Value;
            bytes[31]= (byte)ncActionTimes.Value;

            bytes[42] = (byte)ncUnknowP1.Value;
            bytes[44] = (byte)ncNormal.Value;
            bytes[46] = (byte)ncFire.Value;
            bytes[48] = (byte)ncIce.Value;
            bytes[50] = (byte)ncElectri.Value;
            bytes[52] = (byte)ncSound.Value;
            bytes[54] = (byte)ncGas.Value;
            bytes[56] = (byte)ncLaser.Value;
            bytes[58] = (byte)ncUnknowP2.Value;


            //行动
            bytes[60] = (byte)(cbbAct1.SelectedIndex % 256);
            bytes[61] = (byte)(cbbAct1.SelectedIndex / 256);
            bytes[62] = (byte)(ncAct1.Value==0?0xFF:ncAct1.Value);

            
            bytes[66] = (byte)(cbbAct2.SelectedIndex % 256);
            bytes[67] = (byte)(cbbAct2.SelectedIndex / 256);
            bytes[68] = (byte)(ncAct2.Value == 0 ? 0xFF : ncAct2.Value);

            bytes[72] = (byte)(cbbAct3.SelectedIndex % 256);
            bytes[73] = (byte)(cbbAct3.SelectedIndex / 256);
            bytes[74] = (byte)(ncAct3.Value == 0 ? 0xFF : ncAct3.Value);

            bytes[78] = (byte)(cbbAct4.SelectedIndex % 256);
            bytes[79] = (byte)(cbbAct4.SelectedIndex / 256);
            bytes[80] = (byte)(ncAct4.Value == 0 ? 0xFF : ncAct4.Value);

            bytes[84] = (byte)(cbbAct5.SelectedIndex % 256);
            bytes[85] = (byte)(cbbAct5.SelectedIndex / 256);
            bytes[86] = (byte)(ncAct5.Value == 0 ? 0xFF : ncAct5.Value);

            bytes[90] = (byte)(cbbAct6.SelectedIndex % 256);
            bytes[91] = (byte)(cbbAct6.SelectedIndex / 256);
            bytes[92] = (byte)(ncAct6.Value == 0 ? 0xFF : ncAct6.Value);

            bytes[96] = (byte)(cbbAct7.SelectedIndex % 256);
            bytes[97] = (byte)(cbbAct7.SelectedIndex / 256);
            bytes[98] = (byte)(ncAct7.Value == 0 ? 0xFF : ncAct7.Value);
            //掉落
            bytes[102] = (byte)((Convert.ToInt32(cbbDrop1.SelectedValue)+1) % 256);
            bytes[103] = (byte)((Convert.ToInt32(cbbDrop1.SelectedValue) + 1) / 256);
            bytes[104] = (byte) ncDrop1.Value;

            bytes[106] = (byte)((Convert.ToInt32(cbbDrop2.SelectedValue) + 1) % 256);
            bytes[107] = (byte)((Convert.ToInt32(cbbDrop2.SelectedValue) + 1) / 256);
            bytes[108] = (byte)ncDrop2.Value;


            bytes[110] = (byte)((Convert.ToInt32(cbbDrop3.SelectedValue) + 1) % 256);
            bytes[111] = (byte)((Convert.ToInt32(cbbDrop3.SelectedValue) + 1) / 256);
            bytes[112] = (byte)ncDrop3.Value;

            bytes[114] = (byte)((Convert.ToInt32(cbbDrop4.SelectedValue) + 1) % 256);
            bytes[115] = (byte)((Convert.ToInt32(cbbDrop4.SelectedValue) + 1) / 256);
            bytes[116] = (byte)ncDrop4.Value;

            try
            {
               byte[] newBytes= FileOperateHelper.modifyBin(DataBaseService.getRomPath(), Convert.ToInt32(tbRomAddr.Text, 16), bytes);
               XlsMonster newO= XlsMonsterArrDecode.loadData(newBytes, "edit")[0];
                List<XlsMonster> datas = DataBaseService.getData(ConstanDef.ArrFileKey_monster) as List<XlsMonster>;
                int index = Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag);
                XlsMonster oldO = datas[index];
                newO.Offset = oldO.Offset;
                datas[index] = newO;
                loadItem(index);
                MessageBox.Show("写入完毕" );
            }
            catch (Exception exception)
            {
                MessageBox.Show("写入失败," + exception.Message);
            }
        }
    }
}
