﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsCarInitArrDecode
    {
        public static List<XlsCarInit> loadData(byte[] datas, string key)
        {
            List<XlsNam> itemNames = DataBaseService.getData("carinit_initdata.nam") as List<XlsNam>;
            List<XlsCarInit> result = new List<XlsCarInit>();

            int index = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + 39 >= datas.Length)
                {
                    break;
                }
                XlsCarInit o = new XlsCarInit();
                result.Add(o);
                o.SeqNo = index;
                o.Offset = i;
                o.NameOffAddr = datas[i + 1] * 256 + datas[i];

                var temp = itemNames.FirstOrDefault(x => x.Offset == o.NameOffAddr);
                o.Name = temp == null ? "unknown" : temp.Text;


                o.ChassisIdx = datas[i + 3] * 256 + datas[i + 2];
                o.CunitIdx = datas[i + 5] * 256 + datas[i + 4];
                o.EnginIdx = datas[i + 7] * 256 + datas[i + 6];
                o.Hole1Idx = datas[i + 9] * 256 + datas[i + 8];
                o.Hole2Idx = datas[i + 11] * 256 + datas[i + 10];
                o.Hole3Idx = datas[i + 13] * 256 + datas[i + 12];
                o.Hole4Idx = datas[i + 15] * 256 + datas[i + 14];
                o.Hole5Idx = datas[i + 17] * 256 + datas[i + 16];
                o.Hole6Idx = datas[i + 19] * 256 + datas[i + 18];
                o.Item1Idx = datas[i + 21] * 256 + datas[i + 20];
                o.Item2Idx = datas[i + 23] * 256 + datas[i + 22];
                o.Item3Idx = datas[i + 25] * 256 + datas[i + 24];
                o.Item4Idx = datas[i + 27] * 256 + datas[i + 26];
                o.Item5Idx = datas[i + 29] * 256 + datas[i + 28];
                o.Unknown = String.Format("{0:X2} {1:X2} {2:X2} {3:X2} {4:X2} {5:X2} {6:X2} {7:X2}", datas[i + 30], datas[i + 31],
                    datas[i + 32], datas[i + 33], datas[i + 34], datas[i + 35], datas[i + 36], datas[i + 37]);
                o.Sp= datas[i + 39] * 256 + datas[i + 38];

                for (int j = 0; j < 40; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }


                index++;


                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o,key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null,key,true);
            DataBaseService.addData(key, result);
            return result;
        }
    }
}
