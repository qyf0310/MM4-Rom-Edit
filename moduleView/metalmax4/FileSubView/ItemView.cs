﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using MM4RomEdit.Controls;
using MM4RomEdit.moduleView.metalmax4.FileDecode;
using MM4RomEdit.moduleView.metalmax4.model;
using SharpConfig;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    public partial class ItemView : Form
    {
        private XlsItemList currentItem = null;
        public ItemView()
        {
            InitializeComponent();
            init();
        }

        public void init()
        {
            List<XlsItemList> datas = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            searchTree1.loadList(datas);
        }
        private void searchTree1_DoubleClick(object sender, EventArgs e)
        {
            SearchTree control = (SearchTree)sender;
            if (control.TreeView.SelectedNode != null)
            {
                loadItem(Convert.ToInt32(control.TreeView.SelectedNode.Tag));
            }
        }

        private void loadItem(int index)
        {
            List<XlsItemList> datas = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            XlsItemList o = datas[index];
            currentItem = o;
            tbName.Text = o.Name;
            tbType.Text = DataBaseService.getItemType(o.TypeId);
            ncMoney.Value = o.Cost;
            tbRemark.Text = "";
            tbRomAddr.Text = "";
            tbRomData.Text = "";

            chebIsDLC.Checked = o.IsDlc ? true : false;
            rbRandomYes.Checked = o.StarRandom ;
            rbRandomNo.Checked = !o.StarRandom;
            cbbStar.SelectedIndex = 0;
            try
            {
                String[] starList = o.StarGet.Split(new char[] {','});
                cbbStar.SelectedIndex = Convert.ToInt32(starList[starList.Length-1]);
            }
            catch (Exception e)
            {
                tbRemark.Text = o.StarGet;
            }
            tbRomAddr.Text = String.Format("{0:X8}",
                Convert.ToInt32(DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_itemlist)["data_offset"].StringValue,16) + o.Offset);
            StringBuilder sb=new StringBuilder();
            foreach (byte b in o.Data)
            {
                sb.Append(String.Format("{0:X2} ", b));
            }
            tbRomData.Text = sb.ToString();
        }

        private void btnSaveToRom_Click(object sender, EventArgs e)
        {
            byte[] bytes= currentItem.Data.ToArray();
            
            bool randomStar = rbRandomYes.Checked;
            int star = cbbStar.SelectedIndex;

            bytes[6] = (byte)(ncMoney.Value % 256);
            bytes[7] = (byte) (ncMoney.Value / 256);
            if (bytes[10]<13)
            {
                if (randomStar)
                {
                    bytes[10] = (byte)Math.Min(8 + star, 12);
                }
                else
                {
                    bytes[10] = (byte)star;
                }
            }
            try
            {
                FileOperateHelper.modifyBin(DataBaseService.getRomPath(), Convert.ToInt32(tbRomAddr.Text, 16), bytes);
                currentItem.Cost = (int)ncMoney.Value;

                currentItem.StarRandom = randomStar;
                if (randomStar)
                {
                    StringBuilder sb=new StringBuilder();
                    for (int i = 0; i <=star; i++)
                    {
                        sb.Append(i.ToString()).Append(",");
                    }
                    currentItem.StarGet = sb.ToString(0, sb.Length - 1);
                }
                else
                {
                    currentItem.StarGet = star.ToString();
                }
                currentItem.Data.Clear();
                foreach (byte b in bytes)
                {
                    currentItem.Data.Add(b);
                }

                loadItem(Convert.ToInt32(searchTree1.TreeView.SelectedNode.Tag));
                MessageBox.Show("写入成功");
            }
            catch (Exception exception)
            {
                MessageBox.Show("写入失败,"+exception.Message);
            }
            
        }

        private void cbbStar_SelectedIndexChanged(object sender, EventArgs e)
        {
            int star = cbbStar.SelectedIndex;
            if (rbRandomYes.Checked && star>4)
            {
                MessageBox.Show("星数随机变动时，预置值最大为4");
                cbbStar.SelectedIndex = 4;
                
            }
        }

        private void rbRandomYes_CheckedChanged(object sender, EventArgs e)
        {
            if (rbRandomYes.Checked && cbbStar.SelectedIndex > 4)
            {
                MessageBox.Show("星数预置值大于4时，不能设随机变动");
                rbRandomYes.Checked = false;
                rbRandomNo.Checked = true;
            }
        }

        private void tbPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != '\b')//允许输入退格键  
            {
                if ((e.KeyChar < '0') || (e.KeyChar > '9'))//允许输入0-9数字  
                {
                    e.Handled = true;
                }
            }
        }

        private void btnCarStar7_Click(object sender, EventArgs e)
        {
            List<XlsItemList> datas = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            //引擎、副炮、se、固副、固se
            List<XlsItemList> temp = datas.Where(x => x.TypeId == 12 || x.TypeId == 15 || x.TypeId == 16 || x.TypeId == 18 || x.TypeId == 19).ToList();
            int success = 0;
            foreach (XlsItemList item in temp)
            {
                try
                {
                    byte[] bytes = item.Data.ToArray();
                    bytes[10] = 7;
                    int romAddr =
                        Convert.ToInt32(
                            DataBaseService.getOffsetConfigSection(ConstanDef.ArrFileKey_itemlist)["data_offset"]
                                .StringValue, 16) + item.Offset;
                    FileOperateHelper.modifyBin(DataBaseService.getRomPath(), romAddr, bytes);
                    success++;
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }

            }
            MessageBox.Show(String.Format("修改完毕,total:{0},success:{1}", temp.Count, success));
        }

    }
}
