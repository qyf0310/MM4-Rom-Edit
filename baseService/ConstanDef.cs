﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4
{
    public class ConstanDef
    {
        public const String ArrFileKey_itemlist = "itemlist";
        public const String ArrFileKey_btl_actdata = "btl_actdata";
        public const String ArrFileKey_carinit = "carinit";
        public const String ArrFileKey_carwepon = "carwepon"; 
        public const String ArrFileKey_chassis = "chassis"; 
        public const String ArrFileKey_cunit = "cunit"; 
        public const String ArrFileKey_engine = "engine"; 
        public const String ArrFileKey_monster = "monster"; 
        public const String ArrFileKey_Human_Wepon = "human_wepon";
        public const String ArrFileKey_Human_Armors = "human_armors";
    }
}
