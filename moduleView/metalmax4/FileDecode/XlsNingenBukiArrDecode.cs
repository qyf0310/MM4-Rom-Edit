﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsNingenBukiArrDecode
    {
        public static List<XlsNingenBuki> loadData(byte[] datas, string key)
        {
            List<XlsItemList> itemNames = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            List<XlsNingenBuki> result = new List<XlsNingenBuki>();

            int index = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + 11 > datas.Length)
                {
                    break;
                }
                XlsNingenBuki o = new XlsNingenBuki();
                result.Add(o);
                o.SeqNo = index;
                o.Offset = i;
                o.Male = datas[i];
                o.Female = datas[i + 1];
                o.Animal = datas[i + 2];
                o.Attack = datas[i + 4] + datas[i + 5] * 256;
                o.Attribute= datas[i + 6] + datas[i + 7] * 256;
                o.AnimeId = datas[i + 8] + datas[i + 9] * 256;
                o.Range = datas[i + 10];
                o.Unknown = datas[i + 11];

                o.Name = "";
                if (itemNames != null)
                {
                    List<XlsItemList> temp = itemNames.Where(x => x.TypeId ==1).OrderBy(x => x.Offset).ToList();
                    if (temp.Any())
                    {
                        o.Name = temp[index].Name;
                    }
                }

                for (int j = 0; j < 12; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }


                index++;


                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o, key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null, key, true);
            DataBaseService.addData(key, result);
            return result;
        }
    }
}
