﻿using MM4RomEdit.Controls;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    partial class HumanArmorView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSaveToRom = new System.Windows.Forms.Button();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbRomAddr = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbRomData = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chebEquiFQ = new System.Windows.Forms.CheckBox();
            this.chebEquiMQ = new System.Windows.Forms.CheckBox();
            this.chebEquiFW = new System.Windows.Forms.CheckBox();
            this.chebEquiMW = new System.Windows.Forms.CheckBox();
            this.chebEquiFY = new System.Windows.Forms.CheckBox();
            this.chebEquiMY = new System.Windows.Forms.CheckBox();
            this.chebEquiFS = new System.Windows.Forms.CheckBox();
            this.chebEquiMS = new System.Windows.Forms.CheckBox();
            this.chebEquiFH = new System.Windows.Forms.CheckBox();
            this.chebEquiMH = new System.Windows.Forms.CheckBox();
            this.chebEquiFB = new System.Windows.Forms.CheckBox();
            this.chebEquiMB = new System.Windows.Forms.CheckBox();
            this.chebEquiFX = new System.Windows.Forms.CheckBox();
            this.chebEquiMX = new System.Windows.Forms.CheckBox();
            this.chebEquiFL = new System.Windows.Forms.CheckBox();
            this.chebEquiML = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chebEquiBear = new System.Windows.Forms.CheckBox();
            this.chebEquiDog = new System.Windows.Forms.CheckBox();
            this.chebEquiAnime = new System.Windows.Forms.CheckBox();
            this.chebEquiFM = new System.Windows.Forms.CheckBox();
            this.chebEquibM = new System.Windows.Forms.CheckBox();
            this.chebEquiAll = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbUnknown = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tbPart = new System.Windows.Forms.TextBox();
            this.ncUnknowP2 = new NumericControl();
            this.ncLaser = new NumericControl();
            this.ncGas = new NumericControl();
            this.ncSound = new NumericControl();
            this.ncElectri = new NumericControl();
            this.ncIce = new NumericControl();
            this.ncFire = new NumericControl();
            this.ncUnknowP1 = new NumericControl();
            this.ncMan = new NumericControl();
            this.ncSpeed = new NumericControl();
            this.ncDEF = new NumericControl();
            this.ncATK = new NumericControl();
            this.searchTree1 = new SearchTree();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncUnknowP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncLaser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncGas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncSound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncElectri)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncIce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncFire)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncUnknowP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncMan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncATK)).BeginInit();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(228, 6);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(189, 21);
            this.tbName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "名称    ：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 48;
            this.label7.Text = "攻击：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbUnknown);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.ncMan);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.ncSpeed);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.ncDEF);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.ncATK);
            this.groupBox1.Location = new System.Drawing.Point(521, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(129, 241);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "属性";
            // 
            // btnSaveToRom
            // 
            this.btnSaveToRom.Location = new System.Drawing.Point(480, 280);
            this.btnSaveToRom.Name = "btnSaveToRom";
            this.btnSaveToRom.Size = new System.Drawing.Size(86, 23);
            this.btnSaveToRom.TabIndex = 63;
            this.btnSaveToRom.Text = "写入ROM";
            this.btnSaveToRom.UseVisualStyleBackColor = true;
            this.btnSaveToRom.Click += new System.EventHandler(this.btnSaveToRom_Click);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(423, 280);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(44, 21);
            this.tbLength.TabIndex = 62;
            this.tbLength.Text = "16";
            this.tbLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(376, 283);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 61;
            this.label38.Text = "长度：";
            // 
            // tbRomAddr
            // 
            this.tbRomAddr.Location = new System.Drawing.Point(228, 280);
            this.tbRomAddr.Name = "tbRomAddr";
            this.tbRomAddr.ReadOnly = true;
            this.tbRomAddr.Size = new System.Drawing.Size(136, 21);
            this.tbRomAddr.TabIndex = 60;
            this.tbRomAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(157, 283);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 12);
            this.label37.TabIndex = 59;
            this.label37.Text = "ROM地址：";
            // 
            // tbRomData
            // 
            this.tbRomData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRomData.Location = new System.Drawing.Point(159, 324);
            this.tbRomData.Multiline = true;
            this.tbRomData.Name = "tbRomData";
            this.tbRomData.ReadOnly = true;
            this.tbRomData.Size = new System.Drawing.Size(570, 96);
            this.tbRomData.TabIndex = 65;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(157, 309);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 12);
            this.label39.TabIndex = 64;
            this.label39.Text = "ROM数据：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chebEquiFQ);
            this.groupBox2.Controls.Add(this.chebEquiMQ);
            this.groupBox2.Controls.Add(this.chebEquiFW);
            this.groupBox2.Controls.Add(this.chebEquiMW);
            this.groupBox2.Controls.Add(this.chebEquiFY);
            this.groupBox2.Controls.Add(this.chebEquiMY);
            this.groupBox2.Controls.Add(this.chebEquiFS);
            this.groupBox2.Controls.Add(this.chebEquiMS);
            this.groupBox2.Controls.Add(this.chebEquiFH);
            this.groupBox2.Controls.Add(this.chebEquiMH);
            this.groupBox2.Controls.Add(this.chebEquiFB);
            this.groupBox2.Controls.Add(this.chebEquiMB);
            this.groupBox2.Controls.Add(this.chebEquiFX);
            this.groupBox2.Controls.Add(this.chebEquiMX);
            this.groupBox2.Controls.Add(this.chebEquiFL);
            this.groupBox2.Controls.Add(this.chebEquiML);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.chebEquiBear);
            this.groupBox2.Controls.Add(this.chebEquiDog);
            this.groupBox2.Controls.Add(this.chebEquiAnime);
            this.groupBox2.Controls.Add(this.chebEquiFM);
            this.groupBox2.Controls.Add(this.chebEquibM);
            this.groupBox2.Controls.Add(this.chebEquiAll);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(157, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(201, 241);
            this.groupBox2.TabIndex = 70;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "装备";
            // 
            // chebEquiFQ
            // 
            this.chebEquiFQ.AutoSize = true;
            this.chebEquiFQ.Location = new System.Drawing.Point(101, 222);
            this.chebEquiFQ.Name = "chebEquiFQ";
            this.chebEquiFQ.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFQ.TabIndex = 100;
            this.chebEquiFQ.UseVisualStyleBackColor = true;
            // 
            // chebEquiMQ
            // 
            this.chebEquiMQ.AutoSize = true;
            this.chebEquiMQ.Location = new System.Drawing.Point(59, 221);
            this.chebEquiMQ.Name = "chebEquiMQ";
            this.chebEquiMQ.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMQ.TabIndex = 99;
            this.chebEquiMQ.UseVisualStyleBackColor = true;
            // 
            // chebEquiFW
            // 
            this.chebEquiFW.AutoSize = true;
            this.chebEquiFW.Location = new System.Drawing.Point(101, 197);
            this.chebEquiFW.Name = "chebEquiFW";
            this.chebEquiFW.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFW.TabIndex = 98;
            this.chebEquiFW.UseVisualStyleBackColor = true;
            // 
            // chebEquiMW
            // 
            this.chebEquiMW.AutoSize = true;
            this.chebEquiMW.Location = new System.Drawing.Point(59, 196);
            this.chebEquiMW.Name = "chebEquiMW";
            this.chebEquiMW.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMW.TabIndex = 97;
            this.chebEquiMW.UseVisualStyleBackColor = true;
            // 
            // chebEquiFY
            // 
            this.chebEquiFY.AutoSize = true;
            this.chebEquiFY.Location = new System.Drawing.Point(101, 172);
            this.chebEquiFY.Name = "chebEquiFY";
            this.chebEquiFY.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFY.TabIndex = 96;
            this.chebEquiFY.UseVisualStyleBackColor = true;
            // 
            // chebEquiMY
            // 
            this.chebEquiMY.AutoSize = true;
            this.chebEquiMY.Location = new System.Drawing.Point(59, 171);
            this.chebEquiMY.Name = "chebEquiMY";
            this.chebEquiMY.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMY.TabIndex = 95;
            this.chebEquiMY.UseVisualStyleBackColor = true;
            // 
            // chebEquiFS
            // 
            this.chebEquiFS.AutoSize = true;
            this.chebEquiFS.Location = new System.Drawing.Point(101, 147);
            this.chebEquiFS.Name = "chebEquiFS";
            this.chebEquiFS.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFS.TabIndex = 94;
            this.chebEquiFS.UseVisualStyleBackColor = true;
            // 
            // chebEquiMS
            // 
            this.chebEquiMS.AutoSize = true;
            this.chebEquiMS.Location = new System.Drawing.Point(59, 146);
            this.chebEquiMS.Name = "chebEquiMS";
            this.chebEquiMS.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMS.TabIndex = 93;
            this.chebEquiMS.UseVisualStyleBackColor = true;
            // 
            // chebEquiFH
            // 
            this.chebEquiFH.AutoSize = true;
            this.chebEquiFH.Location = new System.Drawing.Point(101, 123);
            this.chebEquiFH.Name = "chebEquiFH";
            this.chebEquiFH.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFH.TabIndex = 92;
            this.chebEquiFH.UseVisualStyleBackColor = true;
            // 
            // chebEquiMH
            // 
            this.chebEquiMH.AutoSize = true;
            this.chebEquiMH.Location = new System.Drawing.Point(59, 122);
            this.chebEquiMH.Name = "chebEquiMH";
            this.chebEquiMH.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMH.TabIndex = 91;
            this.chebEquiMH.UseVisualStyleBackColor = true;
            // 
            // chebEquiFB
            // 
            this.chebEquiFB.AutoSize = true;
            this.chebEquiFB.Location = new System.Drawing.Point(101, 100);
            this.chebEquiFB.Name = "chebEquiFB";
            this.chebEquiFB.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFB.TabIndex = 90;
            this.chebEquiFB.UseVisualStyleBackColor = true;
            // 
            // chebEquiMB
            // 
            this.chebEquiMB.AutoSize = true;
            this.chebEquiMB.Location = new System.Drawing.Point(59, 99);
            this.chebEquiMB.Name = "chebEquiMB";
            this.chebEquiMB.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMB.TabIndex = 89;
            this.chebEquiMB.UseVisualStyleBackColor = true;
            // 
            // chebEquiFX
            // 
            this.chebEquiFX.AutoSize = true;
            this.chebEquiFX.Location = new System.Drawing.Point(101, 76);
            this.chebEquiFX.Name = "chebEquiFX";
            this.chebEquiFX.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFX.TabIndex = 88;
            this.chebEquiFX.UseVisualStyleBackColor = true;
            // 
            // chebEquiMX
            // 
            this.chebEquiMX.AutoSize = true;
            this.chebEquiMX.Location = new System.Drawing.Point(59, 75);
            this.chebEquiMX.Name = "chebEquiMX";
            this.chebEquiMX.Size = new System.Drawing.Size(15, 14);
            this.chebEquiMX.TabIndex = 87;
            this.chebEquiMX.UseVisualStyleBackColor = true;
            // 
            // chebEquiFL
            // 
            this.chebEquiFL.AutoSize = true;
            this.chebEquiFL.Location = new System.Drawing.Point(101, 51);
            this.chebEquiFL.Name = "chebEquiFL";
            this.chebEquiFL.Size = new System.Drawing.Size(15, 14);
            this.chebEquiFL.TabIndex = 86;
            this.chebEquiFL.UseVisualStyleBackColor = true;
            // 
            // chebEquiML
            // 
            this.chebEquiML.AutoSize = true;
            this.chebEquiML.Location = new System.Drawing.Point(59, 50);
            this.chebEquiML.Name = "chebEquiML";
            this.chebEquiML.Size = new System.Drawing.Size(15, 14);
            this.chebEquiML.TabIndex = 85;
            this.chebEquiML.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 223);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 84;
            this.label10.Text = "骑";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 83;
            this.label9.Text = "舞";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 173);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 82;
            this.label8.Text = "艺";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 81;
            this.label6.Text = "摔";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 80;
            this.label5.Text = "护";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 79;
            this.label4.Text = "兵";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 78;
            this.label2.Text = "械";
            // 
            // chebEquiBear
            // 
            this.chebEquiBear.AutoSize = true;
            this.chebEquiBear.Location = new System.Drawing.Point(147, 74);
            this.chebEquiBear.Name = "chebEquiBear";
            this.chebEquiBear.Size = new System.Drawing.Size(36, 16);
            this.chebEquiBear.TabIndex = 77;
            this.chebEquiBear.Text = "熊";
            this.chebEquiBear.UseVisualStyleBackColor = true;
            // 
            // chebEquiDog
            // 
            this.chebEquiDog.AutoSize = true;
            this.chebEquiDog.Location = new System.Drawing.Point(147, 52);
            this.chebEquiDog.Name = "chebEquiDog";
            this.chebEquiDog.Size = new System.Drawing.Size(36, 16);
            this.chebEquiDog.TabIndex = 76;
            this.chebEquiDog.Text = "犬";
            this.chebEquiDog.UseVisualStyleBackColor = true;
            // 
            // chebEquiAnime
            // 
            this.chebEquiAnime.AutoSize = true;
            this.chebEquiAnime.Location = new System.Drawing.Point(147, 20);
            this.chebEquiAnime.Name = "chebEquiAnime";
            this.chebEquiAnime.Size = new System.Drawing.Size(48, 16);
            this.chebEquiAnime.TabIndex = 75;
            this.chebEquiAnime.Text = "动物";
            this.chebEquiAnime.UseVisualStyleBackColor = true;
            this.chebEquiAnime.CheckedChanged += new System.EventHandler(this.chebEquiAnime_CheckedChanged);
            // 
            // chebEquiFM
            // 
            this.chebEquiFM.AutoSize = true;
            this.chebEquiFM.Location = new System.Drawing.Point(101, 20);
            this.chebEquiFM.Name = "chebEquiFM";
            this.chebEquiFM.Size = new System.Drawing.Size(36, 16);
            this.chebEquiFM.TabIndex = 74;
            this.chebEquiFM.Text = "女";
            this.chebEquiFM.UseVisualStyleBackColor = true;
            this.chebEquiFM.CheckedChanged += new System.EventHandler(this.chebEquiFM_CheckedChanged);
            // 
            // chebEquibM
            // 
            this.chebEquibM.AutoSize = true;
            this.chebEquibM.Location = new System.Drawing.Point(59, 20);
            this.chebEquibM.Name = "chebEquibM";
            this.chebEquibM.Size = new System.Drawing.Size(36, 16);
            this.chebEquibM.TabIndex = 73;
            this.chebEquibM.Text = "男";
            this.chebEquibM.UseVisualStyleBackColor = true;
            this.chebEquibM.CheckedChanged += new System.EventHandler(this.chebEquibM_CheckedChanged);
            // 
            // chebEquiAll
            // 
            this.chebEquiAll.AutoSize = true;
            this.chebEquiAll.Location = new System.Drawing.Point(8, 20);
            this.chebEquiAll.Name = "chebEquiAll";
            this.chebEquiAll.Size = new System.Drawing.Size(36, 16);
            this.chebEquiAll.TabIndex = 72;
            this.chebEquiAll.Text = "全";
            this.chebEquiAll.UseVisualStyleBackColor = true;
            this.chebEquiAll.CheckedChanged += new System.EventHandler(this.chebEquiAll_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 71;
            this.label1.Text = "猎";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ncUnknowP2);
            this.groupBox3.Controls.Add(this.ncLaser);
            this.groupBox3.Controls.Add(this.ncGas);
            this.groupBox3.Controls.Add(this.ncSound);
            this.groupBox3.Controls.Add(this.ncElectri);
            this.groupBox3.Controls.Add(this.ncIce);
            this.groupBox3.Controls.Add(this.ncFire);
            this.groupBox3.Controls.Add(this.ncUnknowP1);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Location = new System.Drawing.Point(370, 33);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(145, 241);
            this.groupBox3.TabIndex = 71;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "抗性";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 208);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 30;
            this.label11.Text = "未知：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 181);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 12);
            this.label16.TabIndex = 28;
            this.label16.Text = "激光：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 154);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 12);
            this.label17.TabIndex = 26;
            this.label17.Text = "瓦斯：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 127);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 24;
            this.label18.Text = "音波：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 100);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 12);
            this.label19.TabIndex = 22;
            this.label19.Text = "电  ：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 73);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 20;
            this.label20.Text = "冰  ：";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 46);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 12);
            this.label21.TabIndex = 18;
            this.label21.Text = "火  ：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 14;
            this.label23.Text = "未知：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 50;
            this.label12.Text = "防御：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 52;
            this.label13.Text = "速度：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 127);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 54;
            this.label14.Text = "男味：";
            // 
            // tbUnknown
            // 
            this.tbUnknown.Location = new System.Drawing.Point(53, 156);
            this.tbUnknown.Name = "tbUnknown";
            this.tbUnknown.ReadOnly = true;
            this.tbUnknown.Size = new System.Drawing.Size(44, 21);
            this.tbUnknown.TabIndex = 73;
            this.tbUnknown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 159);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 72;
            this.label15.Text = "不明：";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(426, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 74;
            this.label22.Text = "部位：";
            // 
            // tbPart
            // 
            this.tbPart.Location = new System.Drawing.Point(465, 6);
            this.tbPart.Name = "tbPart";
            this.tbPart.ReadOnly = true;
            this.tbPart.Size = new System.Drawing.Size(44, 21);
            this.tbPart.TabIndex = 74;
            this.tbPart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ncUnknowP2
            // 
            this.ncUnknowP2.Location = new System.Drawing.Point(53, 204);
            this.ncUnknowP2.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncUnknowP2.MaxNumberControl = null;
            this.ncUnknowP2.MinNumberControl = null;
            this.ncUnknowP2.Name = "ncUnknowP2";
            this.ncUnknowP2.Size = new System.Drawing.Size(86, 21);
            this.ncUnknowP2.TabIndex = 57;
            this.ncUnknowP2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncLaser
            // 
            this.ncLaser.Location = new System.Drawing.Point(53, 177);
            this.ncLaser.MaxNumberControl = null;
            this.ncLaser.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.ncLaser.MinNumberControl = null;
            this.ncLaser.Name = "ncLaser";
            this.ncLaser.Size = new System.Drawing.Size(86, 21);
            this.ncLaser.TabIndex = 56;
            this.ncLaser.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncGas
            // 
            this.ncGas.Location = new System.Drawing.Point(53, 150);
            this.ncGas.MaxNumberControl = null;
            this.ncGas.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.ncGas.MinNumberControl = null;
            this.ncGas.Name = "ncGas";
            this.ncGas.Size = new System.Drawing.Size(86, 21);
            this.ncGas.TabIndex = 55;
            this.ncGas.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncSound
            // 
            this.ncSound.Location = new System.Drawing.Point(53, 123);
            this.ncSound.MaxNumberControl = null;
            this.ncSound.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.ncSound.MinNumberControl = null;
            this.ncSound.Name = "ncSound";
            this.ncSound.Size = new System.Drawing.Size(86, 21);
            this.ncSound.TabIndex = 54;
            this.ncSound.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncElectri
            // 
            this.ncElectri.Location = new System.Drawing.Point(53, 96);
            this.ncElectri.MaxNumberControl = null;
            this.ncElectri.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.ncElectri.MinNumberControl = null;
            this.ncElectri.Name = "ncElectri";
            this.ncElectri.Size = new System.Drawing.Size(86, 21);
            this.ncElectri.TabIndex = 53;
            this.ncElectri.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncIce
            // 
            this.ncIce.Location = new System.Drawing.Point(53, 70);
            this.ncIce.MaxNumberControl = null;
            this.ncIce.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.ncIce.MinNumberControl = null;
            this.ncIce.Name = "ncIce";
            this.ncIce.Size = new System.Drawing.Size(86, 21);
            this.ncIce.TabIndex = 52;
            this.ncIce.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncFire
            // 
            this.ncFire.Location = new System.Drawing.Point(53, 44);
            this.ncFire.MaxNumberControl = null;
            this.ncFire.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            -2147483648});
            this.ncFire.MinNumberControl = null;
            this.ncFire.Name = "ncFire";
            this.ncFire.Size = new System.Drawing.Size(86, 21);
            this.ncFire.TabIndex = 51;
            this.ncFire.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncUnknowP1
            // 
            this.ncUnknowP1.Location = new System.Drawing.Point(53, 19);
            this.ncUnknowP1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncUnknowP1.MaxNumberControl = null;
            this.ncUnknowP1.MinNumberControl = null;
            this.ncUnknowP1.Name = "ncUnknowP1";
            this.ncUnknowP1.Size = new System.Drawing.Size(86, 21);
            this.ncUnknowP1.TabIndex = 49;
            this.ncUnknowP1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncMan
            // 
            this.ncMan.Location = new System.Drawing.Point(53, 125);
            this.ncMan.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.ncMan.MaxNumberControl = null;
            this.ncMan.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            -2147483648});
            this.ncMan.MinNumberControl = null;
            this.ncMan.Name = "ncMan";
            this.ncMan.Size = new System.Drawing.Size(58, 21);
            this.ncMan.TabIndex = 55;
            this.ncMan.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncSpeed
            // 
            this.ncSpeed.Location = new System.Drawing.Point(53, 91);
            this.ncSpeed.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.ncSpeed.MaxNumberControl = null;
            this.ncSpeed.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            -2147483648});
            this.ncSpeed.MinNumberControl = null;
            this.ncSpeed.Name = "ncSpeed";
            this.ncSpeed.Size = new System.Drawing.Size(58, 21);
            this.ncSpeed.TabIndex = 53;
            this.ncSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncDEF
            // 
            this.ncDEF.Location = new System.Drawing.Point(53, 55);
            this.ncDEF.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncDEF.MaxNumberControl = null;
            this.ncDEF.MinNumberControl = null;
            this.ncDEF.Name = "ncDEF";
            this.ncDEF.Size = new System.Drawing.Size(58, 21);
            this.ncDEF.TabIndex = 51;
            this.ncDEF.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncATK
            // 
            this.ncATK.Location = new System.Drawing.Point(53, 19);
            this.ncATK.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncATK.MaxNumberControl = null;
            this.ncATK.MinNumberControl = null;
            this.ncATK.Name = "ncATK";
            this.ncATK.Size = new System.Drawing.Size(58, 21);
            this.ncATK.TabIndex = 49;
            this.ncATK.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // searchTree1
            // 
            this.searchTree1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTree1.Location = new System.Drawing.Point(2, 2);
            this.searchTree1.Name = "searchTree1";
            this.searchTree1.Size = new System.Drawing.Size(149, 428);
            this.searchTree1.TabIndex = 0;
            this.searchTree1.DoubleClick += new System.EventHandler(this.searchTree1_DoubleClick);
            // 
            // HumanArmorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 432);
            this.Controls.Add(this.tbPart);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tbRomData);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.btnSaveToRom);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.tbRomAddr);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.searchTree1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "HumanArmorView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "CarView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncUnknowP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncLaser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncGas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncSound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncElectri)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncIce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncFire)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncUnknowP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncMan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncATK)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.SearchTree searchTree1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private Controls.NumericControl ncATK;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSaveToRom;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbRomAddr;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbRomData;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chebEquiAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chebEquiAnime;
        private System.Windows.Forms.CheckBox chebEquiFM;
        private System.Windows.Forms.CheckBox chebEquibM;
        private System.Windows.Forms.CheckBox chebEquiBear;
        private System.Windows.Forms.CheckBox chebEquiDog;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chebEquiFL;
        private System.Windows.Forms.CheckBox chebEquiML;
        private System.Windows.Forms.CheckBox chebEquiFQ;
        private System.Windows.Forms.CheckBox chebEquiMQ;
        private System.Windows.Forms.CheckBox chebEquiFW;
        private System.Windows.Forms.CheckBox chebEquiMW;
        private System.Windows.Forms.CheckBox chebEquiFY;
        private System.Windows.Forms.CheckBox chebEquiMY;
        private System.Windows.Forms.CheckBox chebEquiFS;
        private System.Windows.Forms.CheckBox chebEquiMS;
        private System.Windows.Forms.CheckBox chebEquiFH;
        private System.Windows.Forms.CheckBox chebEquiMH;
        private System.Windows.Forms.CheckBox chebEquiFB;
        private System.Windows.Forms.CheckBox chebEquiMB;
        private System.Windows.Forms.CheckBox chebEquiFX;
        private System.Windows.Forms.CheckBox chebEquiMX;
        private System.Windows.Forms.GroupBox groupBox3;
        private Controls.NumericControl ncUnknowP2;
        private Controls.NumericControl ncLaser;
        private Controls.NumericControl ncGas;
        private Controls.NumericControl ncSound;
        private Controls.NumericControl ncElectri;
        private Controls.NumericControl ncIce;
        private Controls.NumericControl ncFire;
        private Controls.NumericControl ncUnknowP1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label13;
        private Controls.NumericControl ncSpeed;
        private System.Windows.Forms.Label label12;
        private Controls.NumericControl ncDEF;
        private System.Windows.Forms.Label label14;
        private Controls.NumericControl ncMan;
        private System.Windows.Forms.TextBox tbUnknown;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbPart;
    }
}