﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsCarWeponArrDecode
    {

        public static List<XlsCarWepon> loadData(byte[] datas,string key)
        {
            List<XlsItemList> itemNames = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            List<XlsCarWepon> result = new List<XlsCarWepon>();

            int index = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + 23 >= datas.Length)
                {
                    break;
                }
                XlsCarWepon o = new XlsCarWepon();
                result.Add(o);
                o.SeqNo = index;
                o.Offset = i;
                o.RemodelCost = datas[i + 1] * 256 + datas[i];
                o.Weight = datas[i + 3] * 256 + datas[i + 2];
                o.InitDefen = datas[i + 4];
                o.MaxDefen = datas[i + 5];
                o.InitAttack = datas[i + 7] * 256 + datas[i + 6];
                o.AttAttribute = datas[i + 9] * 256 + datas[i + 8];

                o.AttAnime = datas[i + 11] * 256 + datas[i + 10];
                o.AttRange = datas[i + 12];
                o.InitBombNum = datas[i + 13];
                o.Unknown1 = datas[i + 14];
                o.Unknown2 = datas[i + 15] ;
                o.MaxBombNum = Math.Max(datas[i + 16], o.InitBombNum);
                o.Unknown3 = datas[i + 17];
                o.RemodelWeight = datas[i + 19] * 256 + datas[i + 18];
                o.MaxAttack = datas[i + 21] * 256 + datas[i + 20];
                o.Apperence = datas[i + 23] * 256 + datas[i + 22];

                //                o.AttDistanceName = "";
                o.AttRangeName = DataBaseService.getWeponAttkRange(o.AttRange);

                o.Name = "";
                if (itemNames != null)
                {
                    List<XlsItemList> temp = itemNames.Where(x => x.TypeId >= 14 && x.TypeId < 20).OrderBy(x => x.Offset).ToList();
                    if (temp.Any())
                    {
                        o.Name = temp[index].Name;
                    }
                }


                for (int j = 0; j < 24; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }

                index++;


                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o,key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null,key,true);
            DataBaseService.addData(key, result);
            return result;
        }

    }
}
