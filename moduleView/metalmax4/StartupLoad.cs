﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4
{
    class StartupLoad:AbstractDataProcess
    {
        private int index = 0;
        protected override void run(object data)
        {
            String dataBasePath = System.Windows.Forms.Application.StartupPath + @"\namdatabase";
            index = 0;
            createMessage(string.Format("正在初始化...."), ProcessStateEnum.NORMAL, null);
            String[] files = Directory.GetFiles(dataBasePath);
            loadXlsNamCsv(dataBasePath);
//            loadPriorityArrData(dataBasePath);
//            loadXlsArrCsv(dataBasePath);

         
            createMessage(string.Format("全部加载完毕."), ProcessStateEnum.NORMAL, null);
        }

        private void loadXlsNamCsv(string dirPath)
        {
            createMessage(string.Format("加载系统文字用语...."), ProcessStateEnum.NORMAL, null);
            String[] files = Directory.GetFiles(dirPath);
            foreach (String file in files)
            {
                String fileName = file.Substring(file.LastIndexOf("\\") + 1);
                if (fileName.ToLower().IndexOf(".nam")< 0)
                {
                    continue;
                }
                index++;
                createMessage(string.Format("开始加载数据 {0}/{1}:{2}", index, files.Length, fileName), ProcessStateEnum.NORMAL, null);
                try
                {
                    String[] contents = File.ReadAllText(file).Split(new char[] { '\r' });
                    IList list = loadXlsNamData(contents);
                    DataBaseService.addData(fileName.Replace(".csv", ""), list);
                    createMessage(string.Format("加载成功,{0}/{1}.", index, files.Length), ProcessStateEnum.SUCCESS, null);
                }
                catch (Exception e)
                {
                    createMessage(string.Format("加载失败,{0}/{1}.{2}", index, files.Length, e.Message), ProcessStateEnum.FAILED, null);
                }
            }
            createMessage(string.Format("加载系统文字用语完毕."), ProcessStateEnum.NORMAL, null);
        }

        private void loadPriorityArrData(string dirPath)
        {
            String[] files = Directory.GetFiles(dirPath);
            foreach (String file in files)
            {
                String fileName = file.Substring(file.LastIndexOf("\\") + 1);
                if (fileName.ToLower().IndexOf("itemlist.arr") < 0)
                {
                    continue;
                }
                index++;
                createMessage(string.Format("开始加载数据 {0}/{1}:{2}", index, files.Length, fileName), ProcessStateEnum.NORMAL, null);
                try
                {

                    IList list = null;
                    String[] contents = File.ReadAllText(file).Split(new char[] { '\r' });
                    switch (fileName)
                    {
                        case "itemlist.arr.csv":
                            list = loadXlsItemlistArrData(contents);
                            break;
                        default:
                            createMessage(string.Format("加载失败,{0}/{1}.{2}", index, files.Length, "unsupported data file:" + fileName), ProcessStateEnum.FAILED, null);
                            continue;
                    }
                    DataBaseService.addData(fileName.Replace(".csv", ""), list);
                    createMessage(string.Format("加载成功,{0}/{1}.", index, files.Length), ProcessStateEnum.SUCCESS, null);
                }
                catch (Exception e)
                {
                    createMessage(string.Format("加载失败,{0}/{1}.{2}", index, files.Length, e.Message), ProcessStateEnum.FAILED, null);
                }
            }
        }
        private void loadXlsArrCsv(string dirPath)
        {
            String[] files = Directory.GetFiles(dirPath);
            foreach (String file in files)
            {
                String fileName = file.Substring(file.LastIndexOf("\\") + 1);
                if (fileName.ToLower().IndexOf(".arr") < 0 || fileName.ToLower().IndexOf("itemlist.arr") >= 0)
                {
                    continue;
                }
                index++;
                createMessage(string.Format("开始加载数据 {0}/{1}:{2}", index, files.Length, fileName), ProcessStateEnum.NORMAL, null);
                try
                {

                    IList list = null;
                    String[] contents = File.ReadAllText(file).Split(new char[] { '\r' });
                    switch (fileName)
                    {
                        case "cunit.arr.csv":
                            list = loadXlsCUnitArrData(contents);
                            break;
                        case "engine.arr.csv":
                            list = loadXlsEnginArrData(contents);
                            break;
                        case "carwepon.arr.csv":
                            list = loadXlsCarWeponArrData(contents);
                            break;
                        case "chassis.arr.csv":
                            list = loadXlsChassisArrData(contents);
                            break;
                        case "ene_monster.arr.csv":
                            list = loadXlsEneMonsterArrData(contents);
                            break;
                        default:
                            createMessage(string.Format("加载失败,{0}/{1}.{2}", index, files.Length, "unsupported data file:" + fileName), ProcessStateEnum.FAILED, null);
                            continue;
                    }
                    DataBaseService.addData(fileName.Replace(".csv", ""), list);
                    createMessage(string.Format("加载成功,{0}/{1}.", index, files.Length), ProcessStateEnum.SUCCESS, null);
                }
                catch (Exception e)
                {
                    createMessage(string.Format("加载失败,{0}/{1}.{2}", index, files.Length, e.Message), ProcessStateEnum.FAILED, null);
                }
            }
        }

        private List<XlsNam> loadXlsNamData(String[] contents)
        {
            List<XlsNam> result=new List<XlsNam>();
            Dictionary<String,int> headString2Ids=new Dictionary<string, int>();
            String header = contents[0];
            int index = 0;
            foreach (string s in header.Split(new char[] {','}))
            {
                headString2Ids.Add(s, index++);
            }
            for (int i = 1; i < contents.Length; i++)
            {
                if (contents[i].Equals("\n"))
                {
                    continue;
                }
                try
                {
                    XlsNam o = new XlsNam();
                    result.Add(o);
                    String[] content = contents[i].Split(new char[] { ',' });
                    o.CurrentLength = Convert.ToInt32(content[headString2Ids["CurrentLength"]]);
                    o.MaxLength = Convert.ToInt32(content[headString2Ids["MaxLength"]]);
                    o.Offset = Convert.ToInt64(content[headString2Ids["Offset"]], 16);
                    o.Text = content[headString2Ids["Text"]].Replace("，",",");
                    foreach (String s in content[headString2Ids["Data"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Data.Add(Convert.ToByte(s, 16));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
            return result;
        }
        private List<XlsItemList> loadXlsItemlistArrData(String[] contents)
        {
            List<XlsItemList> result = new List<XlsItemList>();
            Dictionary<String, int> headString2Ids = new Dictionary<string, int>();
            String header = contents[0];
            int index = 0;
            foreach (string s in header.Split(new char[] { ',' }))
            {
                headString2Ids.Add(s, index++);
            }
            for (int i = 1; i < contents.Length; i++)
            {
                if (contents[i].Equals("\n"))
                {
                    continue;
                }
                try
                {
                    XlsItemList o = new XlsItemList();
                    result.Add(o);
                    String[] content = contents[i].Split(new char[] { ',' });
                    o.DataId = Convert.ToInt32(content[headString2Ids["DataId"]]);
                    o.Name = content[headString2Ids["Name"]].Replace("，", ",");
                    o.NameOffsetAddr = Convert.ToInt32(content[headString2Ids["NameOffsetAddr"]], 16);
                    o.TypeId = Convert.ToInt32(content[headString2Ids["TypeId"]]);
                    o.Cost = Convert.ToInt32(content[headString2Ids["Cost"]]);
                    o.StarRandom = content[headString2Ids["StarRandom"]].Equals("是");
                    o.StarGet = content[headString2Ids["StarGet"]].Replace("，", ",");
                    o.IsDlc = content[headString2Ids["IsDlc"]].Equals("是");
                    o.Offset = Convert.ToInt32(content[headString2Ids["Offset"]], 16);

                    foreach (String s in content[headString2Ids["Data"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Data.Add(Convert.ToByte(s, 16));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
            return result;
        }
        private List<XlsCUnit> loadXlsCUnitArrData(String[] contents)
        {
            List<XlsItemList> itemNames = getDataFromDataBase("itemlist.arr") as List<XlsItemList>;
            List<XlsCUnit> result = new List<XlsCUnit>();
            Dictionary<String, int> headString2Ids = new Dictionary<string, int>();
            String header = contents[0];
            int index = 0;
            foreach (string s in header.Split(new char[] { ',' }))
            {
                headString2Ids.Add(s, index++);
            }
            for (int i = 1; i < contents.Length; i++)
            {
                if (contents[i].Equals("\n"))
                {
                    continue;
                }
                try
                {
                    XlsCUnit o = new XlsCUnit();
                    result.Add(o);
                    String[] content = contents[i].Split(new char[] { ',' });
                    o.SeqNo = Convert.ToInt32(content[headString2Ids["SeqNo"]]);
                    o.Offset = Convert.ToInt32(content[headString2Ids["Offset"]], 16);
                    o.RemodelCost = Convert.ToInt32(content[headString2Ids["RemodelCost"]]);
                    o.Weight = Convert.ToInt32(content[headString2Ids["Weight"]]);
                    o.InitDefen = Convert.ToInt32(content[headString2Ids["InitDefen"]]);
                    o.MaxDefen = Convert.ToInt32(content[headString2Ids["MaxDefen"]]);
                    o.InitHitRate = Convert.ToInt32(content[headString2Ids["InitHitRate"]]);
                    o.InitParryRate = Convert.ToInt32(content[headString2Ids["InitParryRate"]]);
                    o.MaxHitRate = Convert.ToInt32(content[headString2Ids["MaxHitRate"]]);
                    o.MaxParryRate = Convert.ToInt32(content[headString2Ids["MaxParryRate"]]);
                    o.InitAbilityNum = Convert.ToInt32(content[headString2Ids["InitAbilityNum"]]);
                    o.Ability1 = Convert.ToInt32(content[headString2Ids["Ability1"]]);
                    o.Ability2 = Convert.ToInt32(content[headString2Ids["Ability2"]]);
                    o.Ability3 = Convert.ToInt32(content[headString2Ids["Ability3"]]);
                    o.AbilityName = content[headString2Ids["AbilityName"]].Replace("，", ",");

                    o.Name = "";
                    if (itemNames != null)
                    {
                        List<XlsItemList> temp = itemNames.Where(x => x.TypeId == 0x0D).OrderBy(x => x.Offset).ToList();
                        if (temp.Any())
                        {
                            o.Name = temp[o.SeqNo].Name;
                        }
                    }

                    foreach (String s in content[headString2Ids["Data"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Data.Add(Convert.ToByte(s, 16));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
            return result;
        }

        private List<XlsEngine> loadXlsEnginArrData(String[] contents)
        {
            List<XlsItemList> itemNames = getDataFromDataBase("itemlist.arr") as List<XlsItemList>;
            List<XlsEngine> result = new List<XlsEngine>();
            Dictionary<String, int> headString2Ids = new Dictionary<string, int>();
            String header = contents[0];
            int index = 0;
            foreach (string s in header.Split(new char[] { ',' }))
            {
                headString2Ids.Add(s, index++);
            }
            for (int i = 1; i < contents.Length; i++)
            {
                if (contents[i].Equals("\n"))
                {
                    continue;
                }
                try
                {
                    XlsEngine o = new XlsEngine();
                    result.Add(o);
                    String[] content = contents[i].Split(new char[] { ',' });
                    o.SeqNo = Convert.ToInt32(content[headString2Ids["SeqNo"]]);
                    o.Offset = Convert.ToInt32(content[headString2Ids["Offset"]], 16);
                    o.RemodelCost = Convert.ToInt32(content[headString2Ids["RemodelCost"]]);
                    o.Weight = Convert.ToInt32(content[headString2Ids["Weight"]]);
                    o.InitDefen = Convert.ToInt32(content[headString2Ids["InitDefen"]]);
                    o.MaxDefen = Convert.ToInt32(content[headString2Ids["MaxDefen"]]);
                    o.LoadPower = Convert.ToInt32(content[headString2Ids["LoadPower"]]);
                    o.Name = "";
                    int starGet = 0;
                    if (itemNames != null)
                    {
                        List<XlsItemList> temp = itemNames.Where(x => x.TypeId == 12).OrderBy(x => x.Offset).ToList();
                        if (temp.Any())
                        {
                            o.Name = temp[o.SeqNo].Name;
                            if (o.SeqNo + 1 < temp.Count)
                            {
                                try
                                {
                                    starGet = Convert.ToInt32(temp[index + 1].StarGet);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e);
                                }
                            }

                        }
                    }
                    o.Ability = Convert.ToInt32(content[headString2Ids["Ability"]]);
                    o.AbilityName = content[headString2Ids["AbilityName"]].Replace("，", ",");
                    o.UpdateLimit = Convert.ToInt32(content[headString2Ids["UpdateLimit"]]);
                    o.UpdateDesc = content[headString2Ids["UpdateDesc"]].Replace("，", ",");
                    o.Generation = Convert.ToInt32(content[headString2Ids["Generation"]]);
                    o.Unknown = Convert.ToInt32(content[headString2Ids["Unknown"]]);


                    foreach (String s in content[headString2Ids["Data"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Data.Add(Convert.ToByte(s, 16));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
            return result;
        }
        private List<XlsCarWepon> loadXlsCarWeponArrData(String[] contents)
        {
            List<XlsItemList> itemNames = getDataFromDataBase("itemlist.arr") as List<XlsItemList>;
            List<XlsCarWepon> result = new List<XlsCarWepon>();
            Dictionary<String, int> headString2Ids = new Dictionary<string, int>();
            String header = contents[0];
            int index = 0;
            foreach (string s in header.Split(new char[] { ',' }))
            {
                headString2Ids.Add(s, index++);
            }
            for (int i = 1; i < contents.Length; i++)
            {
                if (contents[i].Equals("\n"))
                {
                    continue;
                }
                try
                {

                    XlsCarWepon o = new XlsCarWepon();
                    result.Add(o);
                    String[] content = contents[i].Split(new char[] { ',' });
                    o.SeqNo = Convert.ToInt32(content[headString2Ids["SeqNo"]]);
                    o.Offset = Convert.ToInt32(content[headString2Ids["Offset"]], 16);
                    o.RemodelCost = Convert.ToInt32(content[headString2Ids["RemodelCost"]]);
                    o.RemodelWeight = Convert.ToInt32(content[headString2Ids["RemodelWeight"]]);
                    o.Weight = Convert.ToInt32(content[headString2Ids["Weight"]]);
                    o.InitDefen = Convert.ToInt32(content[headString2Ids["InitDefen"]]);
                    o.MaxDefen = Convert.ToInt32(content[headString2Ids["MaxDefen"]]);
                    o.InitAttack = Convert.ToInt32(content[headString2Ids["InitAttack"]]);
                    o.AttAttribute = Convert.ToInt32(content[headString2Ids["AttAttribute"]]);
                    o.AttAnime = Convert.ToInt32(content[headString2Ids["AttAnime"]]);
                    o.AttRange = Convert.ToInt32(content[headString2Ids["AttRange"]]);
                    o.InitBombNum = Convert.ToInt32(content[headString2Ids["InitBombNum"]]);
                    o.MaxBombNum = Convert.ToInt32(content[headString2Ids["MaxBombNum"]]);
                    o.MaxAttack = Convert.ToInt32(content[headString2Ids["MaxAttack"]]);
                    o.AttRangeName = content[headString2Ids["AttRangeName"]].Replace("，", ",");
                    o.Name = "";
                    int starGet = 0;
                    if (itemNames != null)
                    {
                        List<XlsItemList> temp = itemNames.Where(x => x.TypeId >= 14 && x.TypeId < 20).OrderBy(x => x.Offset).ToList();
                        if (temp.Any())
                        {
                            o.Name = temp[o.SeqNo].Name;
                        }
                    }

                    foreach (String s in content[headString2Ids["Data"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Data.Add(Convert.ToByte(s, 16));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
            return result;
        }

        private List<XlsChassis> loadXlsChassisArrData(String[] contents)
        {
            List<XlsItemList> itemNames = getDataFromDataBase("itemlist.arr") as List<XlsItemList>;
            List<XlsChassis> result = new List<XlsChassis>();
            Dictionary<String, int> headString2Ids = new Dictionary<string, int>();
            String header = contents[0];
            int index = 0;
            foreach (string s in header.Split(new char[] { ',' }))
            {
                headString2Ids.Add(s, index++);
            }
            for (int i = 1; i < contents.Length; i++)
            {
                if (contents[i].Equals("\n"))
                {
                    continue;
                }
                try
                {
                    XlsChassis o = new XlsChassis();
                    result.Add(o);
                    String[] content = contents[i].Split(new char[] { ',' });
                    o.SeqNo = Convert.ToInt32(content[headString2Ids["SeqNo"]]);
                    o.Offset = Convert.ToInt32(content[headString2Ids["Offset"]], 16);
                    o.DoubleC = content[headString2Ids["DoubleC"]].Equals("是");
                    o.DoubleE = content[headString2Ids["DoubleE"]].Equals("是");
                    o.Hole1 = Convert.ToInt32(content[headString2Ids["Hole1"]]);
                    o.Hole1Remark = content[headString2Ids["Hole1Remark"]].Replace("，", ",");
                    o.Hole2 = Convert.ToInt32(content[headString2Ids["Hole2"]]);
                    o.Hole2Remark = content[headString2Ids["Hole2Remark"]].Replace("，", ",");
                    o.Hole3 = Convert.ToInt32(content[headString2Ids["Hole3"]]);
                    o.Hole3Remark = content[headString2Ids["Hole3Remark"]].Replace("，", ",");
                    o.Hole4 = Convert.ToInt32(content[headString2Ids["Hole4"]]);
                    o.Hole4Remark = content[headString2Ids["Hole4Remark"]].Replace("，", ",");
                    o.Hole5 = Convert.ToInt32(content[headString2Ids["Hole5"]]);
                    o.Hole5Remark = content[headString2Ids["Hole5Remark"]].Replace("，", ",");
                    o.Hole6 = Convert.ToInt32(content[headString2Ids["Hole6"]]);
                    o.Hole6Remark = content[headString2Ids["Hole6Remark"]].Replace("，", ",");
                    o.Remodel = Convert.ToInt32(content[headString2Ids["Remodel"]]);
                    o.Type = Convert.ToInt32(content[headString2Ids["Type"]]);
                    o.TypeName = content[headString2Ids["TypeName"]].Replace("，", ",");
                    o.Level = Convert.ToInt32(content[headString2Ids["Level"]]);
                    o.Storage = Convert.ToInt32(content[headString2Ids["Storage"]]);
                    o.Ability = Convert.ToInt32(content[headString2Ids["Ability"]]);
                    o.AbilityName = content[headString2Ids["AbilityName"]].Replace("，", ",");
                    o.Name = "";
                    int starGet = 0;
                    if (itemNames != null)
                    {
                        List<XlsItemList> temp = itemNames.Where(x => x.TypeId == 11).OrderBy(x => x.Offset).ToList();
                        if (temp.Any())
                        {
                            o.Name = temp[o.SeqNo].Name;
                        }
                    }

                    foreach (String s in content[headString2Ids["Data"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Data.Add(Convert.ToByte(s, 16));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
            return result;
        }

        private List<XlsMonster> loadXlsEneMonsterArrData(String[] contents)
        {
            List<XlsItemList> itemNames = getDataFromDataBase("itemlist.arr") as List<XlsItemList>;
            List<XlsNam> monsterNames = getDataFromDataBase("ene_monster.nam") as List<XlsNam>;
            List<XlsMonster> result = new List<XlsMonster>();
            Dictionary<String, int> headString2Ids = new Dictionary<string, int>();
            String header = contents[0];
            int index = 0;
            foreach (string s in header.Split(new char[] { ',' }))
            {
                headString2Ids.Add(s, index++);
            }
            for (int i = 1; i < contents.Length; i++)
            {
                if (contents[i].Equals("\n"))
                {
                    continue;
                }
                try
                {

                    XlsMonster o = new XlsMonster();
                    result.Add(o);
                    String[] content = contents[i].Split(new char[] { ',' });
                    o.SeqNo = Convert.ToInt32(content[headString2Ids["SeqNo"]]);
                    o.Offset = Convert.ToInt32(content[headString2Ids["Offset"]], 16);
                    o.NameOffsetAddr = Convert.ToInt32(content[headString2Ids["NameOffsetAddr"]], 16);
                    o.Name = content[headString2Ids["Name"]];
                    if (monsterNames != null)
                    {
                        var firstOrDefault = monsterNames.FirstOrDefault(x => x.Offset == o.NameOffsetAddr);
                        if (firstOrDefault != null)
                            o.Name = firstOrDefault.Text;
                    }

                    o.Appearance = Convert.ToInt32(content[headString2Ids["Appearance"]]);
                    o.Hp = Convert.ToInt32(content[headString2Ids["Hp"]]);
                    o.Attack = Convert.ToInt32(content[headString2Ids["Attack"]]);
                    o.Defence = Convert.ToInt32(content[headString2Ids["Defence"]]);
                    o.Speed = Convert.ToInt32(content[headString2Ids["Speed"]]);
                    o.Exp = Convert.ToInt32(content[headString2Ids["Exp"]]);
                    o.Gold = Convert.ToInt32(content[headString2Ids["Gold"]]);
                    o.SeqNoSub = Convert.ToInt32(content[headString2Ids["SeqNoSub"]]);
                    o.SeqNo = Convert.ToInt32(content[headString2Ids["SeqNo"]]);
                    o.Level = Convert.ToInt32(content[headString2Ids["Level"]]);
                    o.Hit = Convert.ToInt32(content[headString2Ids["Hit"]]);
                    o.Parry = Convert.ToInt32(content[headString2Ids["Parry"]]);
                    o.ActionTimes = Convert.ToInt32(content[headString2Ids["ActionTimes"]]);
                    o.Hit = Convert.ToInt32(content[headString2Ids["Hit"]]);

                    o.UnknownResist1 = Convert.ToInt32(content[headString2Ids["UnknownResist1"]]);
                    o.UnknownResist2 = Convert.ToInt32(content[headString2Ids["UnknownResist2"]]);
                    o.NormalResist = Convert.ToInt32(content[headString2Ids["NormalResist"]]);
                    o.FireResist = Convert.ToInt32(content[headString2Ids["FireResist"]]);
                    o.IceResist = Convert.ToInt32(content[headString2Ids["IceResist"]]);
                    o.ElectriResist = Convert.ToInt32(content[headString2Ids["ElectriResist"]]);
                    o.SoundResist = Convert.ToInt32(content[headString2Ids["SoundResist"]]);
                    o.GasResist = Convert.ToInt32(content[headString2Ids["GasResist"]]);
                    o.LaserRsist = Convert.ToInt32(content[headString2Ids["LaserRsist"]]);


                    o.Drop1 = Convert.ToInt32(content[headString2Ids["Drop1"]]);
                    o.Drop1Rate = Convert.ToInt32(content[headString2Ids["Drop1Rate"]]);
                    o.Drop1Remark = content[headString2Ids["Drop1Remark"]].Replace("，", ",");

                    o.Drop2 = Convert.ToInt32(content[headString2Ids["Drop2"]]);
                    o.Drop2Rate = Convert.ToInt32(content[headString2Ids["Drop2Rate"]]);
                    o.Drop2Remark = content[headString2Ids["Drop2Remark"]].Replace("，", ",");

                    o.Drop3 = Convert.ToInt32(content[headString2Ids["Drop3"]]);
                    o.Drop3Rate = Convert.ToInt32(content[headString2Ids["Drop3Rate"]]);
                    o.Drop3Remark = content[headString2Ids["Drop3Remark"]].Replace("，", ",");

                    o.Drop4 = Convert.ToInt32(content[headString2Ids["Drop4"]]);
                    o.Drop4Rate = Convert.ToInt32(content[headString2Ids["Drop4Rate"]]);
                    o.Drop4Remark = content[headString2Ids["Drop4Remark"]].Replace("，", ",");

                    foreach (String s in content[headString2Ids["Action1Code"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Action1Code.Add(Convert.ToByte(s, 16));
                    }
                    foreach (String s in content[headString2Ids["Action2Code"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Action2Code.Add(Convert.ToByte(s, 16));
                    }
                    foreach (String s in content[headString2Ids["Action3Code"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Action3Code.Add(Convert.ToByte(s, 16));
                    }
                    foreach (String s in content[headString2Ids["Action4Code"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Action4Code.Add(Convert.ToByte(s, 16));
                    }
                    foreach (String s in content[headString2Ids["Action5Code"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Action5Code.Add(Convert.ToByte(s, 16));
                    }
                    foreach (String s in content[headString2Ids["Action6Code"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Action6Code.Add(Convert.ToByte(s, 16));
                    }
                    foreach (String s in content[headString2Ids["Data"]].Split(new char[] { ' ' }))
                    {
                        if (String.IsNullOrEmpty(s))
                        {
                            continue;
                        }
                        o.Data.Add(Convert.ToByte(s, 16));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
            return result;
        }
    }
}
