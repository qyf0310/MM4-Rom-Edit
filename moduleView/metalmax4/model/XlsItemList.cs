﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    public class XlsItemList:ModelBase
    {
        private int seqNo;
        private int dataId;
        private string name;
        private int nameOffsetAddr;
        private int typeId;
        private int cost;
        private bool starRandom;
        private String starGet;
        private bool isDLC;
        private List<byte> data = new List<byte>();
        private int offset = 0;

        public override string getText()
        {
            return name;
        }

        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public int DataId
        {
            get { return dataId; }
            set { dataId = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int NameOffsetAddr
        {
            get { return nameOffsetAddr; }
            set { nameOffsetAddr = value; }
        }

        public int TypeId
        {
            get { return typeId; }
            set { typeId = value; }
        }

        public int Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        public bool StarRandom
        {
            get { return starRandom; }
            set { starRandom = value; }
        }

        public string StarGet
        {
            get { return starGet; }
            set { starGet = value; }
        }

        public bool IsDlc
        {
            get { return isDLC; }
            set { isDLC = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public int Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
