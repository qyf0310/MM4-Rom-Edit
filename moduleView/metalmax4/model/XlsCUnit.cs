﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsCUnit:ModelBase
    {
        private int seqNo;
        private String name;
        private int remodelCost;
        private int weight;
        private int initDefen;
        private int initHitRate;
        private int initParryRate;
        private int maxDefen;
        private int maxHitRate;
        private int maxParryRate;
        private int initAbilityNum;
        private int ability1;
        private int ability2;
        private int ability3;
        private String abilityName;
        private List<byte> data = new List<byte>();
        private long offset = 0;
        public override string getText()
        {
            return name;
        }
        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int RemodelCost
        {
            get { return remodelCost; }
            set { remodelCost = value; }
        }

        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public int InitDefen
        {
            get { return initDefen; }
            set { initDefen = value; }
        }
        public int MaxDefen
        {
            get { return maxDefen; }
            set { maxDefen = value; }
        }
        public int InitHitRate
        {
            get { return initHitRate; }
            set { initHitRate = value; }
        }
        public int MaxHitRate
        {
            get { return maxHitRate; }
            set { maxHitRate = value; }
        }
        public int InitParryRate
        {
            get { return initParryRate; }
            set { initParryRate = value; }
        }
        public int MaxParryRate
        {
            get { return maxParryRate; }
            set { maxParryRate = value; }
        }

        public int InitAbilityNum
        {
            get { return initAbilityNum; }
            set { initAbilityNum = value; }
        }

        public int Ability1
        {
            get { return ability1; }
            set { ability1 = value; }
        }

        public int Ability2
        {
            get { return ability2; }
            set { ability2 = value; }
        }

        public int Ability3
        {
            get { return ability3; }
            set { ability3 = value; }
        }

        public string AbilityName
        {
            get { return abilityName; }
            set { abilityName = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
