﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class AnimeNam
    {
        private int code;
        private string text;
        private int type = 0;//1战车武器,2人武器

        public int Code
        {
            get { return code; }
            set { code = value; }
        }

        public string Text
        {
            get { return String.Format("{0}-{1}",code,text); }
            set { text = value; }
        }

        public int Type
        {
            get { return type; }
            set { type = value; }
        }

        public AnimeNam(int code, string text, int type)
        {
            this.code = code;
            this.text = text;
            this.type = type;
        }
    }
}
