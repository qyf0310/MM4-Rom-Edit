﻿using MM4RomEdit.Controls;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    partial class CarInitView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbNameAddr = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cbbHole6 = new System.Windows.Forms.ComboBox();
            this.cbbHole5 = new System.Windows.Forms.ComboBox();
            this.cbbHole4 = new System.Windows.Forms.ComboBox();
            this.cbbHole3 = new System.Windows.Forms.ComboBox();
            this.cbbHole2 = new System.Windows.Forms.ComboBox();
            this.cbbHole1 = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.tbRomAddr = new System.Windows.Forms.TextBox();
            this.btnSaveToRom = new System.Windows.Forms.Button();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.tbRomData = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbbItem5 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbbItem4 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbbItem3 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbbItem2 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbbItem1 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbbChassis = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbbCunit = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbbEngin = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbUnknown = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ncSP = new NumericControl();
            this.searchTree1 = new SearchTree();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncSP)).BeginInit();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(211, 6);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(100, 21);
            this.tbName.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(164, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "名称：";
            // 
            // tbNameAddr
            // 
            this.tbNameAddr.Location = new System.Drawing.Point(388, 6);
            this.tbNameAddr.Name = "tbNameAddr";
            this.tbNameAddr.ReadOnly = true;
            this.tbNameAddr.Size = new System.Drawing.Size(76, 21);
            this.tbNameAddr.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(317, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "名称地址：";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(this.cbbHole6);
            this.groupBox3.Controls.Add(this.cbbHole5);
            this.groupBox3.Controls.Add(this.cbbHole4);
            this.groupBox3.Controls.Add(this.cbbHole3);
            this.groupBox3.Controls.Add(this.cbbHole2);
            this.groupBox3.Controls.Add(this.cbbHole1);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Location = new System.Drawing.Point(470, 33);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(384, 218);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "穴装备";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(234, 125);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(144, 82);
            this.textBox1.TabIndex = 78;
            this.textBox1.Text = "2）如果对应底盘页面的穴未开，则在整备界面看不见对应的穴武器，但可在战斗中使用。如果底盘的穴是固武，则此处编辑的武器无效";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(234, 32);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(144, 82);
            this.textBox2.TabIndex = 77;
            this.textBox2.Text = "1）仅在车辆登场前修改有效，登场后的修改无效。可在悠远之歌通过战车市场交换还原？";
            // 
            // cbbHole6
            // 
            this.cbbHole6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole6.FormattingEnabled = true;
            this.cbbHole6.Location = new System.Drawing.Point(59, 187);
            this.cbbHole6.Name = "cbbHole6";
            this.cbbHole6.Size = new System.Drawing.Size(169, 20);
            this.cbbHole6.TabIndex = 62;
            // 
            // cbbHole5
            // 
            this.cbbHole5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole5.FormattingEnabled = true;
            this.cbbHole5.Location = new System.Drawing.Point(59, 156);
            this.cbbHole5.Name = "cbbHole5";
            this.cbbHole5.Size = new System.Drawing.Size(169, 20);
            this.cbbHole5.TabIndex = 61;
            // 
            // cbbHole4
            // 
            this.cbbHole4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole4.FormattingEnabled = true;
            this.cbbHole4.Location = new System.Drawing.Point(59, 125);
            this.cbbHole4.Name = "cbbHole4";
            this.cbbHole4.Size = new System.Drawing.Size(169, 20);
            this.cbbHole4.TabIndex = 60;
            // 
            // cbbHole3
            // 
            this.cbbHole3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole3.FormattingEnabled = true;
            this.cbbHole3.Location = new System.Drawing.Point(59, 94);
            this.cbbHole3.Name = "cbbHole3";
            this.cbbHole3.Size = new System.Drawing.Size(169, 20);
            this.cbbHole3.TabIndex = 59;
            // 
            // cbbHole2
            // 
            this.cbbHole2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole2.FormattingEnabled = true;
            this.cbbHole2.Location = new System.Drawing.Point(59, 63);
            this.cbbHole2.Name = "cbbHole2";
            this.cbbHole2.Size = new System.Drawing.Size(169, 20);
            this.cbbHole2.TabIndex = 58;
            // 
            // cbbHole1
            // 
            this.cbbHole1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbHole1.FormattingEnabled = true;
            this.cbbHole1.Location = new System.Drawing.Point(59, 32);
            this.cbbHole1.Name = "cbbHole1";
            this.cbbHole1.Size = new System.Drawing.Size(169, 20);
            this.cbbHole1.TabIndex = 52;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 190);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(35, 12);
            this.label27.TabIndex = 25;
            this.label27.Text = "穴6：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 159);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 12);
            this.label26.TabIndex = 22;
            this.label26.Text = "穴5：";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 128);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(35, 12);
            this.label25.TabIndex = 20;
            this.label25.Text = "穴4：";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 97);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 12);
            this.label24.TabIndex = 18;
            this.label24.Text = "穴3：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 12);
            this.label14.TabIndex = 16;
            this.label14.Text = "穴2：";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 35);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 12);
            this.label31.TabIndex = 14;
            this.label31.Text = "穴1：";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(164, 263);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 12);
            this.label37.TabIndex = 50;
            this.label37.Text = "ROM地址：";
            // 
            // tbRomAddr
            // 
            this.tbRomAddr.Location = new System.Drawing.Point(229, 260);
            this.tbRomAddr.Name = "tbRomAddr";
            this.tbRomAddr.ReadOnly = true;
            this.tbRomAddr.Size = new System.Drawing.Size(124, 21);
            this.tbRomAddr.TabIndex = 51;
            this.tbRomAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSaveToRom
            // 
            this.btnSaveToRom.Location = new System.Drawing.Point(462, 260);
            this.btnSaveToRom.Name = "btnSaveToRom";
            this.btnSaveToRom.Size = new System.Drawing.Size(86, 23);
            this.btnSaveToRom.TabIndex = 54;
            this.btnSaveToRom.Text = "写入ROM";
            this.btnSaveToRom.UseVisualStyleBackColor = true;
            this.btnSaveToRom.Click += new System.EventHandler(this.btnSaveToRom_Click);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(412, 260);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(44, 21);
            this.tbLength.TabIndex = 53;
            this.tbLength.Text = "40";
            this.tbLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(365, 263);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 52;
            this.label38.Text = "长度：";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(164, 292);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 12);
            this.label39.TabIndex = 55;
            this.label39.Text = "ROM数据：";
            // 
            // tbRomData
            // 
            this.tbRomData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRomData.Location = new System.Drawing.Point(164, 307);
            this.tbRomData.Multiline = true;
            this.tbRomData.Name = "tbRomData";
            this.tbRomData.ReadOnly = true;
            this.tbRomData.Size = new System.Drawing.Size(690, 147);
            this.tbRomData.TabIndex = 56;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbbItem5);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cbbItem4);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.cbbItem3);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cbbItem2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cbbItem1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(164, 99);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 152);
            this.groupBox1.TabIndex = 57;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "道具";
            // 
            // cbbItem5
            // 
            this.cbbItem5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbItem5.FormattingEnabled = true;
            this.cbbItem5.Location = new System.Drawing.Point(59, 126);
            this.cbbItem5.Name = "cbbItem5";
            this.cbbItem5.Size = new System.Drawing.Size(135, 20);
            this.cbbItem5.TabIndex = 72;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 12);
            this.label11.TabIndex = 63;
            this.label11.Text = "栏1：";
            // 
            // cbbItem4
            // 
            this.cbbItem4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbItem4.FormattingEnabled = true;
            this.cbbItem4.Location = new System.Drawing.Point(59, 98);
            this.cbbItem4.Name = "cbbItem4";
            this.cbbItem4.Size = new System.Drawing.Size(135, 20);
            this.cbbItem4.TabIndex = 71;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 12);
            this.label10.TabIndex = 64;
            this.label10.Text = "栏2：";
            // 
            // cbbItem3
            // 
            this.cbbItem3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbItem3.FormattingEnabled = true;
            this.cbbItem3.Location = new System.Drawing.Point(59, 70);
            this.cbbItem3.Name = "cbbItem3";
            this.cbbItem3.Size = new System.Drawing.Size(135, 20);
            this.cbbItem3.TabIndex = 70;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 12);
            this.label9.TabIndex = 65;
            this.label9.Text = "栏3：";
            // 
            // cbbItem2
            // 
            this.cbbItem2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbItem2.FormattingEnabled = true;
            this.cbbItem2.Location = new System.Drawing.Point(59, 42);
            this.cbbItem2.Name = "cbbItem2";
            this.cbbItem2.Size = new System.Drawing.Size(135, 20);
            this.cbbItem2.TabIndex = 69;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 101);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 66;
            this.label8.Text = "栏4：";
            // 
            // cbbItem1
            // 
            this.cbbItem1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbItem1.FormattingEnabled = true;
            this.cbbItem1.Location = new System.Drawing.Point(59, 14);
            this.cbbItem1.Name = "cbbItem1";
            this.cbbItem1.Size = new System.Drawing.Size(135, 20);
            this.cbbItem1.TabIndex = 68;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 67;
            this.label7.Text = "栏5：";
            // 
            // cbbChassis
            // 
            this.cbbChassis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbChassis.FormattingEnabled = true;
            this.cbbChassis.Location = new System.Drawing.Point(217, 44);
            this.cbbChassis.Name = "cbbChassis";
            this.cbbChassis.Size = new System.Drawing.Size(94, 20);
            this.cbbChassis.TabIndex = 78;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(164, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 77;
            this.label2.Text = "底盘：";
            // 
            // cbbCunit
            // 
            this.cbbCunit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbCunit.FormattingEnabled = true;
            this.cbbCunit.Location = new System.Drawing.Point(370, 44);
            this.cbbCunit.Name = "cbbCunit";
            this.cbbCunit.Size = new System.Drawing.Size(94, 20);
            this.cbbCunit.TabIndex = 80;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(317, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 79;
            this.label4.Text = "C装置：";
            // 
            // cbbEngin
            // 
            this.cbbEngin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbEngin.FormattingEnabled = true;
            this.cbbEngin.Location = new System.Drawing.Point(217, 71);
            this.cbbEngin.Name = "cbbEngin";
            this.cbbEngin.Size = new System.Drawing.Size(94, 20);
            this.cbbEngin.TabIndex = 82;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(164, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 81;
            this.label5.Text = "引擎：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(317, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 83;
            this.label6.Text = "装甲：";
            // 
            // tbUnknown
            // 
            this.tbUnknown.Location = new System.Drawing.Point(547, 6);
            this.tbUnknown.Name = "tbUnknown";
            this.tbUnknown.ReadOnly = true;
            this.tbUnknown.Size = new System.Drawing.Size(307, 21);
            this.tbUnknown.TabIndex = 86;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(476, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 85;
            this.label12.Text = "未知数据：";
            // 
            // ncSP
            // 
            this.ncSP.Location = new System.Drawing.Point(370, 72);
            this.ncSP.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncSP.MaxNumberControl = null;
            this.ncSP.MinNumberControl = null;
            this.ncSP.Name = "ncSP";
            this.ncSP.Size = new System.Drawing.Size(94, 21);
            this.ncSP.TabIndex = 84;
            this.ncSP.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // searchTree1
            // 
            this.searchTree1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTree1.Location = new System.Drawing.Point(2, 2);
            this.searchTree1.Name = "searchTree1";
            this.searchTree1.Size = new System.Drawing.Size(156, 452);
            this.searchTree1.TabIndex = 0;
            this.searchTree1.DoubleClick += new System.EventHandler(this.searchTree1_DoubleClick);
            // 
            // CarInitView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 455);
            this.Controls.Add(this.tbUnknown);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.ncSP);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbbEngin);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbbCunit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbbChassis);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbRomData);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.btnSaveToRom);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.tbRomAddr);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.tbNameAddr);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.searchTree1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CarInitView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "MonsterView";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncSP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.SearchTree searchTree1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbNameAddr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbRomAddr;
        private System.Windows.Forms.Button btnSaveToRom;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbRomData;
        private System.Windows.Forms.ComboBox cbbHole6;
        private System.Windows.Forms.ComboBox cbbHole5;
        private System.Windows.Forms.ComboBox cbbHole4;
        private System.Windows.Forms.ComboBox cbbHole3;
        private System.Windows.Forms.ComboBox cbbHole2;
        private System.Windows.Forms.ComboBox cbbHole1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbbChassis;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbbCunit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbbEngin;
        private System.Windows.Forms.Label label5;
        private Controls.NumericControl ncSP;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbbItem5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbbItem4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbbItem3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbbItem2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbbItem1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbUnknown;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
    }
}