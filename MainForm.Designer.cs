﻿namespace MM4RomEdit
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tpSysinfo = new System.Windows.Forms.TabPage();
            this.tbSysInfo = new System.Windows.Forms.TextBox();
            this.tpMM4View = new System.Windows.Forms.TabPage();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打开ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbOpenRom = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbCloseRom = new System.Windows.Forms.ToolStripButton();
            this.tcMain.SuspendLayout();
            this.tpSysinfo.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcMain.Controls.Add(this.tpSysinfo);
            this.tcMain.Controls.Add(this.tpMM4View);
            this.tcMain.Location = new System.Drawing.Point(0, 53);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(1084, 548);
            this.tcMain.TabIndex = 0;
            // 
            // tpSysinfo
            // 
            this.tpSysinfo.Controls.Add(this.tbSysInfo);
            this.tpSysinfo.Location = new System.Drawing.Point(4, 22);
            this.tpSysinfo.Name = "tpSysinfo";
            this.tpSysinfo.Padding = new System.Windows.Forms.Padding(3);
            this.tpSysinfo.Size = new System.Drawing.Size(1076, 522);
            this.tpSysinfo.TabIndex = 2;
            this.tpSysinfo.Text = "系统信息";
            this.tpSysinfo.UseVisualStyleBackColor = true;
            // 
            // tbSysInfo
            // 
            this.tbSysInfo.BackColor = System.Drawing.Color.White;
            this.tbSysInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSysInfo.Location = new System.Drawing.Point(3, 3);
            this.tbSysInfo.Multiline = true;
            this.tbSysInfo.Name = "tbSysInfo";
            this.tbSysInfo.ReadOnly = true;
            this.tbSysInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbSysInfo.Size = new System.Drawing.Size(1070, 516);
            this.tbSysInfo.TabIndex = 0;
            // 
            // tpMM4View
            // 
            this.tpMM4View.Location = new System.Drawing.Point(4, 22);
            this.tpMM4View.Name = "tpMM4View";
            this.tpMM4View.Padding = new System.Windows.Forms.Padding(3);
            this.tpMM4View.Size = new System.Drawing.Size(1076, 522);
            this.tpMM4View.TabIndex = 0;
            this.tpMM4View.Text = "文件预览";
            this.tpMM4View.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1084, 25);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 打开ToolStripMenuItem
            // 
            this.打开ToolStripMenuItem.Name = "打开ToolStripMenuItem";
            this.打开ToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.打开ToolStripMenuItem.Text = "打开...";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbOpenRom,
            this.toolStripSeparator1,
            this.tsbCloseRom});
            this.toolStrip1.Location = new System.Drawing.Point(0, 25);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1084, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbOpenRom
            // 
            this.tsbOpenRom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbOpenRom.Image = global::MM4RomEdit.Properties.Resources.Open1;
            this.tsbOpenRom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbOpenRom.Name = "tsbOpenRom";
            this.tsbOpenRom.Size = new System.Drawing.Size(23, 22);
            this.tsbOpenRom.Text = "加载MM4 cci文件";
            this.tsbOpenRom.Click += new System.EventHandler(this.tsbOpenRom_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsbCloseRom
            // 
            this.tsbCloseRom.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbCloseRom.Image = global::MM4RomEdit.Properties.Resources.Close1;
            this.tsbCloseRom.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbCloseRom.Name = "tsbCloseRom";
            this.tsbCloseRom.Size = new System.Drawing.Size(23, 22);
            this.tsbCloseRom.Text = "关闭rom文件";
            this.tsbCloseRom.Click += new System.EventHandler(this.tsbCloseRom_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 601);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.HangulFull;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "MM4 ROM编辑 20180911";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tcMain.ResumeLayout(false);
            this.tpSysinfo.ResumeLayout(false);
            this.tpSysinfo.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tpMM4View;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打开ToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbOpenRom;
        private System.Windows.Forms.TabPage tpSysinfo;
        private System.Windows.Forms.TextBox tbSysInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbCloseRom;
    }
}