﻿using MM4RomEdit.Controls;

namespace MM4RomEdit.moduleView.metalmax4.FileSubView
{
    partial class CarWeponView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbAttackResult = new System.Windows.Forms.TextBox();
            this.cbbAttribute = new System.Windows.Forms.ComboBox();
            this.cbbRange = new System.Windows.Forms.ComboBox();
            this.cbbAnime = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSaveToRom = new System.Windows.Forms.Button();
            this.tbLength = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbRomAddr = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbRomData = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbUnknown1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbUnknown2 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbUnknown3 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbbApperence = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lvData_similar = new System.Windows.Forms.ListView();
            this.chName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAnime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chRange = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chEffect = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ncRemodelWeight = new MM4RomEdit.Controls.NumericControl();
            this.ncBombNumMax = new MM4RomEdit.Controls.NumericControl();
            this.ncBombNumInit = new MM4RomEdit.Controls.NumericControl();
            this.ncAtkMax = new MM4RomEdit.Controls.NumericControl();
            this.ncATKInit = new MM4RomEdit.Controls.NumericControl();
            this.ncDefMax = new MM4RomEdit.Controls.NumericControl();
            this.ncDEFInit = new MM4RomEdit.Controls.NumericControl();
            this.ncCost = new MM4RomEdit.Controls.NumericControl();
            this.ncWeight = new MM4RomEdit.Controls.NumericControl();
            this.searchTree1 = new MM4RomEdit.Controls.SearchTree();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ncRemodelWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncBombNumMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncBombNumInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAtkMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncATKInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDefMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEFInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncWeight)).BeginInit();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(228, 6);
            this.tbName.Name = "tbName";
            this.tbName.ReadOnly = true;
            this.tbName.Size = new System.Drawing.Size(240, 21);
            this.tbName.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "名称    ：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(157, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 11;
            this.label1.Text = "重量    ：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(157, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 42;
            this.label2.Text = "改造费  ：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(157, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 44;
            this.label4.Text = "初始防御：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(310, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 46;
            this.label5.Text = "最大防御：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(309, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 50;
            this.label6.Text = "最大攻击：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(156, 130);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 48;
            this.label7.Text = "初始攻击：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(310, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 54;
            this.label8.Text = "最大弹仓：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(157, 200);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 52;
            this.label9.Text = "初始弹仓：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.tbAttackResult);
            this.groupBox1.Controls.Add(this.cbbAttribute);
            this.groupBox1.Controls.Add(this.cbbRange);
            this.groupBox1.Controls.Add(this.cbbAnime);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(480, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 244);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "攻击属性";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 189);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 12);
            this.label18.TabIndex = 77;
            this.label18.Text = "效果台词：";
            // 
            // tbAttackResult
            // 
            this.tbAttackResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAttackResult.Location = new System.Drawing.Point(76, 186);
            this.tbAttackResult.Multiline = true;
            this.tbAttackResult.Name = "tbAttackResult";
            this.tbAttackResult.ReadOnly = true;
            this.tbAttackResult.Size = new System.Drawing.Size(167, 47);
            this.tbAttackResult.TabIndex = 76;
            // 
            // cbbAttribute
            // 
            this.cbbAttribute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAttribute.FormattingEnabled = true;
            this.cbbAttribute.Location = new System.Drawing.Point(76, 150);
            this.cbbAttribute.Name = "cbbAttribute";
            this.cbbAttribute.Size = new System.Drawing.Size(166, 20);
            this.cbbAttribute.TabIndex = 64;
            this.cbbAttribute.SelectedIndexChanged += new System.EventHandler(this.cbbAttribute_SelectedIndexChanged);
            // 
            // cbbRange
            // 
            this.cbbRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbRange.FormattingEnabled = true;
            this.cbbRange.Location = new System.Drawing.Point(76, 99);
            this.cbbRange.Name = "cbbRange";
            this.cbbRange.Size = new System.Drawing.Size(166, 20);
            this.cbbRange.TabIndex = 63;
            this.cbbRange.SelectedIndexChanged += new System.EventHandler(this.cbbRange_SelectedIndexChanged);
            // 
            // cbbAnime
            // 
            this.cbbAnime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAnime.FormattingEnabled = true;
            this.cbbAnime.Location = new System.Drawing.Point(77, 46);
            this.cbbAnime.Name = "cbbAnime";
            this.cbbAnime.Size = new System.Drawing.Size(166, 20);
            this.cbbAnime.TabIndex = 62;
            this.cbbAnime.SelectedIndexChanged += new System.EventHandler(this.cbbAnime_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 153);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 61;
            this.label13.Text = "效果：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 102);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 60;
            this.label12.Text = "范围：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 59;
            this.label11.Text = "动画：";
            // 
            // btnSaveToRom
            // 
            this.btnSaveToRom.Location = new System.Drawing.Point(480, 269);
            this.btnSaveToRom.Name = "btnSaveToRom";
            this.btnSaveToRom.Size = new System.Drawing.Size(86, 23);
            this.btnSaveToRom.TabIndex = 63;
            this.btnSaveToRom.Text = "写入ROM";
            this.btnSaveToRom.UseVisualStyleBackColor = true;
            this.btnSaveToRom.Click += new System.EventHandler(this.btnSaveToRom_Click);
            // 
            // tbLength
            // 
            this.tbLength.Location = new System.Drawing.Point(425, 271);
            this.tbLength.Name = "tbLength";
            this.tbLength.ReadOnly = true;
            this.tbLength.Size = new System.Drawing.Size(44, 21);
            this.tbLength.TabIndex = 62;
            this.tbLength.Text = "24";
            this.tbLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(378, 274);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 61;
            this.label38.Text = "长度：";
            // 
            // tbRomAddr
            // 
            this.tbRomAddr.Location = new System.Drawing.Point(228, 271);
            this.tbRomAddr.Name = "tbRomAddr";
            this.tbRomAddr.ReadOnly = true;
            this.tbRomAddr.Size = new System.Drawing.Size(136, 21);
            this.tbRomAddr.TabIndex = 60;
            this.tbRomAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(157, 274);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(59, 12);
            this.label37.TabIndex = 59;
            this.label37.Text = "ROM地址：";
            // 
            // tbRomData
            // 
            this.tbRomData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRomData.Location = new System.Drawing.Point(159, 324);
            this.tbRomData.Multiline = true;
            this.tbRomData.Name = "tbRomData";
            this.tbRomData.ReadOnly = true;
            this.tbRomData.Size = new System.Drawing.Size(879, 98);
            this.tbRomData.TabIndex = 65;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(157, 309);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 12);
            this.label39.TabIndex = 64;
            this.label39.Text = "ROM数据：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.label14.Location = new System.Drawing.Point(310, 86);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 12);
            this.label14.TabIndex = 66;
            this.label14.Text = "改造增重：";
            // 
            // tbUnknown1
            // 
            this.tbUnknown1.Location = new System.Drawing.Point(227, 235);
            this.tbUnknown1.Name = "tbUnknown1";
            this.tbUnknown1.ReadOnly = true;
            this.tbUnknown1.Size = new System.Drawing.Size(44, 21);
            this.tbUnknown1.TabIndex = 69;
            this.tbUnknown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(157, 238);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 12);
            this.label15.TabIndex = 68;
            this.label15.Text = "不明1：";
            // 
            // tbUnknown2
            // 
            this.tbUnknown2.Location = new System.Drawing.Point(326, 235);
            this.tbUnknown2.Name = "tbUnknown2";
            this.tbUnknown2.ReadOnly = true;
            this.tbUnknown2.Size = new System.Drawing.Size(38, 21);
            this.tbUnknown2.TabIndex = 71;
            this.tbUnknown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(278, 238);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(47, 12);
            this.label16.TabIndex = 70;
            this.label16.Text = "不明2：";
            // 
            // tbUnknown3
            // 
            this.tbUnknown3.Location = new System.Drawing.Point(430, 235);
            this.tbUnknown3.Name = "tbUnknown3";
            this.tbUnknown3.ReadOnly = true;
            this.tbUnknown3.Size = new System.Drawing.Size(38, 21);
            this.tbUnknown3.TabIndex = 73;
            this.tbUnknown3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(382, 238);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 12);
            this.label17.TabIndex = 72;
            this.label17.Text = "不明3：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(310, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 76;
            this.label10.Text = "外观：";
            // 
            // cbbApperence
            // 
            this.cbbApperence.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbApperence.FormattingEnabled = true;
            this.cbbApperence.Location = new System.Drawing.Point(361, 41);
            this.cbbApperence.Name = "cbbApperence";
            this.cbbApperence.Size = new System.Drawing.Size(108, 20);
            this.cbbApperence.TabIndex = 77;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lvData_similar);
            this.groupBox2.Location = new System.Drawing.Point(735, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(303, 244);
            this.groupBox2.TabIndex = 78;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "同属性武器";
            // 
            // lvData_similar
            // 
            this.lvData_similar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvData_similar.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chName,
            this.chAnime,
            this.chRange,
            this.chEffect});
            this.lvData_similar.FullRowSelect = true;
            this.lvData_similar.GridLines = true;
            this.lvData_similar.Location = new System.Drawing.Point(6, 20);
            this.lvData_similar.Name = "lvData_similar";
            this.lvData_similar.Size = new System.Drawing.Size(291, 218);
            this.lvData_similar.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lvData_similar.TabIndex = 0;
            this.lvData_similar.UseCompatibleStateImageBehavior = false;
            this.lvData_similar.View = System.Windows.Forms.View.Details;
            // 
            // chName
            // 
            this.chName.Text = "名称";
            this.chName.Width = 120;
            // 
            // chAnime
            // 
            this.chAnime.Text = "动画";
            this.chAnime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chRange
            // 
            this.chRange.Text = "范围";
            this.chRange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chEffect
            // 
            this.chEffect.Text = "效果";
            this.chEffect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ncRemodelWeight
            // 
            this.ncRemodelWeight.DecimalPlaces = 2;
            this.ncRemodelWeight.Location = new System.Drawing.Point(381, 84);
            this.ncRemodelWeight.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            131072});
            this.ncRemodelWeight.MaxNumberControl = null;
            this.ncRemodelWeight.MinNumberControl = null;
            this.ncRemodelWeight.Name = "ncRemodelWeight";
            this.ncRemodelWeight.Size = new System.Drawing.Size(88, 21);
            this.ncRemodelWeight.TabIndex = 67;
            this.ncRemodelWeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncBombNumMax
            // 
            this.ncBombNumMax.Location = new System.Drawing.Point(381, 198);
            this.ncBombNumMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncBombNumMax.MaxNumberControl = null;
            this.ncBombNumMax.MinNumberControl = null;
            this.ncBombNumMax.Name = "ncBombNumMax";
            this.ncBombNumMax.Size = new System.Drawing.Size(88, 21);
            this.ncBombNumMax.TabIndex = 55;
            this.ncBombNumMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncBombNumInit
            // 
            this.ncBombNumInit.Location = new System.Drawing.Point(228, 198);
            this.ncBombNumInit.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncBombNumInit.MaxNumberControl = null;
            this.ncBombNumInit.MinNumberControl = null;
            this.ncBombNumInit.Name = "ncBombNumInit";
            this.ncBombNumInit.Size = new System.Drawing.Size(76, 21);
            this.ncBombNumInit.TabIndex = 53;
            this.ncBombNumInit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncAtkMax
            // 
            this.ncAtkMax.Location = new System.Drawing.Point(380, 128);
            this.ncAtkMax.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncAtkMax.MaxNumberControl = null;
            this.ncAtkMax.MinNumberControl = null;
            this.ncAtkMax.Name = "ncAtkMax";
            this.ncAtkMax.Size = new System.Drawing.Size(88, 21);
            this.ncAtkMax.TabIndex = 51;
            this.ncAtkMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncATKInit
            // 
            this.ncATKInit.Location = new System.Drawing.Point(227, 128);
            this.ncATKInit.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncATKInit.MaxNumberControl = null;
            this.ncATKInit.MinNumberControl = null;
            this.ncATKInit.Name = "ncATKInit";
            this.ncATKInit.Size = new System.Drawing.Size(76, 21);
            this.ncATKInit.TabIndex = 49;
            this.ncATKInit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncDefMax
            // 
            this.ncDefMax.Location = new System.Drawing.Point(381, 163);
            this.ncDefMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncDefMax.MaxNumberControl = null;
            this.ncDefMax.MinNumberControl = null;
            this.ncDefMax.Name = "ncDefMax";
            this.ncDefMax.Size = new System.Drawing.Size(88, 21);
            this.ncDefMax.TabIndex = 47;
            this.ncDefMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncDEFInit
            // 
            this.ncDEFInit.Location = new System.Drawing.Point(228, 163);
            this.ncDEFInit.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.ncDEFInit.MaxNumberControl = null;
            this.ncDEFInit.MinNumberControl = null;
            this.ncDEFInit.Name = "ncDEFInit";
            this.ncDEFInit.Size = new System.Drawing.Size(76, 21);
            this.ncDEFInit.TabIndex = 45;
            this.ncDEFInit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncCost
            // 
            this.ncCost.Location = new System.Drawing.Point(228, 84);
            this.ncCost.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.ncCost.MaxNumberControl = null;
            this.ncCost.MinNumberControl = null;
            this.ncCost.Name = "ncCost";
            this.ncCost.Size = new System.Drawing.Size(76, 21);
            this.ncCost.TabIndex = 43;
            this.ncCost.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // ncWeight
            // 
            this.ncWeight.DecimalPlaces = 2;
            this.ncWeight.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ncWeight.Location = new System.Drawing.Point(228, 42);
            this.ncWeight.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            131072});
            this.ncWeight.MaxNumberControl = null;
            this.ncWeight.MinNumberControl = null;
            this.ncWeight.Name = "ncWeight";
            this.ncWeight.Size = new System.Drawing.Size(76, 21);
            this.ncWeight.TabIndex = 41;
            this.ncWeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // searchTree1
            // 
            this.searchTree1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.searchTree1.Location = new System.Drawing.Point(2, 2);
            this.searchTree1.Name = "searchTree1";
            this.searchTree1.Size = new System.Drawing.Size(149, 430);
            this.searchTree1.TabIndex = 0;
            this.searchTree1.DoubleClick += new System.EventHandler(this.searchTree1_DoubleClick);
            // 
            // CarWeponView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1050, 434);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.cbbApperence);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbUnknown3);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbUnknown2);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.tbUnknown1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.ncRemodelWeight);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.tbRomData);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.btnSaveToRom);
            this.Controls.Add(this.tbLength);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.tbRomAddr);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ncBombNumMax);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ncBombNumInit);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ncAtkMax);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ncATKInit);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ncDefMax);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ncDEFInit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ncCost);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ncWeight);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.searchTree1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CarWeponView";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "CarView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ncRemodelWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncBombNumMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncBombNumInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncAtkMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncATKInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDefMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncDEFInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ncWeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.SearchTree searchTree1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Controls.NumericControl ncWeight;
        private Controls.NumericControl ncCost;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private Controls.NumericControl ncDEFInit;
        private Controls.NumericControl ncDefMax;
        private System.Windows.Forms.Label label5;
        private Controls.NumericControl ncAtkMax;
        private System.Windows.Forms.Label label6;
        private Controls.NumericControl ncATKInit;
        private System.Windows.Forms.Label label7;
        private Controls.NumericControl ncBombNumMax;
        private System.Windows.Forms.Label label8;
        private Controls.NumericControl ncBombNumInit;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbbAttribute;
        private System.Windows.Forms.ComboBox cbbRange;
        private System.Windows.Forms.ComboBox cbbAnime;
        private System.Windows.Forms.Button btnSaveToRom;
        private System.Windows.Forms.TextBox tbLength;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbRomAddr;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox tbRomData;
        private System.Windows.Forms.Label label39;
        private Controls.NumericControl ncRemodelWeight;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbUnknown1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbUnknown2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbUnknown3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbAttackResult;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbbApperence;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lvData_similar;
        private System.Windows.Forms.ColumnHeader chName;
        private System.Windows.Forms.ColumnHeader chAnime;
        private System.Windows.Forms.ColumnHeader chRange;
        private System.Windows.Forms.ColumnHeader chEffect;
    }
}