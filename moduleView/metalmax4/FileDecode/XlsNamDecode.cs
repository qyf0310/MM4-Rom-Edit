﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsNamDecode
    {
        public static List<XlsNam> loadData(byte[] datas, string key)
        {
            List<XlsNam> result = new List<XlsNam>();
            StringBuilder sbline = new StringBuilder();
            int curLength = 0;
            List<byte> lineData = new List<byte>();
            int offset = 0;
            for (int i = 0; i < datas.Length;)
            {
                while (i < datas.Length && datas[i] == 0)
                {
                    lineData.Add(datas[i++]);
                    offset++;
                }
                if (i > 0 && datas[i - 1] == 0)
                {
                    if (sbline.Length > 0)
                    {
                        XlsNam o = new XlsNam();
                        result.Add(o);
                        o.CurrentLength = curLength;
                        o.MaxLength = lineData.Count - 1;//必须保留一位0做为间隔
                        o.Text = sbline.ToString();
                        o.Offset = offset - lineData.Count;
                        o.Data.AddRange(lineData);
                        sbline.Clear();
                        curLength = 0;
                        BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o,key);
                    }
                    lineData.Clear();
                }
                if (i >= datas.Length)
                {
                    break;
                }
                if (HexHelper.GetByteHigh(datas[i]) == 0xE && (i+2<datas.Length))
                {
                    StringBuilder sbTemp = new StringBuilder(@"\\u").Append(String.Format("{0:X}", HexHelper.GetByteLow(datas[i])));
                    int temp = (datas[i + 1] - 0x80) * 4 + HexHelper.GetByteHigh(datas[i + 2]) - 8;
                    sbTemp.Append(String.Format("{0:X2}", temp)).Append(String.Format("{0:X}", HexHelper.GetByteLow(datas[i + 2])));
                    sbline.Append(CodeTableHelper.Unicode2String(sbTemp.ToString()).Replace("\\", ""));


                    lineData.Add(datas[i]);
                    lineData.Add(datas[i + 1]);
                    lineData.Add(datas[i + 2]);
                    curLength += 3;
                    i += 3;
                    offset += 3;
                }
                else
                {
                    StringBuilder sbTemp = new StringBuilder(@"\\u00").Append(String.Format("{0:X2}", datas[i]));
                    sbline.Append(CodeTableHelper.Unicode2String(sbTemp.ToString()).Replace("\\", ""));
                    lineData.Add(datas[i]);
                    curLength += 1;
                    i++;
                    offset++;
                }

            }
            if (sbline.Length > 0)
            {
                XlsNam o = new XlsNam();
                result.Add(o);
                o.CurrentLength = curLength;
                o.MaxLength = lineData.Count - 1;
                o.Text = sbline.ToString();
                o.Offset = offset - o.MaxLength;
                lineData.RemoveAt(lineData.Count);
                o.Data.AddRange(lineData);
                
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null, key, true);
            DataBaseService.addData(key, result);
            return result;
        }



    }
}
