﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsBattleActData : ModelBase
    {

        private int seqNo;
        private String name;
        private int nameOffAddr;
        private List<byte> data=new List<byte>();
        private long offset = 0;

        private int actionCode;
        private int unknown1;
        private int animeId;
        private String words;
        private int wordsOffAdr;

        public override string getText()
        {
            return name;
        }

        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int NameOffAddr
        {
            get { return nameOffAddr; }
            set { nameOffAddr = value; }
        }

        public int ActionCode
        {
            get { return actionCode; }
            set { actionCode = value; }
        }

        public int Unknown1
        {
            get { return unknown1; }
            set { unknown1 = value; }
        }

        public int AnimeId
        {
            get { return animeId; }
            set { animeId = value; }
        }

        public string Words
        {
            get { return words; }
            set { words = value; }
        }

        public int WordsOffAdr
        {
            get { return wordsOffAdr; }
            set { wordsOffAdr = value; }
        }

        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
