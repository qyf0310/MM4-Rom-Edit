﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using MM4RomEdit.moduleView.metalmax4;

namespace MM4RomEdit.baseService
{
    public abstract class AbstractDataProcess
    {
        private bool isRun = false;

        public bool IsRun
        {
            get { return isRun; }
        }

        #region 事件定义
        public class ProgressHandleArgs : EventArgs
        {
            public readonly String Msg;
            public readonly DateTime Time = DateTime.Now;
            public readonly ProcessStateEnum Code;//0：成功   1：警告  2：失败
            public readonly Boolean isEnd = false;
            public readonly Object Data = null;
            public ProgressHandleArgs(string msg, ProcessStateEnum code,Object data,Boolean isEnd)
            {
                this.Msg = msg;
                this.Code = code;
                this.Data = data;
                this.isEnd = isEnd;
            }
        }
        public delegate void ProgressChangeHandle(object sender, ProgressHandleArgs e); 
        public event ProgressChangeHandle ProgressChange;
        private void OnProgressChange(ProgressHandleArgs e)
        {
            if (ProgressChange != null)
            {
                ProgressChange(null, e);
            }
        }
        #endregion

        private void pushMessage(String msg, ProcessStateEnum code, Object data, bool complete = false)
        {
            OnProgressChange(new ProgressHandleArgs(msg, code, data, complete));
        }

        private void pushException(Exception ex, Object data, bool complete = false)
        {
            pushMessage(ex.Message,ProcessStateEnum.FAILED, data,complete);
        }
        public void begin(Object data)
        {
            Thread task = new Thread(new ThreadStart(delegate
            {
                try
                {
                    isRun = true;
//                    pushMessage("process data begin...", ProcessStateEnum.SUCCESS,null);
                    run(data);
//                    pushMessage("process data end.", ProcessStateEnum.SUCCESS, null,true);
                }
                catch (Exception ex)
                {
                    pushMessage("process data error," + ex.Message, ProcessStateEnum.FAILED, null,true);
                }
                finally
                {
                    isRun = false;
                }
            }));
            task.Start();
        }

        protected abstract void run(Object data);
        protected void createMessage(String msg, ProcessStateEnum code, Object dataForProcess)
        {
            pushMessage(msg, code, dataForProcess);
        }

        protected void addDataToDataBase(String key, Object data)
        {
            DataBaseService.addData(key,data);
        }
        protected Object getDataFromDataBase(String key)
        {
            return DataBaseService.getData(key);
        }
    }
}
