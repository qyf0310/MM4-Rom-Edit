﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MM4RomEdit.moduleView.metalmax4.model
{
    class XlsMonster:ModelBase
    {
        private int seqNo;
        private int seqNoSub;
        private String name;
        private int nameOffsetAddr;
        private int appearance = 0;
        private int hp;
        private int attack;
        private int defence;
        private int speed;
        private int exp;
        private int gold;
        private int level;
        private int hit;
        private int parry;
        private int actionTimes;
        private int unknownResist1;
        private int normalResist;
        
        private int fireResist;
        private int iceResist;
        private int electriResist;
        private int soundResist;
        private int gasResist;
        private int laserRsist;
        private int unknownResist2;

        private List<byte> action1Code=new List<byte>();
        private List<byte> action2Code = new List<byte>();
        private List<byte> action3Code = new List<byte>();
        private List<byte> action4Code = new List<byte>();
        private List<byte> action5Code = new List<byte>();
        private List<byte> action6Code = new List<byte>();
        private List<byte> action7Code = new List<byte>();

        private int drop1;
        private int drop1Rate;
        private String drop1Remark;
        private int drop2;
        private int drop2Rate;
        private String drop2Remark;
        private int drop3;
        private int drop3Rate;
        private String drop3Remark;
        private int drop4;
        private int drop4Rate;
        private String drop4Remark;

        



        private List<byte> data = new List<byte>();
        private long offset = 0;

        public override string getText()
        {
            return name;
        }
        public int SeqNo
        {
            get { return seqNo; }
            set { seqNo = value; }
        }

        public int SeqNoSub
        {
            get { return seqNoSub; }
            set { seqNoSub = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int NameOffsetAddr
        {
            get { return nameOffsetAddr; }
            set { nameOffsetAddr = value; }
        }

        public int Appearance
        {
            get { return appearance; }
            set { appearance = value; }
        }

        public int Hp
        {
            get { return hp; }
            set { hp = value; }
        }

        public int Attack
        {
            get { return attack; }
            set { attack = value; }
        }

        public int Defence
        {
            get { return defence; }
            set { defence = value; }
        }

        public int Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public int Exp
        {
            get { return exp; }
            set { exp = value; }
        }

        public int Gold
        {
            get { return gold; }
            set { gold = value; }
        }

        public int Level
        {
            get { return level; }
            set { level = value; }
        }

        public int Hit
        {
            get { return hit; }
            set { hit = value; }
        }

        public int Parry
        {
            get { return parry; }
            set { parry = value; }
        }

        public int ActionTimes
        {
            get { return actionTimes; }
            set { actionTimes = value; }
        }
        public int UnknownResist1
        {
            get { return unknownResist1; }
            set { unknownResist1 = value; }
        }
        public int NormalResist
        {
            get { return normalResist; }
            set { normalResist = value; }
        }



        public int FireResist
        {
            get { return fireResist; }
            set { fireResist = value; }
        }

        public int IceResist
        {
            get { return iceResist; }
            set { iceResist = value; }
        }

        public int ElectriResist
        {
            get { return electriResist; }
            set { electriResist = value; }
        }

        public int SoundResist
        {
            get { return soundResist; }
            set { soundResist = value; }
        }

        public int GasResist
        {
            get { return gasResist; }
            set { gasResist = value; }
        }

        public int LaserRsist
        {
            get { return laserRsist; }
            set { laserRsist = value; }
        }
        public int UnknownResist2
        {
            get { return unknownResist2; }
            set { unknownResist2 = value; }
        }
        public int Drop1
        {
            get { return drop1; }
            set { drop1 = value; }
        }

        public int Drop1Rate
        {
            get { return drop1Rate; }
            set { drop1Rate = value; }
        }

        public string Drop1Remark
        {
            get { return drop1Remark; }
            set { drop1Remark = value; }
        }

        public int Drop2
        {
            get { return drop2; }
            set { drop2 = value; }
        }

        public int Drop2Rate
        {
            get { return drop2Rate; }
            set { drop2Rate = value; }
        }

        public string Drop2Remark
        {
            get { return drop2Remark; }
            set { drop2Remark = value; }
        }

        public int Drop3
        {
            get { return drop3; }
            set { drop3 = value; }
        }

        public int Drop3Rate
        {
            get { return drop3Rate; }
            set { drop3Rate = value; }
        }

        public string Drop3Remark
        {
            get { return drop3Remark; }
            set { drop3Remark = value; }
        }

        public int Drop4
        {
            get { return drop4; }
            set { drop4 = value; }
        }

        public int Drop4Rate
        {
            get { return drop4Rate; }
            set { drop4Rate = value; }
        }

        public string Drop4Remark
        {
            get { return drop4Remark; }
            set { drop4Remark = value; }
        }
        public List<byte> Action1Code
        {
            get { return action1Code; }
            set { action1Code = value; }
        }

        public List<byte> Action2Code
        {
            get { return action2Code; }
            set { action2Code = value; }
        }

        public List<byte> Action3Code
        {
            get { return action3Code; }
            set { action3Code = value; }
        }

        public List<byte> Action4Code
        {
            get { return action4Code; }
            set { action4Code = value; }
        }

        public List<byte> Action5Code
        {
            get { return action5Code; }
            set { action5Code = value; }
        }

        public List<byte> Action6Code
        {
            get { return action6Code; }
            set { action6Code = value; }
        }
        public List<byte> Action7Code
        {
            get { return action7Code; }
            set { action7Code = value; }
        }


        public List<byte> Data
        {
            get { return data; }
            set { data = value; }
        }

        public long Offset
        {
            get { return offset; }
            set { offset = value; }
        }
    }
}
