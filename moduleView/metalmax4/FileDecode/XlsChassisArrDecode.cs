﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;
using MM4RomEdit.baseService;
using MM4RomEdit.moduleView.metalmax4.model;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class XlsChassisArrDecode 
    {
        public static List<XlsChassis> loadData(byte[] datas, string key)
        {
            List<XlsItemList> itemNames = DataBaseService.getData(ConstanDef.ArrFileKey_itemlist) as List<XlsItemList>;
            List<XlsChassis> result = new List<XlsChassis>();

            int index = 0;
            for (int i = 0; i < datas.Length;)
            {
                if (i + 19 >= datas.Length)
                {
                    break;
                }
                XlsChassis o = new XlsChassis();
                result.Add(o);
                o.SeqNo = index;
                o.Offset = i;
                o.Hole1 = datas[i + 1] * 256 + datas[i];
                o.Hole1Remark = getHoleRemark(o.Hole1);
                o.Hole2 = datas[i + 3] * 256 + datas[i + 2];
                o.Hole2Remark = getHoleRemark(o.Hole2);
                o.Hole3 = datas[i + 5] * 256 + datas[i + 4];
                o.Hole3Remark = getHoleRemark(o.Hole3);
                o.Hole4 = datas[i + 7] * 256 + datas[i + 6];
                o.Hole4Remark = getHoleRemark(o.Hole4);
                o.Hole5 = datas[i + 9] * 256 + datas[i + 8];
                o.Hole5Remark = getHoleRemark(o.Hole5);
                o.Hole6 = datas[i + 11] * 256 + datas[i + 10];
                o.Hole6Remark = getHoleRemark(o.Hole6);
                o.Remodel = datas[i + 12];
                o.Type = datas[i + 13];
                o.TypeName = DataBaseService.getChassisSeriesName(o.Type);
                o.Level = datas[i + 14];
                o.Storage = datas[i + 15];
                o.DoubleE = datas[i + 16] > 0;
                o.DoubleC = datas[i + 17] > 0;
                o.Ability = datas[i + 18];
                o.AbilityName = DataBaseService.getTankAbility(o.Ability);
                //                datas[i + 19]=0;

                o.Name = "";
                if (itemNames != null)
                {
                    List<XlsItemList> temp = itemNames.Where(x => x.TypeId == 11)
                        .OrderBy(x => x.Offset).ToList();
                    if (temp.Any())
                    {
                        o.Name = temp[index].Name+"["+o.TypeName+"]";
                    }
                }


                for (int j = 0; j < 20; j++)
                {
                    if (i < datas.Length)
                    {
                        o.Data.Add(datas[i++]);
                    }

                }

                index++;


                BroadCastService.createMessage(String.Format("已完成:{0}%", i * 100 / datas.Length), ProcessStateEnum.NORMAL, o,key);
            }
            BroadCastService.createMessage(String.Format("已完成:100%"), ProcessStateEnum.NORMAL, null,key,true);
            DataBaseService.addData(key, result);
            return result;
        }


        private static String getHoleRemark(int code)
        {
            if (code==0)
            {
                return "无";
            }
            else if(code<=79 || code==148)
            {
                return "大炮类型";//stand:2
            }
            else if(code<=111 || code==146 || code==147)
            {
                return "机枪类型";//stand:88
            }
            else if (code <= 145)
            {
                return "SE类型";
            }
            else
            {
                return "固武";//range:149~702
            }
        }
        

    }
}
