﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace MM4RomEdit.moduleView.metalmax4.FileDecode
{
    class CodeTableHelper
    {
        public static Dictionary<String, String> load(String charsetTxtFile, String charsetXlltFile)
        {
            Dictionary<String,String> result=new Dictionary<string, string>();
            List<String> charsetList = loadcharsetTxtFile(charsetTxtFile);
            foreach (string s in charsetList)
            {
                result.Add(String2Unicode(s),s);
                String a = Unicode2String(String2Unicode(s));
            }
            return result;
        }
        public static string String2Unicode(string source)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(source);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i += 2)
            {
                stringBuilder.AppendFormat("\\u{0}{1}", bytes[i + 1].ToString("x").PadLeft(2, '0'), bytes[i].ToString("x").PadLeft(2, '0'));
            }
            return stringBuilder.ToString();
        }
        public static string Unicode2String(string source)
        {
            return new Regex(@"\\u([0-9A-F]{4})", RegexOptions.IgnoreCase | RegexOptions.Compiled).Replace(
                source, x => string.Empty + Convert.ToChar(Convert.ToUInt16(x.Result("$1"), 16)));
        }
        private static List<String> loadcharsetXlltFile(String charsetXlltFile)
        {
            List<String> result = new List<string>();
            if (!File.Exists(charsetXlltFile))
            {
                throw new Exception("file not exist:" + charsetXlltFile);
            }
            XmlReaderSettings xs = new XmlReaderSettings();
            xs.XmlResolver = null;
            xs.ProhibitDtd = false;
            XmlReader reader = XmlReader.Create(charsetXlltFile, xs);
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            XmlNode xnLetter = doc.SelectSingleNode("/letter-list/body/letter");
            String allContent = xnLetter.InnerText;
            String[] conents = allContent.Split(new char[] {';'});
            foreach (string s in conents)
            {
                Console.WriteLine(s);
            }
            return result;
        }
        private static List<String> loadcharsetTxtFile(String charsetTxtFile)
        {
            List<String> result = new List<string>();
            if (!File.Exists(charsetTxtFile))
            {
                throw new Exception("file not exist:" + charsetTxtFile);
            }
            string[] fileAll = File.ReadAllLines(charsetTxtFile);
            foreach (string line in fileAll)
            {
                for (int i = 0; i < line.Length; i++)
                {
                    result.Add(line.Substring(i, 1));
                }
            }
            return result;
        }
    }
}
